﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoreBankingSystem.Common
{
    public class WorkStations
    {
        public static List<SystemInformation> GetWorkStations()
        {
            List<SystemInformation> li = new List<SystemInformation>();
            li.Add(new SystemInformation { MacAddress = "2409:4070:81f:24bf:759f:2948:8e0f:d5c" });
            li.Add(new SystemInformation { MacAddress = "157.48.204.157" });
            li.Add(new SystemInformation { MacAddress = "192.168.20.194" });
            li.Add(new SystemInformation { MacAddress = "192.168.20.195" });
            li.Add(new SystemInformation { MacAddress = "192.168.20.196" });
            return li;
        }
    }
}


public class SystemInformation
{
    public string MacAddress { get; set; }
}
