﻿using DataAccess.IRepository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataAccess.Models;
using DomainModels;

namespace DataAccess.Repository
{
    public class AgentRepository : GenericRepository<Agent>, IAgentRepository
    {
        public AgentRepository(CBAContext context) : base(context)
        {

        }
        public IEnumerable<AgentModel> GetAgents()
        {
            return _cbaContext.Agents
                .Join(
                    _cbaContext.Accounts,
                    agent => agent.AccountID,
                    account => account.AccountID,
                    (agent, account) =>
                        new
                        {
                            agent = agent,
                            account = account
                        }
                )
                .Where(temp0 => (temp0.agent.RecordStatus != "D"))
                .Select(
                    temp0 =>
                        new AgentModel()
                        {
                            AgentID = temp0.agent.AgentID,
                            AgentNo = temp0.agent.AgentNo,
                            AccountID = temp0.agent.AccountID,
                            AccountNumber = temp0.account.AccountNo,
                            EnglishName = temp0.agent.EnglishName,
                            Phone = temp0.agent.Phone,
                            Fax = temp0.agent.Fax,
                            Adress = temp0.agent.Adress,
                            Notes = temp0.agent.Notes,
                            RecordStatus = temp0.agent.RecordStatus
                        }
                );
        }
    }
}
