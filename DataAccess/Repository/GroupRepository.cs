﻿using DataAccess.IRepository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataAccess.Models;
using DomainModels;

namespace DataAccess.Repository
{
    public class GroupRepository : GenericRepository<Group>, IGroupRepository
    {
        public GroupRepository(CBAContext context) : base(context)
        {

        }

        public IEnumerable<GroupModel> GetGroups()
        {
            return _cbaContext.Groups
                  .Join(
                      _cbaContext.GroupTypes,
                      grp => grp.GroupTypeID,
                      groupType => groupType.GroupTypeID,
                      (grp, groupType) =>
                          new
                          {
                              grp = grp,
                              groupType = groupType
                          }
                  )
                  .Where(temp0 => (temp0.grp.RecordStatus != "D"))
                  .Select(
                      temp0 =>
                          new GroupModel()
                          {
                              GroupID = temp0.grp.GroupID,
                              GroupName = temp0.grp.GroupName,
                              GroupTypeID = temp0.grp.GroupTypeID,
                              Notes = temp0.grp.Notes,
                              GroupTypeName = temp0.groupType.GroupTypeName,
                              RecordStatus = temp0.groupType.RecordStatus
                          }
                  );
        }
    }
}
