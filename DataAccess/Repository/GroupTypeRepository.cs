﻿using DataAccess.IRepository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataAccess.Models;

namespace DataAccess.Repository
{
    public class GroupTypeRepository : GenericRepository<GroupType>, IGroupTypeRepository
    {
        public GroupTypeRepository(CBAContext context) : base(context)
        {

        }
    }
}
