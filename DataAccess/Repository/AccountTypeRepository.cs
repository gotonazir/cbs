﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataAccess.IRepository;
using DataAccess.Models;

namespace DataAccess.Repository
{
   public class AccountTypeRepository : GenericRepository<AccountType>, IAccountTypeRepository
   {
       public AccountTypeRepository(CBAContext context) : base(context)
       {

       }
   }
}
