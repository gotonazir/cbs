﻿using DataAccess.IRepository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataAccess.Models;
using DomainModels;

namespace DataAccess.Repository
{
    public class AgentUserRepository : GenericRepository<AgentUser>, IAgentUserRepository
    {
        public AgentUserRepository(CBAContext context) : base(context)
        {

        }

        public IEnumerable<AgentUserModel> GetAgentUsers()
        {
            return _cbaContext.AgentUsers
                .Join(
                    _cbaContext.Agents,
                    agentUser => agentUser.AgentID,
                    agent => agent.AgentID,
                    (agentUser, agent) =>
                        new
                        {
                            agentUser = agentUser,
                            agent = agent
                        }
                )
                .Where(temp0 => (temp0.agentUser.RecordStatus != "D"))
                .Select(
                    temp0 =>
                        new AgentUserModel()
                        {
                            AgentUserID = temp0.agentUser.AgentUserID,
                            AgentID = temp0.agentUser.AgentID,
                            UserName = temp0.agentUser.UserName,
                            Notes = temp0.agentUser.Notes,
                            AgentName = temp0.agent.AgentName,
                            RecordStatus = temp0.agentUser.RecordStatus
                        }
                );
        }
    }
}
