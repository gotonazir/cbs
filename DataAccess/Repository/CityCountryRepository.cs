﻿using DataAccess.IRepository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataAccess.Models;

namespace DataAccess.Repository
{
    public class CityCountryRepository : GenericRepository<CityCountry>, ICityCountryRepository
    {
        public CityCountryRepository(CBAContext context) : base(context)
        {

        }
    }
}
