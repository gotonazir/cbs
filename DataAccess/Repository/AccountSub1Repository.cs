﻿using DataAccess.IRepository;
using DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Repository
{
    public class AccountSub1Repository : GenericRepository<AccountTypeSub1>, IAccountSub1Repository
    {
        public AccountSub1Repository(CBAContext context) : base(context)
        {

        }
    }
}
