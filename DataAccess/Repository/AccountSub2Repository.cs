﻿using DataAccess.IRepository;
using DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Repository
{
    public class AccountSub2Repository : GenericRepository<AccountTypeSub2>, IAccountSub2Repository
    {
        public AccountSub2Repository(CBAContext context) : base(context)
        {

        }
    }
}
