﻿using DataAccess.IRepository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataAccess.Models;
using DomainModels;

namespace DataAccess.Repository
{
    public class AgentAreaRepository : GenericRepository<AgentArea>, IAgentAreaRepository
    {
        public AgentAreaRepository(CBAContext context) : base(context)
        {

        }

        public IEnumerable<AgentAreaModel> GetAgentAreas()
        {
            return _cbaContext.AgentAreas
                .Join(
                    _cbaContext.CityCountries,
                    agentArea => agentArea.CityID,
                    city => (Int32?)(city.CityID),
                    (agentArea, city) =>
                        new
                        {
                            agentArea = agentArea,
                            city = city
                        }
                )
                .Join(
                    _cbaContext.Agents,
                    temp0 => temp0.agentArea.AgentID,
                    agnt => agnt.AgentID,
                    (temp0, agnt) =>
                        new AgentAreaModel
                        {
                            AgentAreaID = temp0.agentArea.AgentAreaID,
                            AreaNo = temp0.agentArea.AreaNo,
                            AreaName = temp0.agentArea.AreaName,
                            AgentID = temp0.agentArea.AgentID,
                            CityID = temp0.agentArea.CityID,
                            TranactionTypeID = temp0.agentArea.TranactionTypeID,
                            LocalOrintl = temp0.agentArea.LocalOrintl,
                            Phone = temp0.agentArea.Phone,
                            Fax = temp0.agentArea.fax,
                            Adress = temp0.agentArea.Adress,
                            RequiredApproval = temp0.agentArea.RequiredApproval,
                            DirectExchange = temp0.agentArea.DirectExchange,
                            FreezArea = temp0.agentArea.FreezArea,
                            Notes = temp0.agentArea.Notes,
                            AgentName = agnt.AgentName,
                            CityName = temp0.city.CityName
                        }
                );



        }
    }
}
