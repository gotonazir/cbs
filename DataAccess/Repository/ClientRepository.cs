﻿using DataAccess.IRepository;
using DataAccess.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DomainModels;

namespace DataAccess.Repository
{
    public class ClientRepository : GenericRepository<Client>, IClientRepository
    {
        public ClientRepository(CBAContext context) : base(context)
        {

        }

        public Client GetClientById(long id)
        {
            return _cbaContext.Clients.Where(x => x.ClientID == id).Include("ClientDocuments").FirstOrDefault();
        }


        public IEnumerable<ClientModel> GetClients()
        {
            return _cbaContext.Clients
                .Join(
                    _cbaContext.CityCountries,
                    client => client.CityID,
                    city => city.CityID,
                    (client, city) =>
                        new
                        {
                            client = client,
                            city = city
                        }
                )
                .GroupJoin(
                    _cbaContext.Clients,
                    temp0 => (Int64?) (temp0.client.ClientID),
                    guarantor => guarantor.GuarantorID,
                    (temp0, guarantorLeft) =>
                        new
                        {
                            temp0 = temp0,
                            guarantorLeft = guarantorLeft
                        }
                )
                .SelectMany(
                    temp1 => temp1.guarantorLeft.DefaultIfEmpty(),
                    (temp1, gLeft) =>
                        new
                        {
                            temp1 = temp1,
                            gLeft = gLeft
                        }
                )
                .GroupJoin(
                    _cbaContext.Clients,
                    temp2 => temp2.temp1.temp0.client.ClientID,
                    prev => (Int64) (prev.PrivilegeID),
                    (temp2, previlageLeft) =>
                        new
                        {
                            temp2 = temp2,
                            previlageLeft = previlageLeft
                        }
                )
                .SelectMany(
                    temp3 => temp3.previlageLeft.DefaultIfEmpty(),
                    (temp3, preLeft) =>
                        new
                        {
                            temp3 = temp3,
                            preLeft = preLeft
                        }
                )
                .Where(temp4 => (temp4.temp3.temp2.temp1.temp0.client.RecordStatus != "D"))
                .Select(
                    temp4 =>
                        new ClientModel()
                        {
                            ClientID = temp4.temp3.temp2.temp1.temp0.client.ClientID,
                            FullName = temp4.temp3.temp2.temp1.temp0.client.FullName,
                            Address = temp4.temp3.temp2.temp1.temp0.client.Address,
                            CityName = temp4.temp3.temp2.temp1.temp0.city.CityName,
                            Phone = temp4.temp3.temp2.temp1.temp0.client.Phone,
                            Mobile = temp4.temp3.temp2.temp1.temp0.client.Mobile,
                            Fax = temp4.temp3.temp2.temp1.temp0.client.Fax,
                            PersonOrCorp = temp4.temp3.temp2.temp1.temp0.client.PersonOrCorp,
                            GuarantorFullName = temp4.temp3.temp2.gLeft.FullName,
                            PrivilegeFullName = temp4.preLeft.FullName,
                            Notes = temp4.temp3.temp2.temp1.temp0.client.Notes,
                            RecordStatus = temp4.temp3.temp2.temp1.temp0.client.RecordStatus
                        }
                );
        }
    }
}
