﻿using DataAccess.IRepository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataAccess.Models;
using DomainModels;

namespace DataAccess.Repository
{
    public class AgentGroupRepository : GenericRepository<AgentGroup>, IAgentGroupRepository
    {
        public AgentGroupRepository(CBAContext context) : base(context)
        {

        }

        public IEnumerable<AgentGroupModel> GetAgentGroups()
        {
            return _cbaContext.AgentGroups
                 .Join(
                     _cbaContext.Groups,
                     agentGroup => agentGroup.GroupID,
                     grp => grp.GroupID,
                     (agentGroup, grp) =>
                         new
                         {
                             agentGroup = agentGroup,
                             grp = grp
                         }
                 )
                 .Join(
                     _cbaContext.Agents,
                     temp0 => temp0.agentGroup.AgentID,
                     agent => agent.AgentID,
                     (temp0, agent) =>
                         new AgentGroupModel
                         {
                             AgentGroupID = temp0.agentGroup.AgentGroupID,
                             AgentID = temp0.agentGroup.AgentID,
                             GroupID = temp0.agentGroup.GroupID,
                             RecordStatus = temp0.agentGroup.RecordStatus,
                             GroupName = temp0.grp.GroupName,
                             AgentName = agent.AgentName
                         }
                 );
        }
    }
}
