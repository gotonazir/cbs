﻿using DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataAccess.IRepository;

namespace DataAccess.Repository
{
   public class UserRepository : GenericRepository<User>,IUserRepository
    {
        public UserRepository(CBAContext context) : base(context)
        {
            
        }
    }
}
