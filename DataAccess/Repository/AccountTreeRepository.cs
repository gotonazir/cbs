﻿using DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataAccess.IRepository;

namespace DataAccess.Repository
{
   public class AccountTreeRepository:GenericRepository<AccountTree>, IAccountTreeRepository
    {
        public AccountTreeRepository(CBAContext context) : base(context)
        {
           
        }

        public IQueryable<AccountTree> GetAccountTrees()
        {
            // TODO make include as generic repository
          return  _cbaContext.AccountTrees.Include("AccountTypes");
        }
    }
}
