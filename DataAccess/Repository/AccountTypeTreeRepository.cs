﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataAccess.IRepository;
using DataAccess.Models;

namespace DataAccess.Repository
{
    public class AccountTypeTreeRepository : GenericRepository<AccountTypeTree>, IAccountTypeTreeRepository
    {
        public AccountTypeTreeRepository(CBAContext context) : base(context)
        {
            RecordStatus = "A";
        }

        public int AccountTypeTreeId { get; set; }
        public int ParentId { get; set; }
        public string Description { get; set; }
        public Nullable<int> Value { get; set; }
        public Nullable<long> AccountRange { get; set; }
        public Nullable<long> CurrentAccountNo { get; set; }
        public string RecordStatus { get; set; }
    }
}
    