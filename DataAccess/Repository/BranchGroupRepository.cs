﻿using DataAccess.IRepository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataAccess.Models;

namespace DataAccess.Repository
{
    public class BranchGroupRepository : GenericRepository<BranchGroup>, IBranchGroupRepository
    {
        public BranchGroupRepository(CBAContext context) : base(context)
        {

        }
    }
}
