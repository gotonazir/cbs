﻿using DataAccess.IRepository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataAccess.Models;
using DomainModels;

namespace DataAccess.Repository
{
    public class SandooqRepository : GenericRepository<Sandooq>, ISandooqRepository
    {
        public SandooqRepository(CBAContext context) : base(context)
        {

        }

        public IEnumerable<SandooqModel> GetSandooqs()
        {
            return _cbaContext.Sandooqs
                  .Join(
                      _cbaContext.Accounts,
              sandooq => sandooq.AccountID,
              account => account.AccountID,
              (sandooq, account) =>
                  new
                  {
                      sandooq = sandooq,
                      account = account
                  }
                  )
                  .Where(temp0 => (temp0.sandooq.RecordStatus != "D"))
                  .Select(
                      temp0 =>
                          new SandooqModel()
                          {
                              SandooqID = temp0.sandooq.SandooqID,
                              AccountID = temp0.sandooq.AccountID,
                              TransactionType = temp0.sandooq.TransactionType,
                              Notes = temp0.sandooq.Notes,
                              RecordStatus = temp0.sandooq.RecordStatus,
                              AccountNo = temp0.account.AccountNo
                          }
                  );
        }
    }
}
