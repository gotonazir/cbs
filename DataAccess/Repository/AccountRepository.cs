﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using DataAccess.IRepository;
using DataAccess.Models;
using DomainModels;

namespace DataAccess.Repository
{
    public class AccountRepository : GenericRepository<Account>, IAccountRepository
    {
        public AccountRepository(CBAContext context) : base(context)
        {
        }

        public IQueryable<AccountModel> GetAccountDetails()
        {
            return _cbaContext.Accounts
                  .Join(
                      _cbaContext.AccountTypeTrees,
                      account => account.AccountTypeTreeId,
                      accountTypeTree => accountTypeTree.AccountTypeTreeId,
                      (account, accountTypeTree) =>
                          new
                          {
                              account = account,
                              accountTypeTree = accountTypeTree
                          }
                  )
                  .Join(
                      _cbaContext.Clients,
                      temp0 => temp0.account.ClientID,
                      clint => clint.ClientID,
                      (temp0, clint) =>
                          new
                          {
                              temp0 = temp0,
                              clint = clint
                          }
                  )
                  .GroupJoin(
                      _cbaContext.Clients,
                      temp1 => temp1.temp0.account.BeneficiaryID,
                      beneficiary => (Int64?)(beneficiary.ClientID),
                      (temp1, beneficiaryLeft) =>
                          new
                          {
                              temp1 = temp1,
                              beneficiaryLeft = beneficiaryLeft
                          }
                  )
                  .SelectMany(
                      temp2 => temp2.beneficiaryLeft.DefaultIfEmpty(),
                      (temp2, benLeft) =>
                          new
                          {
                              temp2 = temp2,
                              benLeft = benLeft
                          }
                  )
                  .GroupJoin(
                      _cbaContext.Clients,
                      temp3 => temp3.temp2.temp1.temp0.account.AuthorizedID,
                      authorized => (Int64?)(authorized.ClientID),
                      (temp3, authorizedLeft) =>
                          new
                          {
                              temp3 = temp3,
                              authorizedLeft = authorizedLeft
                          }
                  )
                  .SelectMany(
                      temp4 => temp4.authorizedLeft.DefaultIfEmpty(),
                      (temp4, authLeft) =>
                          new
                          {
                              temp4 = temp4,
                              authLeft = authLeft
                          }
                  )
                  .Join(
                      _cbaContext.Branches,
                      temp5 => temp5.temp4.temp3.temp2.temp1.temp0.account.BranchID,
                      branch => branch.BranchID,
                      (temp5, branch) =>
                          new
                          {
                              temp5 = temp5,
                              branch = branch
                          }
                  )
                  .Where(temp6 => (temp6.temp5.temp4.temp3.temp2.temp1.temp0.account.RecordStatus != "D"))
                  .Select(
                      temp6 =>
                          new AccountModel()
                          {
                              AccountType = temp6.temp5.temp4.temp3.temp2.temp1.temp0.accountTypeTree.Description,
                              BranchName = temp6.branch.BranchName,
                              ClientName = temp6.temp5.temp4.temp3.temp2.temp1.clint.FullName,
                              Beneficiary = temp6.temp5.temp4.temp3.benLeft.FullName,
                              Authorized = temp6.temp5.authLeft.FullName,
                              OpenDt = temp6.temp5.temp4.temp3.temp2.temp1.temp0.account.OpenDt,
                              VIPNo = temp6.temp5.temp4.temp3.temp2.temp1.temp0.account.VIPNo,
                              Notes = temp6.temp5.temp4.temp3.temp2.temp1.temp0.account.Notes,
                              CloseDt = temp6.temp5.temp4.temp3.temp2.temp1.temp0.account.CloseDt,
                              RecordStatus = temp6.temp5.temp4.temp3.temp2.temp1.temp0.account.RecordStatus
                          }
                  );
        }

        public Account GetAccountById(long id)
        {
            return _cbaContext.Accounts.Where(x => x.AccountID == id).Include("AccountCurrencies").FirstOrDefault();
        }
    }
}
