﻿using DataAccess.Models;
using DomainModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.IRepository
{
    public interface IAgentGroupRepository : IGenericRepository<AgentGroup>
    {
        IEnumerable<AgentGroupModel> GetAgentGroups();
    }
}
