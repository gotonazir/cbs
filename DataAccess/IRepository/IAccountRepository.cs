﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataAccess.Models;
using DomainModels;

namespace DataAccess.IRepository
{
    public interface IAccountRepository  : IGenericRepository<Account>
    {
        IQueryable<AccountModel> GetAccountDetails();
        Account GetAccountById(long id);
    }
}
