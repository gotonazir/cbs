﻿using DataAccess.Models;
using DomainModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.IRepository
{
    public interface IGroupRepository : IGenericRepository<Group>
    {
        IEnumerable<GroupModel> GetGroups();
    }
}
