using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using DataAccess.Models;
using DataAccess.Models.Mapping;

namespace DataAccess
{
    public interface ICBAContext
    {

         DbSet<Account> Accounts { get; set; }
         DbSet<AccountCurrency> AccountCurrencies { get; set; }
         DbSet<AccountGroup> AccountGroups { get; set; }
         DbSet<AccountNoRange> AccountNoRanges { get; set; }
         DbSet<AccountTree> AccountTrees { get; set; }
         DbSet<AccountType> AccountTypes { get; set; }
         DbSet<AccountTypeDetail> AccountTypeDetails { get; set; }
         DbSet<AccountTypeSub1> AccountTypeSub1 { get; set; }
         DbSet<AccountTypeSub2> AccountTypeSub2 { get; set; }
         DbSet<Agent> Agents { get; set; }
         DbSet<AgentArea> AgentAreas { get; set; }
         DbSet<AgentGroup> AgentGroups { get; set; }
         DbSet<AgentUser> AgentUsers { get; set; }
         DbSet<BankTransaction> BankTransactions { get; set; }
         DbSet<Branch> Branches { get; set; }
         DbSet<BranchGroup> BranchGroups { get; set; }
         DbSet<CityCountry> CityCountries { get; set; }
         DbSet<Client> Clients { get; set; }
         DbSet<ClientDocument> ClientDocuments { get; set; }
         DbSet<Currency> Currencies { get; set; }
         DbSet<CurrencyExchangeRate> CurrencyExchangeRates { get; set; }
         DbSet<CurrencyGroup> CurrencyGroups { get; set; }
         DbSet<Group> Groups { get; set; }
         DbSet<GroupType> GroupTypes { get; set; }
         DbSet<HawalaTransaction> HawalaTransactions { get; set; }
         DbSet<Sandooq> Sandooqs { get; set; }
         DbSet<SandooqReconciliation> SandooqReconciliations { get; set; }
         DbSet<User> Users { get; set; }
         DbSet<UserBranch> UserBranches { get; set; }
         DbSet<UserGroup> UserGroups { get; set; }
         DbSet<UserSession> UserSessions { get; set; }
    }
}
