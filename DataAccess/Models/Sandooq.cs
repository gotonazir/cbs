using System;
using System.Collections.Generic;

namespace DataAccess.Models
{
    public partial class Sandooq
    {
        public Sandooq()
        {
            this.BankTransactions = new List<BankTransaction>();
            this.HawalaTransactions = new List<HawalaTransaction>();
        }

        public int SandooqID { get; set; }
        public long AccountID { get; set; }
        public string TransactionType { get; set; }
        public string Notes { get; set; }
        public string RecordStatus { get; set; }
        public virtual Account Account { get; set; }
        public virtual ICollection<BankTransaction> BankTransactions { get; set; }
        public virtual ICollection<HawalaTransaction> HawalaTransactions { get; set; }
    }
}
