using System;
using System.Collections.Generic;

namespace DataAccess.Models
{
    public partial class Agent
    {
        public Agent()
        {
            this.AgentAreas = new List<AgentArea>();
            this.AgentGroups = new List<AgentGroup>();
            this.AgentUsers = new List<AgentUser>();
            this.HawalaTransactions = new List<HawalaTransaction>();
            this.HawalaTransactions1 = new List<HawalaTransaction>();
        }

        public int AgentID { get; set; }
        public int AgentNo { get; set; }
        public string AgentName { get; set; }
        public long AccountID { get; set; }
        public string EnglishName { get; set; }
        public string Phone { get; set; }
        public string Fax { get; set; }
        public string Adress { get; set; }
        public string Notes { get; set; }
        public string RecordStatus { get; set; }
        public virtual ICollection<AgentArea> AgentAreas { get; set; }
        public virtual ICollection<AgentGroup> AgentGroups { get; set; }
        public virtual ICollection<AgentUser> AgentUsers { get; set; }
        public virtual ICollection<HawalaTransaction> HawalaTransactions { get; set; }
        public virtual ICollection<HawalaTransaction> HawalaTransactions1 { get; set; }
    }
}
