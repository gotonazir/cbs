using System;
using System.Collections.Generic;

namespace DataAccess.Models
{
    public partial class AccountTree
    {
        public AccountTree()
        {
            this.AccountNoRanges = new List<AccountNoRange>();
            this.AccountTypes = new List<AccountType>();
            this.AccountTypeDetails = new List<AccountTypeDetail>();
        }

        public int AccountTreeID { get; set; }
        public string Name { get; set; }
        public virtual ICollection<AccountNoRange> AccountNoRanges { get; set; }
        public virtual ICollection<AccountType> AccountTypes { get; set; }
        public virtual ICollection<AccountTypeDetail> AccountTypeDetails { get; set; }
    }
}
