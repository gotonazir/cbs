using System;
using System.Collections.Generic;

namespace DataAccess.Models
{
    public partial class AccountType
    {
        public AccountType()
        {
            this.AccountNoRanges = new List<AccountNoRange>();
            this.AccountTypeDetails = new List<AccountTypeDetail>();
            this.AccountTypeSub1 = new List<AccountTypeSub1>();
        }

        public int AccountTypeID { get; set; }
        public string Name { get; set; }
        public int AccountTreeID { get; set; }
        public string Notes { get; set; }
        public string RecordStatus { get; set; }
        public virtual ICollection<AccountNoRange> AccountNoRanges { get; set; }
        public virtual AccountTree AccountTree { get; set; }
        public virtual ICollection<AccountTypeDetail> AccountTypeDetails { get; set; }
        public virtual ICollection<AccountTypeSub1> AccountTypeSub1 { get; set; }
    }
}
