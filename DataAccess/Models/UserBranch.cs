using System;
using System.Collections.Generic;

namespace DataAccess.Models
{
    public partial class UserBranch
    {
        public int UserBranchID { get; set; }
        public int UserID { get; set; }
        public int BranchID { get; set; }
        public Nullable<bool> DefaultBranch { get; set; }
        public string RecordStatus { get; set; }
        public virtual Branch Branch { get; set; }
        public virtual User User { get; set; }
    }
}
