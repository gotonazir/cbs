using System;
using System.Collections.Generic;

namespace DataAccess.Models
{
    public partial class ClientDocument
    {
        public long ClientDocumentID { get; set; }
        public long ClientID { get; set; }
        public string DocumentType { get; set; }
        public string PlaceOfIssue { get; set; }
        public Nullable<System.DateTime> IssueDt { get; set; }
        public Nullable<System.DateTime> ExpiryDt { get; set; }
        public byte[] DocumentImage { get; set; }
        public string RecordStatus { get; set; }
        public virtual Client Client { get; set; }
    }
}
