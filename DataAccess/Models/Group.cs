using System;
using System.Collections.Generic;

namespace DataAccess.Models
{
    public partial class Group
    {
        public Group()
        {
            this.AccountGroups = new List<AccountGroup>();
        }

        public int GroupID { get; set; }
        public string GroupName { get; set; }
        public int GroupTypeID { get; set; }
        public string Notes { get; set; }
        public string RecordStatus { get; set; }
        public virtual ICollection<AccountGroup> AccountGroups { get; set; }
        public virtual GroupType GroupType { get; set; }
    }
}
