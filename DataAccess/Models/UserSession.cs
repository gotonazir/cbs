using System;
using System.Collections.Generic;

namespace DataAccess.Models
{
    public partial class UserSession
    {
        public long UserSessionID { get; set; }
        public int UserID { get; set; }
        public System.DateTime LoginDt { get; set; }
        public System.DateTime Logout { get; set; }
        public int BranchID { get; set; }
        public string IP { get; set; }
        public string MacAddress { get; set; }
        public string RecordStatus { get; set; }
        public virtual Branch Branch { get; set; }
        public virtual User User { get; set; }
    }
}
