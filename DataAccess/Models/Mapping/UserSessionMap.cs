using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace DataAccess.Models.Mapping
{
    public class UserSessionMap : EntityTypeConfiguration<UserSession>
    {
        public UserSessionMap()
        {
            // Primary Key
            this.HasKey(t => t.UserSessionID);

            // Properties
            this.Property(t => t.IP)
                .HasMaxLength(16);

            this.Property(t => t.MacAddress)
                .HasMaxLength(50);

            this.Property(t => t.RecordStatus)
                .IsFixedLength()
                .HasMaxLength(1);

            // Table & Column Mappings
            this.ToTable("UserSession");
            this.Property(t => t.UserSessionID).HasColumnName("UserSessionID");
            this.Property(t => t.UserID).HasColumnName("UserID");
            this.Property(t => t.LoginDt).HasColumnName("LoginDt");
            this.Property(t => t.Logout).HasColumnName("Logout");
            this.Property(t => t.BranchID).HasColumnName("BranchID");
            this.Property(t => t.IP).HasColumnName("IP");
            this.Property(t => t.MacAddress).HasColumnName("MacAddress");
            this.Property(t => t.RecordStatus).HasColumnName("RecordStatus");

            // Relationships
            this.HasRequired(t => t.Branch)
                .WithMany(t => t.UserSessions)
                .HasForeignKey(d => d.BranchID);
            this.HasRequired(t => t.User)
                .WithMany(t => t.UserSessions)
                .HasForeignKey(d => d.UserID);

        }
    }
}
