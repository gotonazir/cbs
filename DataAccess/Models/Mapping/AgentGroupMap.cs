using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace DataAccess.Models.Mapping
{
    public class AgentGroupMap : EntityTypeConfiguration<AgentGroup>
    {
        public AgentGroupMap()
        {
            // Primary Key
            this.HasKey(t => t.AgentGroupID);

            // Properties
            this.Property(t => t.RecordStatus)
                .IsFixedLength()
                .HasMaxLength(1);

            // Table & Column Mappings
            this.ToTable("AgentGroup");
            this.Property(t => t.AgentGroupID).HasColumnName("AgentGroupID");
            this.Property(t => t.AgentID).HasColumnName("AgentID");
            this.Property(t => t.GroupID).HasColumnName("GroupID");
            this.Property(t => t.RecordStatus).HasColumnName("RecordStatus");

            // Relationships
            this.HasRequired(t => t.Agent)
                .WithMany(t => t.AgentGroups)
                .HasForeignKey(d => d.AgentID);

        }
    }
}
