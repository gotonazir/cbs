using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace DataAccess.Models.Mapping
{
    public class AccountTreeMap : EntityTypeConfiguration<AccountTree>
    {
        public AccountTreeMap()
        {
            // Primary Key
            this.HasKey(t => t.AccountTreeID);

            // Properties
            this.Property(t => t.Name)
                .IsRequired()
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("AccountTree");
            this.Property(t => t.AccountTreeID).HasColumnName("AccountTreeID");
            this.Property(t => t.Name).HasColumnName("Name");
        }
    }
}
