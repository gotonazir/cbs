using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace DataAccess.Models.Mapping
{
    public class AgentAreaMap : EntityTypeConfiguration<AgentArea>
    {
        public AgentAreaMap()
        {
            // Primary Key
            this.HasKey(t => t.AgentAreaID);

            // Properties
            this.Property(t => t.AreaName)
                .IsRequired()
                .HasMaxLength(100);

            this.Property(t => t.LocalOrintl)
                .IsFixedLength()
                .HasMaxLength(1);

            this.Property(t => t.Phone)
                .HasMaxLength(60);

            this.Property(t => t.fax)
                .HasMaxLength(20);

            this.Property(t => t.Adress)
                .HasMaxLength(100);

            this.Property(t => t.RequiredApproval)
                .IsFixedLength()
                .HasMaxLength(1);

            this.Property(t => t.DirectExchange)
                .HasMaxLength(150);

            this.Property(t => t.FreezArea)
                .IsFixedLength()
                .HasMaxLength(1);

            this.Property(t => t.Notes)
                .HasMaxLength(200);

            // Table & Column Mappings
            this.ToTable("AgentArea");
            this.Property(t => t.AgentAreaID).HasColumnName("AgentAreaID");
            this.Property(t => t.AreaNo).HasColumnName("AreaNo");
            this.Property(t => t.AreaName).HasColumnName("AreaName");
            this.Property(t => t.AgentID).HasColumnName("AgentID");
            this.Property(t => t.CityID).HasColumnName("CityID");
            this.Property(t => t.TranactionTypeID).HasColumnName("TranactionTypeID");
            this.Property(t => t.LocalOrintl).HasColumnName("LocalOrintl");
            this.Property(t => t.Phone).HasColumnName("Phone");
            this.Property(t => t.fax).HasColumnName("fax");
            this.Property(t => t.Adress).HasColumnName("Adress");
            this.Property(t => t.RequiredApproval).HasColumnName("RequiredApproval");
            this.Property(t => t.DirectExchange).HasColumnName("DirectExchange");
            this.Property(t => t.FreezArea).HasColumnName("FreezArea");
            this.Property(t => t.Notes).HasColumnName("Notes");

            // Relationships
            this.HasRequired(t => t.Agent)
                .WithMany(t => t.AgentAreas)
                .HasForeignKey(d => d.AgentID);
            this.HasOptional(t => t.CityCountry)
                .WithMany(t => t.AgentAreas)
                .HasForeignKey(d => d.CityID);

        }
    }
}
