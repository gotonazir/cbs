using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace DataAccess.Models.Mapping
{
    public class AgentUserMap : EntityTypeConfiguration<AgentUser>
    {
        public AgentUserMap()
        {
            // Primary Key
            this.HasKey(t => t.AgentUserID);

            // Properties
            this.Property(t => t.UserName)
                .IsRequired()
                .HasMaxLength(100);

            this.Property(t => t.Notes)
                .HasMaxLength(200);

            this.Property(t => t.RecordStatus)
                .IsFixedLength()
                .HasMaxLength(1);

            // Table & Column Mappings
            this.ToTable("AgentUser");
            this.Property(t => t.AgentUserID).HasColumnName("AgentUserID");
            this.Property(t => t.AgentID).HasColumnName("AgentID");
            this.Property(t => t.UserName).HasColumnName("UserName");
            this.Property(t => t.Notes).HasColumnName("Notes");
            this.Property(t => t.RecordStatus).HasColumnName("RecordStatus");

            // Relationships
            this.HasRequired(t => t.Agent)
                .WithMany(t => t.AgentUsers)
                .HasForeignKey(d => d.AgentID);

        }
    }
}
