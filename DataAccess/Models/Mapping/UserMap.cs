using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace DataAccess.Models.Mapping
{
    public class UserMap : EntityTypeConfiguration<User>
    {
        public UserMap()
        {
            // Primary Key
            this.HasKey(t => t.UserID);

            // Properties
            this.Property(t => t.UserName)
                .IsRequired()
                .HasMaxLength(100);

            this.Property(t => t.PassWord)
                .HasMaxLength(8);

            this.Property(t => t.UserRole)
                .HasMaxLength(50);

            this.Property(t => t.BlockedStatus)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(1);

            this.Property(t => t.Title)
                .HasMaxLength(50);

            this.Property(t => t.Notes)
                .HasMaxLength(200);

            this.Property(t => t.RecordStatus)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(1);

            // Table & Column Mappings
            this.ToTable("User");
            this.Property(t => t.UserID).HasColumnName("UserID");
            this.Property(t => t.UserNo).HasColumnName("UserNo");
            this.Property(t => t.UserName).HasColumnName("UserName");
            this.Property(t => t.PassWord).HasColumnName("PassWord");
            this.Property(t => t.UserRole).HasColumnName("UserRole");
            this.Property(t => t.DefaultBranchID).HasColumnName("DefaultBranchID");
            this.Property(t => t.BlockedStatus).HasColumnName("BlockedStatus");
            this.Property(t => t.ChangePasswordDays).HasColumnName("ChangePasswordDays");
            this.Property(t => t.ChangePasswordDt).HasColumnName("ChangePasswordDt");
            this.Property(t => t.Title).HasColumnName("Title");
            this.Property(t => t.Salary).HasColumnName("Salary");
            this.Property(t => t.Notes).HasColumnName("Notes");
            this.Property(t => t.RecordStatus).HasColumnName("RecordStatus");
        }
    }
}
