using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace DataAccess.Models.Mapping
{
    public class BankTransactionMap : EntityTypeConfiguration<BankTransaction>
    {
        public BankTransactionMap()
        {
            // Primary Key
            this.HasKey(t => t.BTID);

            // Properties
            this.Property(t => t.PersonName)
                .HasMaxLength(100);

            this.Property(t => t.Notes)
                .HasMaxLength(150);

            this.Property(t => t.LockTransaction)
                .IsFixedLength()
                .HasMaxLength(1);

            this.Property(t => t.RecordStatus)
                .IsFixedLength()
                .HasMaxLength(1);

            // Table & Column Mappings
            this.ToTable("BankTransaction");
            this.Property(t => t.BTID).HasColumnName("BTID");
            this.Property(t => t.TransactionMode).HasColumnName("TransactionMode");
            this.Property(t => t.ReceiptNo).HasColumnName("ReceiptNo");
            this.Property(t => t.AmountReceived).HasColumnName("AmountReceived");
            this.Property(t => t.CurrencyIDReceived).HasColumnName("CurrencyIDReceived");
            this.Property(t => t.HomeBranchID).HasColumnName("HomeBranchID");
            this.Property(t => t.AccountID).HasColumnName("AccountID");
            this.Property(t => t.PersonName).HasColumnName("PersonName");
            this.Property(t => t.SandooqID).HasColumnName("SandooqID");
            this.Property(t => t.TransactionDate).HasColumnName("TransactionDate");
            this.Property(t => t.TrasactionBranchID).HasColumnName("TrasactionBranchID");
            this.Property(t => t.CommissionAmount).HasColumnName("CommissionAmount");
            this.Property(t => t.CommissionCurrencyID).HasColumnName("CommissionCurrencyID");
            this.Property(t => t.ToBranchID).HasColumnName("ToBranchID");
            this.Property(t => t.ToAccountID).HasColumnName("ToAccountID");
            this.Property(t => t.ToAmount).HasColumnName("ToAmount");
            this.Property(t => t.ToCurrencyID).HasColumnName("ToCurrencyID");
            this.Property(t => t.AmountCounted).HasColumnName("AmountCounted");
            this.Property(t => t.PrintReceipt).HasColumnName("PrintReceipt");
            this.Property(t => t.Notes).HasColumnName("Notes");
            this.Property(t => t.LockTransaction).HasColumnName("LockTransaction");
            this.Property(t => t.RecordStatus).HasColumnName("RecordStatus");

            // Relationships
            this.HasRequired(t => t.Account)
                .WithMany(t => t.BankTransactions)
                .HasForeignKey(d => d.AccountID);
            this.HasOptional(t => t.Account1)
                .WithMany(t => t.BankTransactions1)
                .HasForeignKey(d => d.ToAccountID);
            this.HasOptional(t => t.Branch)
                .WithMany(t => t.BankTransactions)
                .HasForeignKey(d => d.HomeBranchID);
            this.HasOptional(t => t.Branch1)
                .WithMany(t => t.BankTransactions1)
                .HasForeignKey(d => d.TrasactionBranchID);
            this.HasOptional(t => t.Branch2)
                .WithMany(t => t.BankTransactions2)
                .HasForeignKey(d => d.ToBranchID);
            this.HasOptional(t => t.Currency)
                .WithMany(t => t.BankTransactions)
                .HasForeignKey(d => d.CommissionCurrencyID);
            this.HasOptional(t => t.Currency1)
                .WithMany(t => t.BankTransactions1)
                .HasForeignKey(d => d.ToCurrencyID);
            this.HasOptional(t => t.Sandooq)
                .WithMany(t => t.BankTransactions)
                .HasForeignKey(d => d.SandooqID);

        }
    }
}
