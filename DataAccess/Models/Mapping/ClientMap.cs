using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace DataAccess.Models.Mapping
{
    public class ClientMap : EntityTypeConfiguration<Client>
    {
        public ClientMap()
        {
            // Primary Key
            this.HasKey(t => t.ClientID);

            // Properties
            this.Property(t => t.FullName)
                .IsRequired()
                .HasMaxLength(100);

            this.Property(t => t.Address)
                .IsRequired()
                .HasMaxLength(150);

            this.Property(t => t.Phone)
                .HasMaxLength(20);

            this.Property(t => t.Mobile)
                .IsRequired()
                .HasMaxLength(20);

            this.Property(t => t.Fax)
                .HasMaxLength(20);

            this.Property(t => t.PersonOrCorp)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(1);

            this.Property(t => t.Notes)
                .HasMaxLength(200);

            this.Property(t => t.TimeStamp)
                .IsFixedLength()
                .HasMaxLength(8)
                .IsRowVersion();

            this.Property(t => t.RecordStatus)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(1);

            // Table & Column Mappings
            this.ToTable("Client");
            this.Property(t => t.ClientID).HasColumnName("ClientID");
            this.Property(t => t.FullName).HasColumnName("FullName");
            this.Property(t => t.Address).HasColumnName("Address");
            this.Property(t => t.CityID).HasColumnName("CityID");
            this.Property(t => t.Phone).HasColumnName("Phone");
            this.Property(t => t.Mobile).HasColumnName("Mobile");
            this.Property(t => t.Fax).HasColumnName("Fax");
            this.Property(t => t.PersonOrCorp).HasColumnName("PersonOrCorp");
            this.Property(t => t.GuarantorID).HasColumnName("GuarantorID");
            this.Property(t => t.PrivilegeID).HasColumnName("PrivilegeID");
            this.Property(t => t.Notes).HasColumnName("Notes");
            this.Property(t => t.TimeStamp).HasColumnName("TimeStamp");
            this.Property(t => t.RecordStatus).HasColumnName("RecordStatus");
            this.Property(t => t.SignImage).HasColumnName("SignImage");

            // Relationships
            this.HasRequired(t => t.CityCountry)
                .WithMany(t => t.Clients)
                .HasForeignKey(d => d.CityID);

        }
    }
}
