using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace DataAccess.Models.Mapping
{
    public class BranchGroupMap : EntityTypeConfiguration<BranchGroup>
    {
        public BranchGroupMap()
        {
            // Primary Key
            this.HasKey(t => t.BranchGroupID);

            // Properties
            this.Property(t => t.RecordStatus)
                .IsFixedLength()
                .HasMaxLength(1);

            // Table & Column Mappings
            this.ToTable("BranchGroup");
            this.Property(t => t.BranchGroupID).HasColumnName("BranchGroupID");
            this.Property(t => t.BranchID).HasColumnName("BranchID");
            this.Property(t => t.GroupID).HasColumnName("GroupID");
            this.Property(t => t.RecordStatus).HasColumnName("RecordStatus");

            // Relationships
            this.HasRequired(t => t.Branch)
                .WithMany(t => t.BranchGroups)
                .HasForeignKey(d => d.BranchID);

        }
    }
}
