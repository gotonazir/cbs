using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace DataAccess.Models.Mapping
{
    public class AccountTypeSub1Map : EntityTypeConfiguration<AccountTypeSub1>
    {
        public AccountTypeSub1Map()
        {
            // Primary Key
            this.HasKey(t => t.AccountTypeSub1ID);

            // Properties
            this.Property(t => t.Name)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.Notes)
                .HasMaxLength(50);

            this.Property(t => t.RecordStatus)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(1);

            // Table & Column Mappings
            this.ToTable("AccountTypeSub1");
            this.Property(t => t.AccountTypeSub1ID).HasColumnName("AccountTypeSub1ID");
            this.Property(t => t.AccountTypeID).HasColumnName("AccountTypeID");
            this.Property(t => t.Name).HasColumnName("Name");
            this.Property(t => t.Notes).HasColumnName("Notes");
            this.Property(t => t.RecordStatus).HasColumnName("RecordStatus");

            // Relationships
            this.HasOptional(t => t.AccountType)
                .WithMany(t => t.AccountTypeSub1)
                .HasForeignKey(d => d.AccountTypeID);

        }
    }
}
