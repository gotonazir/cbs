using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace DataAccess.Models.Mapping
{
    public class AccountMap : EntityTypeConfiguration<Account>
    {
        public AccountMap()
        {
            // Primary Key
            this.HasKey(t => t.AccountID);

            // Properties
            this.Property(t => t.Notes)
                .HasMaxLength(200);

            this.Property(t => t.timestamp)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(8)
                .IsRowVersion();

            this.Property(t => t.RecordStatus)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(1);

            // Table & Column Mappings
            this.ToTable("Account");
            this.Property(t => t.AccountID).HasColumnName("AccountID");
            this.Property(t => t.AccountTypeTreeId).HasColumnName("AccountTypeTreeId");
            this.Property(t => t.AccountNo).HasColumnName("AccountNo");
            this.Property(t => t.BranchID).HasColumnName("BranchID");
            this.Property(t => t.ClientID).HasColumnName("ClientID");
            this.Property(t => t.OpenDt).HasColumnName("OpenDt");
            this.Property(t => t.VIPNo).HasColumnName("VIPNo");
            this.Property(t => t.BeneficiaryID).HasColumnName("BeneficiaryID");
            this.Property(t => t.AuthorizedID).HasColumnName("AuthorizedID");
            this.Property(t => t.Notes).HasColumnName("Notes");
            this.Property(t => t.CloseDt).HasColumnName("CloseDt");
            this.Property(t => t.timestamp).HasColumnName("timestamp");
            this.Property(t => t.RecordStatus).HasColumnName("RecordStatus");

            // Relationships
            this.HasRequired(t => t.AccountTypeTree)
                .WithMany(t => t.Accounts)
                .HasForeignKey(d => d.AccountTypeTreeId);
            this.HasRequired(t => t.Branch)
                .WithMany(t => t.Accounts)
                .HasForeignKey(d => d.BranchID);
            this.HasRequired(t => t.Client)
                .WithMany(t => t.Accounts)
                .HasForeignKey(d => d.ClientID);
            this.HasOptional(t => t.Client1)
                .WithMany(t => t.Accounts1)
                .HasForeignKey(d => d.BeneficiaryID);
            this.HasOptional(t => t.Client2)
                .WithMany(t => t.Accounts2)
                .HasForeignKey(d => d.AuthorizedID);

        }
    }
}
