using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace DataAccess.Models.Mapping
{
    public class CurrencyMap : EntityTypeConfiguration<Currency>
    {
        public CurrencyMap()
        {
            // Primary Key
            this.HasKey(t => t.CurrencyID);

            // Properties
            this.Property(t => t.Name)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.ShortNameAr)
                .HasMaxLength(50);

            this.Property(t => t.ShortNameEn)
                .HasMaxLength(50);

            this.Property(t => t.Notes)
                .HasMaxLength(200);

            // Table & Column Mappings
            this.ToTable("Currency");
            this.Property(t => t.CurrencyID).HasColumnName("CurrencyID");
            this.Property(t => t.Name).HasColumnName("Name");
            this.Property(t => t.ShortNameAr).HasColumnName("ShortNameAr");
            this.Property(t => t.ShortNameEn).HasColumnName("ShortNameEn");
            this.Property(t => t.Notes).HasColumnName("Notes");
        }
    }
}
