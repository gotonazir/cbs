using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace DataAccess.Models.Mapping
{
    public class AccountTypeTreeMap : EntityTypeConfiguration<AccountTypeTree>
    {
        public AccountTypeTreeMap()
        {
            // Primary Key
            this.HasKey(t => t.AccountTypeTreeId);

            // Properties
            this.Property(t => t.Description)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.RecordStatus)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(10);

            // Table & Column Mappings
            this.ToTable("AccountTypeTree");
            this.Property(t => t.AccountTypeTreeId).HasColumnName("AccountTypeTreeId");
            this.Property(t => t.ParentId).HasColumnName("ParentId");
            this.Property(t => t.Description).HasColumnName("Description");
            this.Property(t => t.Value).HasColumnName("Value");
            this.Property(t => t.AccountRange).HasColumnName("AccountRange");
            this.Property(t => t.CurrentAccountNo).HasColumnName("CurrentAccountNo");
            this.Property(t => t.RecordStatus).HasColumnName("RecordStatus");
        }
    }
}
