using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace DataAccess.Models.Mapping
{
    public class SandooqMap : EntityTypeConfiguration<Sandooq>
    {
        public SandooqMap()
        {
            // Primary Key
            this.HasKey(t => t.SandooqID);

            // Properties
            this.Property(t => t.TransactionType)
                .HasMaxLength(50);

            this.Property(t => t.Notes)
                .HasMaxLength(200);

            this.Property(t => t.RecordStatus)
                .IsFixedLength()
                .HasMaxLength(1);

            // Table & Column Mappings
            this.ToTable("Sandooq");
            this.Property(t => t.SandooqID).HasColumnName("SandooqID");
            this.Property(t => t.AccountID).HasColumnName("AccountID");
            this.Property(t => t.TransactionType).HasColumnName("TransactionType");
            this.Property(t => t.Notes).HasColumnName("Notes");
            this.Property(t => t.RecordStatus).HasColumnName("RecordStatus");

            // Relationships
            this.HasRequired(t => t.Account)
                .WithMany(t => t.Sandooqs)
                .HasForeignKey(d => d.AccountID);

        }
    }
}
