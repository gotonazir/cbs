using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace DataAccess.Models.Mapping
{
    public class SandooqReconciliationMap : EntityTypeConfiguration<SandooqReconciliation>
    {
        public SandooqReconciliationMap()
        {
            // Primary Key
            this.HasKey(t => t.SandooqReconciliationID);

            // Properties
            this.Property(t => t.Notes)
                .HasMaxLength(200);

            this.Property(t => t.RecordStatus)
                .IsFixedLength()
                .HasMaxLength(1);

            // Table & Column Mappings
            this.ToTable("SandooqReconciliation");
            this.Property(t => t.SandooqReconciliationID).HasColumnName("SandooqReconciliationID");
            this.Property(t => t.AccountCurrencyID).HasColumnName("AccountCurrencyID");
            this.Property(t => t.DailyBalance).HasColumnName("DailyBalance");
            this.Property(t => t.PhysicalBalance).HasColumnName("PhysicalBalance");
            this.Property(t => t.ReconciliationDt).HasColumnName("ReconciliationDt");
            this.Property(t => t.DifferenceDt).HasColumnName("DifferenceDt");
            this.Property(t => t.SettlementDt).HasColumnName("SettlementDt");
            this.Property(t => t.Notes).HasColumnName("Notes");
            this.Property(t => t.RecordStatus).HasColumnName("RecordStatus");

            // Relationships
            this.HasRequired(t => t.AccountCurrency)
                .WithMany(t => t.SandooqReconciliations)
                .HasForeignKey(d => d.AccountCurrencyID);

        }
    }
}
