using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace DataAccess.Models.Mapping
{
    public class ClientDocumentMap : EntityTypeConfiguration<ClientDocument>
    {
        public ClientDocumentMap()
        {
            // Primary Key
            this.HasKey(t => t.ClientDocumentID);

            // Properties
            this.Property(t => t.DocumentType)
                .HasMaxLength(50);

            this.Property(t => t.PlaceOfIssue)
                .HasMaxLength(50);

            this.Property(t => t.RecordStatus)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(1);

            // Table & Column Mappings
            this.ToTable("ClientDocument");
            this.Property(t => t.ClientDocumentID).HasColumnName("ClientDocumentID");
            this.Property(t => t.ClientID).HasColumnName("ClientID");
            this.Property(t => t.DocumentType).HasColumnName("DocumentType");
            this.Property(t => t.PlaceOfIssue).HasColumnName("PlaceOfIssue");
            this.Property(t => t.IssueDt).HasColumnName("IssueDt");
            this.Property(t => t.ExpiryDt).HasColumnName("ExpiryDt");
            this.Property(t => t.DocumentImage).HasColumnName("DocumentImage");
            this.Property(t => t.RecordStatus).HasColumnName("RecordStatus");

            // Relationships
            this.HasRequired(t => t.Client)
                .WithMany(t => t.ClientDocuments)
                .HasForeignKey(d => d.ClientID);

        }
    }
}
