using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace DataAccess.Models.Mapping
{
    public class CurrencyExchangeRateMap : EntityTypeConfiguration<CurrencyExchangeRate>
    {
        public CurrencyExchangeRateMap()
        {
            // Primary Key
            this.HasKey(t => t.CurrencyExchangeID);

            // Properties
            this.Property(t => t.Notes)
                .HasMaxLength(200);

            // Table & Column Mappings
            this.ToTable("CurrencyExchangeRate");
            this.Property(t => t.CurrencyExchangeID).HasColumnName("CurrencyExchangeID");
            this.Property(t => t.CurrencyIDFrom).HasColumnName("CurrencyIDFrom");
            this.Property(t => t.CurrencyIDTo).HasColumnName("CurrencyIDTo");
            this.Property(t => t.CentralBankRate).HasColumnName("CentralBankRate");
            this.Property(t => t.TransferMin).HasColumnName("TransferMin");
            this.Property(t => t.TransferMax).HasColumnName("TransferMax");
            this.Property(t => t.TransferRate).HasColumnName("TransferRate");
            this.Property(t => t.BuyMin).HasColumnName("BuyMin");
            this.Property(t => t.BuyMax).HasColumnName("BuyMax");
            this.Property(t => t.BuyRate).HasColumnName("BuyRate");
            this.Property(t => t.SellMin).HasColumnName("SellMin");
            this.Property(t => t.SellMax).HasColumnName("SellMax");
            this.Property(t => t.SellRate).HasColumnName("SellRate");
            this.Property(t => t.Notes).HasColumnName("Notes");

            // Relationships
            this.HasRequired(t => t.Currency)
                .WithMany(t => t.CurrencyExchangeRates)
                .HasForeignKey(d => d.CurrencyIDFrom);
            this.HasRequired(t => t.Currency1)
                .WithMany(t => t.CurrencyExchangeRates1)
                .HasForeignKey(d => d.CurrencyIDTo);

        }
    }
}
