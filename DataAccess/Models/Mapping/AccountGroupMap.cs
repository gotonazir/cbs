using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace DataAccess.Models.Mapping
{
    public class AccountGroupMap : EntityTypeConfiguration<AccountGroup>
    {
        public AccountGroupMap()
        {
            // Primary Key
            this.HasKey(t => t.AccountGroupID);

            // Properties
            this.Property(t => t.RecordStatus)
                .IsFixedLength()
                .HasMaxLength(1);

            // Table & Column Mappings
            this.ToTable("AccountGroup");
            this.Property(t => t.AccountGroupID).HasColumnName("AccountGroupID");
            this.Property(t => t.AccountID).HasColumnName("AccountID");
            this.Property(t => t.GroupID).HasColumnName("GroupID");
            this.Property(t => t.RecordStatus).HasColumnName("RecordStatus");

            // Relationships
            this.HasRequired(t => t.Account)
                .WithMany(t => t.AccountGroups)
                .HasForeignKey(d => d.AccountID);
            this.HasRequired(t => t.Group)
                .WithMany(t => t.AccountGroups)
                .HasForeignKey(d => d.GroupID);

        }
    }
}
