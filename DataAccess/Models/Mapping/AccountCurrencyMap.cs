using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace DataAccess.Models.Mapping
{
    public class AccountCurrencyMap : EntityTypeConfiguration<AccountCurrency>
    {
        public AccountCurrencyMap()
        {
            // Primary Key
            this.HasKey(t => t.AccountCurrencyID);

            // Properties
            this.Property(t => t.TimeStamp)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(8)
                .IsRowVersion();

            this.Property(t => t.RecordStatus)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(1);

            // Table & Column Mappings
            this.ToTable("AccountCurrency");
            this.Property(t => t.AccountCurrencyID).HasColumnName("AccountCurrencyID");
            this.Property(t => t.AccountID).HasColumnName("AccountID");
            this.Property(t => t.CurrencyID).HasColumnName("CurrencyID");
            this.Property(t => t.Balance).HasColumnName("Balance");
            this.Property(t => t.MinBalance).HasColumnName("MinBalance");
            this.Property(t => t.OverDraft).HasColumnName("OverDraft");
            this.Property(t => t.TimeStamp).HasColumnName("TimeStamp");
            this.Property(t => t.RecordStatus).HasColumnName("RecordStatus");

            // Relationships
            this.HasRequired(t => t.Account)
                .WithMany(t => t.AccountCurrencies)
                .HasForeignKey(d => d.AccountID);
            this.HasRequired(t => t.Currency)
                .WithMany(t => t.AccountCurrencies)
                .HasForeignKey(d => d.CurrencyID);

        }
    }
}
