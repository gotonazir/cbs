using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace DataAccess.Models.Mapping
{
    public class UserGroupMap : EntityTypeConfiguration<UserGroup>
    {
        public UserGroupMap()
        {
            // Primary Key
            this.HasKey(t => t.UserGroupID);

            // Properties
            this.Property(t => t.RecordStatus)
                .IsFixedLength()
                .HasMaxLength(1);

            // Table & Column Mappings
            this.ToTable("UserGroup");
            this.Property(t => t.UserGroupID).HasColumnName("UserGroupID");
            this.Property(t => t.GroupID).HasColumnName("GroupID");
            this.Property(t => t.UserID).HasColumnName("UserID");
            this.Property(t => t.RecordStatus).HasColumnName("RecordStatus");

            // Relationships
            this.HasRequired(t => t.User)
                .WithMany(t => t.UserGroups)
                .HasForeignKey(d => d.UserID);

        }
    }
}
