using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace DataAccess.Models.Mapping
{
    public class UserBranchMap : EntityTypeConfiguration<UserBranch>
    {
        public UserBranchMap()
        {
            // Primary Key
            this.HasKey(t => t.UserBranchID);

            // Properties
            this.Property(t => t.RecordStatus)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(1);

            // Table & Column Mappings
            this.ToTable("UserBranch");
            this.Property(t => t.UserBranchID).HasColumnName("UserBranchID");
            this.Property(t => t.UserID).HasColumnName("UserID");
            this.Property(t => t.BranchID).HasColumnName("BranchID");
            this.Property(t => t.DefaultBranch).HasColumnName("DefaultBranch");
            this.Property(t => t.RecordStatus).HasColumnName("RecordStatus");

            // Relationships
            this.HasRequired(t => t.Branch)
                .WithMany(t => t.UserBranches)
                .HasForeignKey(d => d.BranchID);
            this.HasRequired(t => t.User)
                .WithMany(t => t.UserBranches)
                .HasForeignKey(d => d.UserID);

        }
    }
}
