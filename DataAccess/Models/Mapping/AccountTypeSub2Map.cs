using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace DataAccess.Models.Mapping
{
    public class AccountTypeSub2Map : EntityTypeConfiguration<AccountTypeSub2>
    {
        public AccountTypeSub2Map()
        {
            // Primary Key
            this.HasKey(t => t.AccountTypeSub2ID);

            // Properties
            this.Property(t => t.Name)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.Notes)
                .HasMaxLength(50);

            this.Property(t => t.RecordStatus)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(1);

            // Table & Column Mappings
            this.ToTable("AccountTypeSub2");
            this.Property(t => t.AccountTypeSub2ID).HasColumnName("AccountTypeSub2ID");
            this.Property(t => t.AccountTypeSub1ID).HasColumnName("AccountTypeSub1ID");
            this.Property(t => t.Name).HasColumnName("Name");
            this.Property(t => t.Notes).HasColumnName("Notes");
            this.Property(t => t.RecordStatus).HasColumnName("RecordStatus");

            // Relationships
            this.HasRequired(t => t.AccountTypeSub1)
                .WithMany(t => t.AccountTypeSub2)
                .HasForeignKey(d => d.AccountTypeSub1ID);

        }
    }
}
