using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace DataAccess.Models.Mapping
{
    public class AgentMap : EntityTypeConfiguration<Agent>
    {
        public AgentMap()
        {
            // Primary Key
            this.HasKey(t => t.AgentID);

            // Properties
            this.Property(t => t.AgentName)
                .IsRequired()
                .HasMaxLength(100);

            this.Property(t => t.EnglishName)
                .HasMaxLength(100);

            this.Property(t => t.Phone)
                .HasMaxLength(20);

            this.Property(t => t.Fax)
                .HasMaxLength(20);

            this.Property(t => t.Adress)
                .HasMaxLength(150);

            this.Property(t => t.Notes)
                .HasMaxLength(200);

            this.Property(t => t.RecordStatus)
                .IsFixedLength()
                .HasMaxLength(1);

            // Table & Column Mappings
            this.ToTable("Agent");
            this.Property(t => t.AgentID).HasColumnName("AgentID");
            this.Property(t => t.AgentNo).HasColumnName("AgentNo");
            this.Property(t => t.AgentName).HasColumnName("AgentName");
            this.Property(t => t.AccountID).HasColumnName("AccountID");
            this.Property(t => t.EnglishName).HasColumnName("EnglishName");
            this.Property(t => t.Phone).HasColumnName("Phone");
            this.Property(t => t.Fax).HasColumnName("Fax");
            this.Property(t => t.Adress).HasColumnName("Adress");
            this.Property(t => t.Notes).HasColumnName("Notes");
            this.Property(t => t.RecordStatus).HasColumnName("RecordStatus");
        }
    }
}
