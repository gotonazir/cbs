using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace DataAccess.Models.Mapping
{
    public class CurrencyGroupMap : EntityTypeConfiguration<CurrencyGroup>
    {
        public CurrencyGroupMap()
        {
            // Primary Key
            this.HasKey(t => t.CurrencyGroupID);

            // Properties
            this.Property(t => t.RecordStatus)
                .IsFixedLength()
                .HasMaxLength(1);

            // Table & Column Mappings
            this.ToTable("CurrencyGroup");
            this.Property(t => t.CurrencyGroupID).HasColumnName("CurrencyGroupID");
            this.Property(t => t.CurrencyID).HasColumnName("CurrencyID");
            this.Property(t => t.GroupID).HasColumnName("GroupID");
            this.Property(t => t.RecordStatus).HasColumnName("RecordStatus");

            // Relationships
            this.HasRequired(t => t.Currency)
                .WithMany(t => t.CurrencyGroups)
                .HasForeignKey(d => d.CurrencyID);

        }
    }
}
