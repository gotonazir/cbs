using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace DataAccess.Models.Mapping
{
    public class HawalaTransactionMap : EntityTypeConfiguration<HawalaTransaction>
    {
        public HawalaTransactionMap()
        {
            // Primary Key
            this.HasKey(t => t.HawalaID);

            // Properties
            this.Property(t => t.TranType)
                .IsFixedLength()
                .HasMaxLength(1);

            this.Property(t => t.ReceiverArea)
                .HasMaxLength(50);

            this.Property(t => t.ReasonForHawala)
                .HasMaxLength(100);

            this.Property(t => t.Relationship)
                .HasMaxLength(50);

            this.Property(t => t.SenderNotes)
                .HasMaxLength(200);

            this.Property(t => t.SourceOfFunds)
                .HasMaxLength(100);

            this.Property(t => t.SenderChequeBank)
                .HasMaxLength(50);

            this.Property(t => t.TimeStamp)
                .IsFixedLength()
                .HasMaxLength(8)
                .IsRowVersion();

            this.Property(t => t.LockTransaction)
                .IsFixedLength()
                .HasMaxLength(1);

            this.Property(t => t.RecordStatus)
                .IsFixedLength()
                .HasMaxLength(1);

            // Table & Column Mappings
            this.ToTable("HawalaTransaction");
            this.Property(t => t.HawalaID).HasColumnName("HawalaID");
            this.Property(t => t.TranType).HasColumnName("TranType");
            this.Property(t => t.ReceiverCityID).HasColumnName("ReceiverCityID");
            this.Property(t => t.ReceiverArea).HasColumnName("ReceiverArea");
            this.Property(t => t.ReceiverClientID).HasColumnName("ReceiverClientID");
            this.Property(t => t.SendingAmount).HasColumnName("SendingAmount");
            this.Property(t => t.SendingCurrencyID).HasColumnName("SendingCurrencyID");
            this.Property(t => t.SenderClientID).HasColumnName("SenderClientID");
            this.Property(t => t.ReasonForHawala).HasColumnName("ReasonForHawala");
            this.Property(t => t.Relationship).HasColumnName("Relationship");
            this.Property(t => t.SenderNotes).HasColumnName("SenderNotes");
            this.Property(t => t.BranchCommission).HasColumnName("BranchCommission");
            this.Property(t => t.HeadOfficeCommission).HasColumnName("HeadOfficeCommission");
            this.Property(t => t.BCommissionCurrencyID).HasColumnName("BCommissionCurrencyID");
            this.Property(t => t.HOCommissionCurrencyID).HasColumnName("HOCommissionCurrencyID");
            this.Property(t => t.SourceOfFunds).HasColumnName("SourceOfFunds");
            this.Property(t => t.HawalaNo).HasColumnName("HawalaNo");
            this.Property(t => t.TransactionDt).HasColumnName("TransactionDt");
            this.Property(t => t.ModeOfPayment).HasColumnName("ModeOfPayment");
            this.Property(t => t.SandooqID).HasColumnName("SandooqID");
            this.Property(t => t.SenderAccountID).HasColumnName("SenderAccountID");
            this.Property(t => t.SenderChequeNo).HasColumnName("SenderChequeNo");
            this.Property(t => t.SenderChequeDt).HasColumnName("SenderChequeDt");
            this.Property(t => t.SenderChequeBank).HasColumnName("SenderChequeBank");
            this.Property(t => t.SenderChequeClearanceMode).HasColumnName("SenderChequeClearanceMode");
            this.Property(t => t.CollectedAmount).HasColumnName("CollectedAmount");
            this.Property(t => t.CollectedCurrencyID).HasColumnName("CollectedCurrencyID");
            this.Property(t => t.CollectedExRate).HasColumnName("CollectedExRate");
            this.Property(t => t.AgentCommission).HasColumnName("AgentCommission");
            this.Property(t => t.HawalaTransactionCount).HasColumnName("HawalaTransactionCount");
            this.Property(t => t.SenderAgentID).HasColumnName("SenderAgentID");
            this.Property(t => t.HOSentAmount).HasColumnName("HOSentAmount");
            this.Property(t => t.HOSentCurrencyID).HasColumnName("HOSentCurrencyID");
            this.Property(t => t.ClientReceiveAmount).HasColumnName("ClientReceiveAmount");
            this.Property(t => t.ClientReceiveCurrencyID).HasColumnName("ClientReceiveCurrencyID");
            this.Property(t => t.ReceiverAgentID).HasColumnName("ReceiverAgentID");
            this.Property(t => t.RecieverAgentAreaID).HasColumnName("RecieverAgentAreaID");
            this.Property(t => t.TimeStamp).HasColumnName("TimeStamp");
            this.Property(t => t.LockTransaction).HasColumnName("LockTransaction");
            this.Property(t => t.RecordStatus).HasColumnName("RecordStatus");

            // Relationships
            this.HasOptional(t => t.Agent)
                .WithMany(t => t.HawalaTransactions)
                .HasForeignKey(d => d.ReceiverAgentID);
            this.HasOptional(t => t.Agent1)
                .WithMany(t => t.HawalaTransactions1)
                .HasForeignKey(d => d.SenderAgentID);
            this.HasOptional(t => t.AgentArea)
                .WithMany(t => t.HawalaTransactions)
                .HasForeignKey(d => d.RecieverAgentAreaID);
            this.HasRequired(t => t.CityCountry)
                .WithMany(t => t.HawalaTransactions)
                .HasForeignKey(d => d.ReceiverCityID);
            this.HasOptional(t => t.Client)
                .WithMany(t => t.HawalaTransactions)
                .HasForeignKey(d => d.ReceiverClientID);
            this.HasOptional(t => t.Client1)
                .WithMany(t => t.HawalaTransactions1)
                .HasForeignKey(d => d.SenderClientID);
            this.HasOptional(t => t.Currency)
                .WithMany(t => t.HawalaTransactions)
                .HasForeignKey(d => d.SendingCurrencyID);
            this.HasOptional(t => t.Currency1)
                .WithMany(t => t.HawalaTransactions1)
                .HasForeignKey(d => d.BCommissionCurrencyID);
            this.HasOptional(t => t.Currency2)
                .WithMany(t => t.HawalaTransactions2)
                .HasForeignKey(d => d.HOCommissionCurrencyID);
            this.HasOptional(t => t.Currency3)
                .WithMany(t => t.HawalaTransactions3)
                .HasForeignKey(d => d.CollectedCurrencyID);
            this.HasOptional(t => t.Currency4)
                .WithMany(t => t.HawalaTransactions4)
                .HasForeignKey(d => d.HOSentCurrencyID);
            this.HasOptional(t => t.Currency5)
                .WithMany(t => t.HawalaTransactions5)
                .HasForeignKey(d => d.SendingCurrencyID);
            this.HasOptional(t => t.Currency6)
                .WithMany(t => t.HawalaTransactions6)
                .HasForeignKey(d => d.ClientReceiveCurrencyID);
            this.HasOptional(t => t.Sandooq)
                .WithMany(t => t.HawalaTransactions)
                .HasForeignKey(d => d.SandooqID);

        }
    }
}
