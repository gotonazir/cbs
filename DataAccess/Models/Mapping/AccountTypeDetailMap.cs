using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace DataAccess.Models.Mapping
{
    public class AccountTypeDetailMap : EntityTypeConfiguration<AccountTypeDetail>
    {
        public AccountTypeDetailMap()
        {
            // Primary Key
            this.HasKey(t => t.AccountTypeDetailID);

            // Properties
            this.Property(t => t.TimeStamp)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(8)
                .IsRowVersion();

            this.Property(t => t.RecordStatus)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(1);

            // Table & Column Mappings
            this.ToTable("AccountTypeDetail");
            this.Property(t => t.AccountTypeDetailID).HasColumnName("AccountTypeDetailID");
            this.Property(t => t.AccountID).HasColumnName("AccountID");
            this.Property(t => t.AccountTreeID).HasColumnName("AccountTreeID");
            this.Property(t => t.AccountTypeID).HasColumnName("AccountTypeID");
            this.Property(t => t.AccountTypeSub1ID).HasColumnName("AccountTypeSub1ID");
            this.Property(t => t.AccountTypeSub2ID).HasColumnName("AccountTypeSub2ID");
            this.Property(t => t.TimeStamp).HasColumnName("TimeStamp");
            this.Property(t => t.RecordStatus).HasColumnName("RecordStatus");

            // Relationships
            this.HasRequired(t => t.Account)
                .WithMany(t => t.AccountTypeDetails)
                .HasForeignKey(d => d.AccountID);
            this.HasRequired(t => t.AccountTree)
                .WithMany(t => t.AccountTypeDetails)
                .HasForeignKey(d => d.AccountTreeID);
            this.HasRequired(t => t.AccountType)
                .WithMany(t => t.AccountTypeDetails)
                .HasForeignKey(d => d.AccountTypeID);
            this.HasRequired(t => t.AccountTypeSub1)
                .WithMany(t => t.AccountTypeDetails)
                .HasForeignKey(d => d.AccountTypeSub1ID);
            this.HasRequired(t => t.AccountTypeSub2)
                .WithMany(t => t.AccountTypeDetails)
                .HasForeignKey(d => d.AccountTypeSub2ID);

        }
    }
}
