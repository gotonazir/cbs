using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace DataAccess.Models.Mapping
{
    public class GroupTypeMap : EntityTypeConfiguration<GroupType>
    {
        public GroupTypeMap()
        {
            // Primary Key
            this.HasKey(t => t.GroupTypeID);

            // Properties
            this.Property(t => t.GroupTypeName)
                .HasMaxLength(50);

            this.Property(t => t.RecordStatus)
                .IsFixedLength()
                .HasMaxLength(1);

            // Table & Column Mappings
            this.ToTable("GroupType");
            this.Property(t => t.GroupTypeID).HasColumnName("GroupTypeID");
            this.Property(t => t.GroupTypeName).HasColumnName("GroupTypeName");
            this.Property(t => t.RecordStatus).HasColumnName("RecordStatus");
        }
    }
}
