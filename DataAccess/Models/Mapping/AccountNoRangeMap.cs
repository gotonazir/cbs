using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace DataAccess.Models.Mapping
{
    public class AccountNoRangeMap : EntityTypeConfiguration<AccountNoRange>
    {
        public AccountNoRangeMap()
        {
            // Primary Key
            this.HasKey(t => t.AccountRangeID);

            // Properties
            // Table & Column Mappings
            this.ToTable("AccountNoRange");
            this.Property(t => t.AccountRangeID).HasColumnName("AccountRangeID");
            this.Property(t => t.AccountTreeID).HasColumnName("AccountTreeID");
            this.Property(t => t.AccountRange).HasColumnName("AccountRange");
            this.Property(t => t.CurrentAcccountNo).HasColumnName("CurrentAcccountNo");
            this.Property(t => t.AccountTypeID).HasColumnName("AccountTypeID");
            this.Property(t => t.AccountTypeSub1ID).HasColumnName("AccountTypeSub1ID");
            this.Property(t => t.AccountTypeSub2ID).HasColumnName("AccountTypeSub2ID");

            // Relationships
            this.HasRequired(t => t.AccountTree)
                .WithMany(t => t.AccountNoRanges)
                .HasForeignKey(d => d.AccountTreeID);
            this.HasOptional(t => t.AccountType)
                .WithMany(t => t.AccountNoRanges)
                .HasForeignKey(d => d.AccountTypeID);
            this.HasOptional(t => t.AccountTypeSub1)
                .WithMany(t => t.AccountNoRanges)
                .HasForeignKey(d => d.AccountTypeSub1ID);
            this.HasOptional(t => t.AccountTypeSub2)
                .WithMany(t => t.AccountNoRanges)
                .HasForeignKey(d => d.AccountTypeSub2ID);

        }
    }
}
