using System;
using System.Collections.Generic;

namespace DataAccess.Models
{
    public partial class BranchGroup
    {
        public long BranchGroupID { get; set; }
        public int BranchID { get; set; }
        public int GroupID { get; set; }
        public string RecordStatus { get; set; }
        public virtual Branch Branch { get; set; }
    }
}
