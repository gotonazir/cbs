using System;
using System.Collections.Generic;

namespace DataAccess.Models
{
    public partial class AccountTypeDetail
    {
        public long AccountTypeDetailID { get; set; }
        public long AccountID { get; set; }
        public int AccountTreeID { get; set; }
        public int AccountTypeID { get; set; }
        public int AccountTypeSub1ID { get; set; }
        public int AccountTypeSub2ID { get; set; }
        public byte[] TimeStamp { get; set; }
        public string RecordStatus { get; set; }
        public virtual Account Account { get; set; }
        public virtual AccountTree AccountTree { get; set; }
        public virtual AccountType AccountType { get; set; }
        public virtual AccountTypeSub1 AccountTypeSub1 { get; set; }
        public virtual AccountTypeSub2 AccountTypeSub2 { get; set; }
    }
}
