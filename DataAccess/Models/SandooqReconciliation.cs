using System;
using System.Collections.Generic;

namespace DataAccess.Models
{
    public partial class SandooqReconciliation
    {
        public long SandooqReconciliationID { get; set; }
        public long AccountCurrencyID { get; set; }
        public decimal DailyBalance { get; set; }
        public decimal PhysicalBalance { get; set; }
        public System.DateTime ReconciliationDt { get; set; }
        public Nullable<System.DateTime> DifferenceDt { get; set; }
        public Nullable<System.DateTime> SettlementDt { get; set; }
        public string Notes { get; set; }
        public string RecordStatus { get; set; }
        public virtual AccountCurrency AccountCurrency { get; set; }
    }
}
