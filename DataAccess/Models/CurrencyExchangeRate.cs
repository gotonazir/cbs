using System;
using System.Collections.Generic;

namespace DataAccess.Models
{
    public partial class CurrencyExchangeRate
    {
        public int CurrencyExchangeID { get; set; }
        public int CurrencyIDFrom { get; set; }
        public int CurrencyIDTo { get; set; }
        public Nullable<decimal> CentralBankRate { get; set; }
        public Nullable<decimal> TransferMin { get; set; }
        public Nullable<decimal> TransferMax { get; set; }
        public Nullable<decimal> TransferRate { get; set; }
        public Nullable<decimal> BuyMin { get; set; }
        public Nullable<decimal> BuyMax { get; set; }
        public Nullable<decimal> BuyRate { get; set; }
        public Nullable<decimal> SellMin { get; set; }
        public Nullable<decimal> SellMax { get; set; }
        public Nullable<decimal> SellRate { get; set; }
        public string Notes { get; set; }
        public virtual Currency Currency { get; set; }
        public virtual Currency Currency1 { get; set; }
    }
}
