using System;
using System.Collections.Generic;

namespace DataAccess.Models
{
    public partial class AccountCurrency
    {
        public AccountCurrency()
        {
            this.SandooqReconciliations = new List<SandooqReconciliation>();
        }

        public long AccountCurrencyID { get; set; }
        public long AccountID { get; set; }
        public int CurrencyID { get; set; }
        public decimal Balance { get; set; }
        public Nullable<decimal> MinBalance { get; set; }
        public Nullable<decimal> OverDraft { get; set; }
        public byte[] TimeStamp { get; set; }
        public string RecordStatus { get; set; }
        public virtual Account Account { get; set; }
        public virtual Currency Currency { get; set; }
        public virtual ICollection<SandooqReconciliation> SandooqReconciliations { get; set; }
    }
}
