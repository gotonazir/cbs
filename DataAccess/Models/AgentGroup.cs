using System;
using System.Collections.Generic;

namespace DataAccess.Models
{
    public partial class AgentGroup
    {
        public long AgentGroupID { get; set; }
        public int AgentID { get; set; }
        public int GroupID { get; set; }
        public string RecordStatus { get; set; }
        public virtual Agent Agent { get; set; }
    }
}
