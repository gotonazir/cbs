using System;
using System.Collections.Generic;

namespace DataAccess.Models
{
    public partial class Branch
    {
        public Branch()
        {
            this.Accounts = new List<Account>();
            this.BankTransactions = new List<BankTransaction>();
            this.BankTransactions1 = new List<BankTransaction>();
            this.BankTransactions2 = new List<BankTransaction>();
            this.BranchGroups = new List<BranchGroup>();
            this.UserBranches = new List<UserBranch>();
            this.UserSessions = new List<UserSession>();
        }

        public int BranchID { get; set; }
        public string BranchName { get; set; }
        public string Address { get; set; }
        public string Phone { get; set; }
        public string Fax { get; set; }
        public Nullable<short> BranchType { get; set; }
        public byte[] TimeStamp { get; set; }
        public string RecordStatus { get; set; }
        public virtual ICollection<Account> Accounts { get; set; }
        public virtual ICollection<BankTransaction> BankTransactions { get; set; }
        public virtual ICollection<BankTransaction> BankTransactions1 { get; set; }
        public virtual ICollection<BankTransaction> BankTransactions2 { get; set; }
        public virtual ICollection<BranchGroup> BranchGroups { get; set; }
        public virtual ICollection<UserBranch> UserBranches { get; set; }
        public virtual ICollection<UserSession> UserSessions { get; set; }
    }
}
