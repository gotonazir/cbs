using System;
using System.Collections.Generic;

namespace DataAccess.Models
{
    public partial class Currency
    {
        public Currency()
        {
            this.AccountCurrencies = new List<AccountCurrency>();
            this.BankTransactions = new List<BankTransaction>();
            this.BankTransactions1 = new List<BankTransaction>();
            this.CurrencyExchangeRates = new List<CurrencyExchangeRate>();
            this.CurrencyExchangeRates1 = new List<CurrencyExchangeRate>();
            this.CurrencyGroups = new List<CurrencyGroup>();
            this.HawalaTransactions = new List<HawalaTransaction>();
            this.HawalaTransactions1 = new List<HawalaTransaction>();
            this.HawalaTransactions2 = new List<HawalaTransaction>();
            this.HawalaTransactions3 = new List<HawalaTransaction>();
            this.HawalaTransactions4 = new List<HawalaTransaction>();
            this.HawalaTransactions5 = new List<HawalaTransaction>();
            this.HawalaTransactions6 = new List<HawalaTransaction>();
        }

        public int CurrencyID { get; set; }
        public string Name { get; set; }
        public string ShortNameAr { get; set; }
        public string ShortNameEn { get; set; }
        public string Notes { get; set; }
        public virtual ICollection<AccountCurrency> AccountCurrencies { get; set; }
        public virtual ICollection<BankTransaction> BankTransactions { get; set; }
        public virtual ICollection<BankTransaction> BankTransactions1 { get; set; }
        public virtual ICollection<CurrencyExchangeRate> CurrencyExchangeRates { get; set; }
        public virtual ICollection<CurrencyExchangeRate> CurrencyExchangeRates1 { get; set; }
        public virtual ICollection<CurrencyGroup> CurrencyGroups { get; set; }
        public virtual ICollection<HawalaTransaction> HawalaTransactions { get; set; }
        public virtual ICollection<HawalaTransaction> HawalaTransactions1 { get; set; }
        public virtual ICollection<HawalaTransaction> HawalaTransactions2 { get; set; }
        public virtual ICollection<HawalaTransaction> HawalaTransactions3 { get; set; }
        public virtual ICollection<HawalaTransaction> HawalaTransactions4 { get; set; }
        public virtual ICollection<HawalaTransaction> HawalaTransactions5 { get; set; }
        public virtual ICollection<HawalaTransaction> HawalaTransactions6 { get; set; }
    }
}
