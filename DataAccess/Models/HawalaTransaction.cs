using System;
using System.Collections.Generic;

namespace DataAccess.Models
{
    public partial class HawalaTransaction
    {
        public long HawalaID { get; set; }
        public string TranType { get; set; }
        public int ReceiverCityID { get; set; }
        public string ReceiverArea { get; set; }
        public Nullable<long> ReceiverClientID { get; set; }
        public Nullable<decimal> SendingAmount { get; set; }
        public Nullable<int> SendingCurrencyID { get; set; }
        public Nullable<long> SenderClientID { get; set; }
        public string ReasonForHawala { get; set; }
        public string Relationship { get; set; }
        public string SenderNotes { get; set; }
        public Nullable<decimal> BranchCommission { get; set; }
        public Nullable<decimal> HeadOfficeCommission { get; set; }
        public Nullable<int> BCommissionCurrencyID { get; set; }
        public Nullable<int> HOCommissionCurrencyID { get; set; }
        public string SourceOfFunds { get; set; }
        public Nullable<long> HawalaNo { get; set; }
        public Nullable<System.DateTime> TransactionDt { get; set; }
        public Nullable<int> ModeOfPayment { get; set; }
        public Nullable<int> SandooqID { get; set; }
        public Nullable<long> SenderAccountID { get; set; }
        public Nullable<int> SenderChequeNo { get; set; }
        public Nullable<System.DateTime> SenderChequeDt { get; set; }
        public string SenderChequeBank { get; set; }
        public Nullable<int> SenderChequeClearanceMode { get; set; }
        public Nullable<decimal> CollectedAmount { get; set; }
        public Nullable<int> CollectedCurrencyID { get; set; }
        public Nullable<decimal> CollectedExRate { get; set; }
        public Nullable<decimal> AgentCommission { get; set; }
        public Nullable<long> HawalaTransactionCount { get; set; }
        public Nullable<int> SenderAgentID { get; set; }
        public Nullable<decimal> HOSentAmount { get; set; }
        public Nullable<int> HOSentCurrencyID { get; set; }
        public Nullable<decimal> ClientReceiveAmount { get; set; }
        public Nullable<int> ClientReceiveCurrencyID { get; set; }
        public Nullable<int> ReceiverAgentID { get; set; }
        public Nullable<int> RecieverAgentAreaID { get; set; }
        public byte[] TimeStamp { get; set; }
        public string LockTransaction { get; set; }
        public string RecordStatus { get; set; }
        public virtual Agent Agent { get; set; }
        public virtual Agent Agent1 { get; set; }
        public virtual AgentArea AgentArea { get; set; }
        public virtual CityCountry CityCountry { get; set; }
        public virtual Client Client { get; set; }
        public virtual Client Client1 { get; set; }
        public virtual Currency Currency { get; set; }
        public virtual Currency Currency1 { get; set; }
        public virtual Currency Currency2 { get; set; }
        public virtual Currency Currency3 { get; set; }
        public virtual Currency Currency4 { get; set; }
        public virtual Currency Currency5 { get; set; }
        public virtual Currency Currency6 { get; set; }
        public virtual Sandooq Sandooq { get; set; }
    }
}
