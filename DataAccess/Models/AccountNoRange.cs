using System;
using System.Collections.Generic;

namespace DataAccess.Models
{
    public partial class AccountNoRange
    {
        public int AccountRangeID { get; set; }
        public int AccountTreeID { get; set; }
        public int AccountRange { get; set; }
        public long CurrentAcccountNo { get; set; }
        public Nullable<int> AccountTypeID { get; set; }
        public Nullable<int> AccountTypeSub1ID { get; set; }
        public Nullable<int> AccountTypeSub2ID { get; set; }
        public virtual AccountTree AccountTree { get; set; }
        public virtual AccountType AccountType { get; set; }
        public virtual AccountTypeSub1 AccountTypeSub1 { get; set; }
        public virtual AccountTypeSub2 AccountTypeSub2 { get; set; }
    }
}
