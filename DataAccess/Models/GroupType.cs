using System;
using System.Collections.Generic;

namespace DataAccess.Models
{
    public partial class GroupType
    {
        public GroupType()
        {
            this.Groups = new List<Group>();
        }

        public int GroupTypeID { get; set; }
        public string GroupTypeName { get; set; }
        public string RecordStatus { get; set; }
        public virtual ICollection<Group> Groups { get; set; }
    }
}
