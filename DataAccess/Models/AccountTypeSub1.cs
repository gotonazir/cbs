using System;
using System.Collections.Generic;

namespace DataAccess.Models
{
    public partial class AccountTypeSub1
    {
        public AccountTypeSub1()
        {
            this.AccountNoRanges = new List<AccountNoRange>();
            this.AccountTypeDetails = new List<AccountTypeDetail>();
            this.AccountTypeSub2 = new List<AccountTypeSub2>();
        }

        public int AccountTypeSub1ID { get; set; }
        public Nullable<int> AccountTypeID { get; set; }
        public string Name { get; set; }
        public string Notes { get; set; }
        public string RecordStatus { get; set; }
        public virtual ICollection<AccountNoRange> AccountNoRanges { get; set; }
        public virtual AccountType AccountType { get; set; }
        public virtual ICollection<AccountTypeDetail> AccountTypeDetails { get; set; }
        public virtual ICollection<AccountTypeSub2> AccountTypeSub2 { get; set; }
    }
}
