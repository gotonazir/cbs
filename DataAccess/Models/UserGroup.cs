using System;
using System.Collections.Generic;

namespace DataAccess.Models
{
    public partial class UserGroup
    {
        public long UserGroupID { get; set; }
        public int GroupID { get; set; }
        public int UserID { get; set; }
        public string RecordStatus { get; set; }
        public virtual User User { get; set; }
    }
}
