using System;
using System.Collections.Generic;

namespace DataAccess.Models
{
    public partial class CityCountry
    {
        public CityCountry()
        {
            this.AgentAreas = new List<AgentArea>();
            this.Clients = new List<Client>();
            this.HawalaTransactions = new List<HawalaTransaction>();
        }

        public int CityID { get; set; }
        public string CityName { get; set; }
        public string Country { get; set; }
        public virtual ICollection<AgentArea> AgentAreas { get; set; }
        public virtual ICollection<Client> Clients { get; set; }
        public virtual ICollection<HawalaTransaction> HawalaTransactions { get; set; }
    }
}
