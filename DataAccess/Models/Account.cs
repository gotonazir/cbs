using System;
using System.Collections.Generic;

namespace DataAccess.Models
{
    public partial class Account
    {
        public Account()
        {
            this.AccountCurrencies = new List<AccountCurrency>();
            this.AccountGroups = new List<AccountGroup>();
            this.AccountTypeDetails = new List<AccountTypeDetail>();
            this.BankTransactions = new List<BankTransaction>();
            this.BankTransactions1 = new List<BankTransaction>();
            this.Sandooqs = new List<Sandooq>();
        }

        public long AccountID { get; set; }
        public int AccountTypeTreeId { get; set; }
        public long AccountNo { get; set; }
        public int BranchID { get; set; }
        public long ClientID { get; set; }
        public System.DateTime OpenDt { get; set; }
        public Nullable<long> VIPNo { get; set; }
        public Nullable<long> BeneficiaryID { get; set; }
        public Nullable<long> AuthorizedID { get; set; }
        public string Notes { get; set; }
        public Nullable<System.DateTime> CloseDt { get; set; }
        public byte[] timestamp { get; set; }
        public string RecordStatus { get; set; }
        public virtual AccountTypeTree AccountTypeTree { get; set; }
        public virtual Branch Branch { get; set; }
        public virtual Client Client { get; set; }
        public virtual Client Client1 { get; set; }
        public virtual Client Client2 { get; set; }
        public virtual IList<AccountCurrency> AccountCurrencies { get; set; }
        public virtual ICollection<AccountGroup> AccountGroups { get; set; }
        public virtual ICollection<AccountTypeDetail> AccountTypeDetails { get; set; }
        public virtual ICollection<BankTransaction> BankTransactions { get; set; }
        public virtual ICollection<BankTransaction> BankTransactions1 { get; set; }
        public virtual ICollection<Sandooq> Sandooqs { get; set; }
    }
}
