using System;
using System.Collections.Generic;

namespace DataAccess.Models
{
    public partial class BankTransaction
    {
        public long BTID { get; set; }
        public int TransactionMode { get; set; }
        public long ReceiptNo { get; set; }
        public decimal AmountReceived { get; set; }
        public int CurrencyIDReceived { get; set; }
        public Nullable<int> HomeBranchID { get; set; }
        public long AccountID { get; set; }
        public string PersonName { get; set; }
        public Nullable<int> SandooqID { get; set; }
        public Nullable<System.DateTime> TransactionDate { get; set; }
        public Nullable<int> TrasactionBranchID { get; set; }
        public Nullable<decimal> CommissionAmount { get; set; }
        public Nullable<int> CommissionCurrencyID { get; set; }
        public Nullable<int> ToBranchID { get; set; }
        public Nullable<long> ToAccountID { get; set; }
        public Nullable<decimal> ToAmount { get; set; }
        public Nullable<int> ToCurrencyID { get; set; }
        public Nullable<bool> AmountCounted { get; set; }
        public Nullable<short> PrintReceipt { get; set; }
        public string Notes { get; set; }
        public string LockTransaction { get; set; }
        public string RecordStatus { get; set; }
        public virtual Account Account { get; set; }
        public virtual Account Account1 { get; set; }
        public virtual Branch Branch { get; set; }
        public virtual Branch Branch1 { get; set; }
        public virtual Branch Branch2 { get; set; }
        public virtual Currency Currency { get; set; }
        public virtual Currency Currency1 { get; set; }
        public virtual Sandooq Sandooq { get; set; }
    }
}
