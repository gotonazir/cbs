using System;
using System.Collections.Generic;

namespace DataAccess.Models
{
    public partial class AccountGroup
    {
        public long AccountGroupID { get; set; }
        public long AccountID { get; set; }
        public int GroupID { get; set; }
        public string RecordStatus { get; set; }
        public virtual Account Account { get; set; }
        public virtual Group Group { get; set; }
    }
}
