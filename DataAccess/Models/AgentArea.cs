using System;
using System.Collections.Generic;

namespace DataAccess.Models
{
    public partial class AgentArea
    {
        public AgentArea()
        {
            this.HawalaTransactions = new List<HawalaTransaction>();
        }

        public int AgentAreaID { get; set; }
        public int AreaNo { get; set; }
        public string AreaName { get; set; }
        public int AgentID { get; set; }
        public Nullable<int> CityID { get; set; }
        public Nullable<int> TranactionTypeID { get; set; }
        public string LocalOrintl { get; set; }
        public string Phone { get; set; }
        public string fax { get; set; }
        public string Adress { get; set; }
        public string RequiredApproval { get; set; }
        public string DirectExchange { get; set; }
        public string FreezArea { get; set; }
        public string Notes { get; set; }
        public virtual Agent Agent { get; set; }
        public virtual CityCountry CityCountry { get; set; }
        public virtual ICollection<HawalaTransaction> HawalaTransactions { get; set; }
    }
}
