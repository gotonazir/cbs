using System;
using System.Collections.Generic;

namespace DataAccess.Models
{
    public partial class CurrencyGroup
    {
        public long CurrencyGroupID { get; set; }
        public int CurrencyID { get; set; }
        public int GroupID { get; set; }
        public string RecordStatus { get; set; }
        public virtual Currency Currency { get; set; }
    }
}
