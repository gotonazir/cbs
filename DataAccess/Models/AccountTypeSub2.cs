using System;
using System.Collections.Generic;

namespace DataAccess.Models
{
    public partial class AccountTypeSub2
    {
        public AccountTypeSub2()
        {
            this.AccountNoRanges = new List<AccountNoRange>();
            this.AccountTypeDetails = new List<AccountTypeDetail>();
        }

        public int AccountTypeSub2ID { get; set; }
        public int AccountTypeSub1ID { get; set; }
        public string Name { get; set; }
        public string Notes { get; set; }
        public string RecordStatus { get; set; }
        public virtual ICollection<AccountNoRange> AccountNoRanges { get; set; }
        public virtual ICollection<AccountTypeDetail> AccountTypeDetails { get; set; }
        public virtual AccountTypeSub1 AccountTypeSub1 { get; set; }
    }
}
