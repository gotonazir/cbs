using System;
using System.Collections.Generic;

namespace DataAccess.Models
{
    public partial class Client
    {
        public Client()
        {
            this.Accounts = new List<Account>();
            this.Accounts1 = new List<Account>();
            this.Accounts2 = new List<Account>();
            this.ClientDocuments = new List<ClientDocument>();
            this.HawalaTransactions = new List<HawalaTransaction>();
            this.HawalaTransactions1 = new List<HawalaTransaction>();
        }

        public long ClientID { get; set; }
        public string FullName { get; set; }
        public string Address { get; set; }
        public int CityID { get; set; }
        public string Phone { get; set; }
        public string Mobile { get; set; }
        public string Fax { get; set; }
        public string PersonOrCorp { get; set; }
        public Nullable<long> GuarantorID { get; set; }
        public Nullable<int> PrivilegeID { get; set; }
        public string Notes { get; set; }
        public byte[] TimeStamp { get; set; }
        public string RecordStatus { get; set; }
        public byte[] SignImage { get; set; }
        public virtual ICollection<Account> Accounts { get; set; }
        public virtual ICollection<Account> Accounts1 { get; set; }
        public virtual ICollection<Account> Accounts2 { get; set; }
        public virtual CityCountry CityCountry { get; set; }
        public virtual IList<ClientDocument> ClientDocuments { get; set; }
        public virtual ICollection<HawalaTransaction> HawalaTransactions { get; set; }
        public virtual ICollection<HawalaTransaction> HawalaTransactions1 { get; set; }
    }
}
