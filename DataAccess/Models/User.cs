using System;
using System.Collections.Generic;

namespace DataAccess.Models
{
    public partial class User
    {
        public User()
        {
            this.UserBranches = new List<UserBranch>();
            this.UserGroups = new List<UserGroup>();
            this.UserSessions = new List<UserSession>();
        }

        public int UserID { get; set; }
        public Nullable<int> UserNo { get; set; }
        public string UserName { get; set; }
        public string PassWord { get; set; }
        public string UserRole { get; set; }
        public int DefaultBranchID { get; set; }
        public string BlockedStatus { get; set; }
        public short ChangePasswordDays { get; set; }
        public System.DateTime ChangePasswordDt { get; set; }
        public string Title { get; set; }
        public Nullable<decimal> Salary { get; set; }
        public string Notes { get; set; }
        public string RecordStatus { get; set; }
        public virtual ICollection<UserBranch> UserBranches { get; set; }
        public virtual ICollection<UserGroup> UserGroups { get; set; }
        public virtual ICollection<UserSession> UserSessions { get; set; }
    }
}
