using System;
using System.Collections.Generic;

namespace DataAccess.Models
{
    public partial class AccountTypeTree
    {
        public AccountTypeTree()
        {
            this.Accounts = new List<Account>();
        }

        public int AccountTypeTreeId { get; set; }
        public int ParentId { get; set; }
        public string Description { get; set; }
        public Nullable<int> Value { get; set; }
        public Nullable<long> AccountRange { get; set; }
        public Nullable<long> CurrentAccountNo { get; set; }
        public string RecordStatus { get; set; }
        public virtual ICollection<Account> Accounts { get; set; }
    }
}
