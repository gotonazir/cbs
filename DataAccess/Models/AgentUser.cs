using System;
using System.Collections.Generic;

namespace DataAccess.Models
{
    public partial class AgentUser
    {
        public int AgentUserID { get; set; }
        public int AgentID { get; set; }
        public string UserName { get; set; }
        public string Notes { get; set; }
        public string RecordStatus { get; set; }
        public virtual Agent Agent { get; set; }
    }
}
