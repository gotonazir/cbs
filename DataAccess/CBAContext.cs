using DataAccess.Models.Mapping;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;

namespace DataAccess.Models
{
    public partial class CBAContext : DbContext
    {
        static CBAContext()
        {
            Database.SetInitializer<CBAContext>(null);
        }

        public CBAContext()
            : base("Name=CBAContext")
        {
        }

        public DbSet<Account> Accounts { get; set; }
        public DbSet<AccountCurrency> AccountCurrencies { get; set; }
        public DbSet<AccountGroup> AccountGroups { get; set; }
        public DbSet<AccountNoRange> AccountNoRanges { get; set; }
        public DbSet<AccountTree> AccountTrees { get; set; }
        public DbSet<AccountType> AccountTypes { get; set; }
        public DbSet<AccountTypeDetail> AccountTypeDetails { get; set; }
        public DbSet<AccountTypeSub1> AccountTypeSub1 { get; set; }
        public DbSet<AccountTypeSub2> AccountTypeSub2 { get; set; }
        public DbSet<AccountTypeTree> AccountTypeTrees { get; set; }
        public DbSet<Agent> Agents { get; set; }
        public DbSet<AgentArea> AgentAreas { get; set; }
        public DbSet<AgentGroup> AgentGroups { get; set; }
        public DbSet<AgentUser> AgentUsers { get; set; }
        public DbSet<BankTransaction> BankTransactions { get; set; }
        public DbSet<Branch> Branches { get; set; }
        public DbSet<BranchGroup> BranchGroups { get; set; }
        public DbSet<CityCountry> CityCountries { get; set; }
        public DbSet<Client> Clients { get; set; }
        public DbSet<ClientDocument> ClientDocuments { get; set; }
        public DbSet<Currency> Currencies { get; set; }
        public DbSet<CurrencyExchangeRate> CurrencyExchangeRates { get; set; }
        public DbSet<CurrencyGroup> CurrencyGroups { get; set; }
        public DbSet<Group> Groups { get; set; }
        public DbSet<GroupType> GroupTypes { get; set; }
        public DbSet<HawalaTransaction> HawalaTransactions { get; set; }
        public DbSet<Sandooq> Sandooqs { get; set; }
        public DbSet<SandooqReconciliation> SandooqReconciliations { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<UserBranch> UserBranches { get; set; }
        public DbSet<UserGroup> UserGroups { get; set; }
        public DbSet<UserSession> UserSessions { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add(new AccountMap());
            modelBuilder.Configurations.Add(new AccountCurrencyMap());
            modelBuilder.Configurations.Add(new AccountGroupMap());
            modelBuilder.Configurations.Add(new AccountNoRangeMap());
            modelBuilder.Configurations.Add(new AccountTreeMap());
            modelBuilder.Configurations.Add(new AccountTypeMap());
            modelBuilder.Configurations.Add(new AccountTypeDetailMap());
            modelBuilder.Configurations.Add(new AccountTypeSub1Map());
            modelBuilder.Configurations.Add(new AccountTypeSub2Map());
            modelBuilder.Configurations.Add(new AccountTypeTreeMap());
            modelBuilder.Configurations.Add(new AgentMap());
            modelBuilder.Configurations.Add(new AgentAreaMap());
            modelBuilder.Configurations.Add(new AgentGroupMap());
            modelBuilder.Configurations.Add(new AgentUserMap());
            modelBuilder.Configurations.Add(new BankTransactionMap());
            modelBuilder.Configurations.Add(new BranchMap());
            modelBuilder.Configurations.Add(new BranchGroupMap());
            modelBuilder.Configurations.Add(new CityCountryMap());
            modelBuilder.Configurations.Add(new ClientMap());
            modelBuilder.Configurations.Add(new ClientDocumentMap());
            modelBuilder.Configurations.Add(new CurrencyMap());
            modelBuilder.Configurations.Add(new CurrencyExchangeRateMap());
            modelBuilder.Configurations.Add(new CurrencyGroupMap());
            modelBuilder.Configurations.Add(new GroupMap());
            modelBuilder.Configurations.Add(new GroupTypeMap());
            modelBuilder.Configurations.Add(new HawalaTransactionMap());
            modelBuilder.Configurations.Add(new SandooqMap());
            modelBuilder.Configurations.Add(new SandooqReconciliationMap());
            modelBuilder.Configurations.Add(new UserMap());
            modelBuilder.Configurations.Add(new UserBranchMap());
            modelBuilder.Configurations.Add(new UserGroupMap());
            modelBuilder.Configurations.Add(new UserSessionMap());
        }
    }
}
