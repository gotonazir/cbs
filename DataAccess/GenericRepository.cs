﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using DataAccess.Models;

namespace DataAccess
{
    // Separation of concerns method TODo
    public class GenericRepository<TEntity> : IGenericRepository<TEntity> where TEntity : class
    {
        protected readonly CBAContext _cbaContext = null;
        protected readonly DbContext Context;
        protected readonly DbSet<TEntity> Entities;

        public GenericRepository(CBAContext context)
        {
            Context = context;
            _cbaContext = context;
            Entities = Context.Set<TEntity>();
        }

        protected DbContext CbaContext
        {
            get { return Context ?? new CBAContext(); }
        }

        public void Add(TEntity entity)
        {
            Entities.Add(entity);
        }

        public void Update(TEntity entity)
        {
            Entities.Attach(entity);
            Context.Entry(entity).State = EntityState.Modified;
        }

        public void Remove(TEntity entity)
        {
            Entities.Remove(entity);
        }

        public TEntity Get(long id)
        {
            return Entities.Find(id);
        }

        public virtual IQueryable<TEntity> GetAll()
        {
            return Entities.AsQueryable();
        }


        public IQueryable<TEntity> Find(Expression<Func<TEntity, bool>> predicate)
        {
            return Entities.Where(predicate).AsQueryable();
        }

        public int SaveChanges()
        {
            return Context.SaveChanges();
        }

        //TEntity Single(Func<TEntity, bool> predicate);
        //TEntity First(Func<TEntity, bool> predicate);

        public IQueryable<TEntity> GetQueryWithIncludeById(TEntity entity, string toInclude)
        {
            Entities.Include(toInclude);
            return Entities;
        }

        public IQueryable<TEntity> GetQueryWithInclude(TEntity entity, string toInclude)
        {
             Entities.Include(toInclude);
            return Entities;
        }

        public IQueryable<TEntity> GetQueryWithMultipleInclude(string[] includes)
        {
            foreach (string include in includes)
            {
                Entities.Include(include);
            }
            return Entities;
        }
    }
}
