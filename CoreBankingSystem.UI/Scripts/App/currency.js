﻿
$(document).ready(function () {
    onCurrencyFormLoad();
});

function onCurrencyFormLoad() {

    $('#btnCurrencyEdit').hide();
    $('#btnCurrencySave').show();

    if (location.search != "") {
        var curencyId = new URLSearchParams(location.search).get('currencyId');
        if (currencyId != null) {
            console.log('CurrencyId' + currencyId);
            var get_currencies = jqxhr('get', 'api/currencyapi/Get?id=' + currencyId + '&fromScreen=reg');

            get_currencies.done(function (data) {
                $('#Name').val(data.Name);
                $('#ShortNameAr').val(data.ShortNameAr);
                $('#ShortNameEn').val(data.ShortNameEn);
                $('#Notes').val(data.Notes);
                $('#btnCurrencySave').hide();
                $('#btnCurrencyEdit').show();
            });

            get_currencies.fail(function (r) {
                console.log(r);
            });
        }
    }
}

$('#btnCurrencySave').click(function () {
    if ($('#CurrencyForm').valid()) {
        var currencyModel = {
            Name: $('#Name').val(),
            ShortNameAr: $('#ShortNameAr').val(),
            ShortNameEn: $('#ShortNameEn').val(),
            Notes: $('#Notes').val()
        };

        var addCurrency = jqxhr('post', 'api/currencyapi/post', currencyModel);

        addCurrency.done(function (r) {

            $('#dlg').html(CoreBankingSuccessDialog());
            $("#myModal").modal();
            document.getElementById("CurrencyForm").reset();

        });

        addCurrency.fail(function (r) {
            $('#dlg').html(CoreBankingErrorDialog(r.responseJSON.ExceptionMessage));
            $("#myModal").modal();
        });
    }

});

$('#btnCurrencyEdit').click(function () {
    debugger;
    if ($('#CurrencyForm').valid()) {
        var currencyId = new URLSearchParams(location.search).get('currencyId');
        var currencyModel = {
            CurrencyId: currencyId,
            Name: $('#Name').val(),
            ShortNameAr: $('#ShortNameAr').val(),
            ShortNameEn: $('#ShortNameEn').val(),
            Notes: $('#Notes').val()
        };

        var edit_currency = jqxhr('put', 'api/currencyapi/put', currencyModel);

        edit_currency.done(function (r) {
            $('#dlg').html(CoreBankingSuccessDialog());
            $("#myModal").modal();  
        });

        edit_currency.fail(function (r) {
            $('#dlg').html(CoreBankingErrorDialog(r.responseJSON.ExceptionMessage));
            $("#myModal").modal();
        });
    }
});

$('#btnCurrencyReset').click(function () {
    document.getElementById("CurrencyForm").reset();
});
$("#btnCurrencyClose").click(function () {
    var url = "/Dashboard/Index";
    window.location.href = url;
});