﻿
$(document).ready(function () { 
    onAgentAreaFormLoad();
});

function onAgentAreaFormLoad() {
    
    $('#btnAgentAreaEdit').hide();
    $('#btnAgentAreaSave').show();

    var get_AgentNames = jqxhr('get', 'api/agentapi/AgentNames');
    var get_cities = jqxhr('get', 'api/CityCountryApi/CityNames');

    get_AgentNames.done(function (response) {
        $("#AgentID").select2({
            placeholder: "Select Agent",
            data: response
        });
        //$("#AgentID").select2("val", "-1");
    });

    get_cities.done(function (response) {
        $("#CityID").select2({
            placeholder: "Select City",
            data: response
        });
        //$("#CityID").select2("val", "-1");

    });
    
    if (location.search !== "") {
        var agentAreaId = new URLSearchParams(location.search).get('agentAreaId');
        if (agentAreaId !== null) {
            console.log('AgentAreaId' + agentAreaId);
            var get_agentAreas = jqxhr('get', 'api/agentAreaapi/Get?id=' + agentAreaId + '&fromScreen=reg');

            get_agentAreas.done(function (data) {
                console.log(data); // response object
                $('#AgentAreaID').val(data.AgentAreaID);
                $('#AreaNo').val(data.AreaNo);
                $('#AreaName').val(data.AreaName); 
                $('#AgentID').val(data.AgentID); 
                $('#CityID').val(data.CityID); 
                $('#TranactionTypeID').val(data.TranactionTypeID);
                $('#LocalOrintl').val(data.LocalOrintl); 
                $('#Phone').val(data.Phone); 
                $('#Fax').val(data.Fax); 
                $('#Adress').val(data.Adress); 
                $('#RequiredApproval').val(data.RequiredApproval); 
                $('#DirectExchange').val(data.DirectExchange); 
                $('#FreezArea').val(data.FreezArea); 
                $('#Notes').val(data.Notes); 
                $('#btnAgentAreaSave').hide();
                $('#btnAgentAreaEdit').show();
            });

            get_users.fail(function (r) {
                console.log(r); 
            });
        }
    }
}

$('#btnAgentAreaSave').click(function () {
    if ($('#agentAreaForm').valid()) {
        var agentAreaModel = {
            //AgentAreaID: $('#AgentAreaID').val(),
            AreaNo: $('#AreaNo').val(),
            AreaName: $('#AreaName').val(),
            AgentID: $('#AgentID').val(),
            CityID: $('#CityID').val(),
            TranactionTypeID: $('#TranactionTypeID').val(),
            LocalOrintl: $('#LocalOrintl').val(),
            Phone: $('#Phone').val(),
            Fax: $('#Fax').val(),
            Adress: $('#Adress').val(),
            RequiredApproval: $('#RequiredApproval').val(),
            DirectExchange: $('#DirectExchange').val(),
            FreezArea: $('#FreezArea').val(),
            Notes: $('#Notes').val()
        };

        var addAgentArea = jqxhr('post', 'api/agentAreaapi/post', agentAreaModel);

        addAgentArea.done(function (r) {
            
            $('#dlg').html(CoreBankingSuccessDialog());          
            $("#myModal").modal();           
            document.getElementById("agentAreaForm").reset();

        });

        addAgentArea.fail(function (r) {
            $('#dlg').html(CoreBankingErrorDialog(r.responseJSON.ExceptionMessage));
            $("#myModal").modal();
        });
    }

});

$('#btnAgentAreaEdit').click(function () {
    if ($('#agentAreaForm').valid()) {
        var agentAreaId = new URLSearchParams(location.search).get('agentAreaId');
        var agentAreaModel = {
            AgentAreaId: agentAreaId,
            AreaNo: $('#AreaNo').val(),
            AreaName: $('#AreaName').val(),
            AgentID: $('#AgentID').val(),
            CityID: $('#CityID').val(),
            TranactionTypeID: $('#TranactionTypeID').val(),
            Phone: $('#Phone').val(),
            Fax: $('#Fax').val(),
            Adress: $('#Adress').val(),
            RequiredApproval: $('#RequiredApproval').val(),
            DirectExchange: $('#DirectExchange').val(),
            FreezArea: $('#FreezArea').val(),
            Notes: $('#Notes').val()                      
        };

        var edit_agentArea = jqxhr('put', 'api/agentAreaapi/put', agentAreaModel);

        edit_agentArea.done(function (r) {
            $('#dlg').html(CoreBankingSuccessDialog());
            $("#myModal").modal();  
        });

        edit_agentArea.fail(function (r) {
            $('#dlg').html(CoreBankingErrorDialog(r.responseJSON.ExceptionMessage));
            $("#myModal").modal();
        });
    }
});

$('#btnAgentAreaReset').click(function () {
    document.getElementById("agentAreaForm").reset();
});
$("#btnAgentAreaClose").click(function () {
    var url = "/Dashboard/Index";
    window.location.href = url;
});