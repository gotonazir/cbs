﻿+
$(document).ready(function () { 
    onAgentGroupFormLoad();
});

function onAgentGroupFormLoad() {
    
    $('#btnAgentGroupEdit').hide();
    $('#btnAgentGroupSave').show();

    var get_AgentNames = jqxhr('get', 'api/agentapi/AgentNames');

    get_AgentNames.done(function (response) {
        $("#AgentID").select2({
            placeholder: "Select Agent",
            data: response
        });
        //$("#AgentID").select2("val", "-1");
    });

    var get_GroupNames = jqxhr('get', 'api/groupapi/GroupNames');

    get_GroupNames.done(function (response) {
        $("#GroupID").select2({
            placeholder: "Select Group",
            data: response
        });
        //$("#GroupID").select2("val", "-1");
    });
    
    if (location.search !== "") {
        var agentGroupId = new URLSearchParams(location.search).get('agentGroupId');
        if (agentGroupId !== null) {
            console.log('AgentGroupId' + agentGroupId);
            var get_agentGroupes = jqxhr('get', 'api/agentgroupapi/Get?id=' + agentGroupId + '&fromScreen=reg');

            get_agentGroupes.done(function (data) {
                console.log(data); // response object
                $('#AgentGroupID').val(data.AgentGroupID);
                $('#AgentID').val(data.AgentID);
                $('#GroupID').val(data.GroupID);                              
                $('#btnAgentGroupSave').hide();
                $('#btnAgentGroupEdit').show();
            });

            get_users.fail(function (r) {
                console.log(r); 
            });
        }
    }
}

$('#btnAgentGroupSave').click(function () {
    if ($('#agentGroupForm').valid()) {
        var agentGroupModel = {
            //AgentGroupID: $('#AgentGroupID').val(),
            AgentID: $('#AgentID').val(),
            GroupID: $('#GroupID').val()
        };

        var addAgentGroup = jqxhr('post', 'api/agentgroupapi/post', agentGroupModel);

        addAgentGroup.done(function (r) {
            
            $('#dlg').html(CoreBankingSuccessDialog());          
            $("#myModal").modal();           
            document.getElementById("agentGroupForm").reset();

        });

        addAgentGroup.fail(function (r) {
            $('#dlg').html(CoreBankingErrorDialog(r.responseJSON.ExceptionMessage));
            $("#myModal").modal();
        });
    }

});

$('#btnAgentGroupEdit').click(function () {
    if ($('#agentGroupForm').valid()) {
        var agentGroupId = new URLSearchParams(location.search).get('agentGroupId');
        var agentGroupModel = {
            AgentGroupId: agentGroupId,
            AgentID: $('#AgentID').val(),
            GroupID: $('#GroupID').val()                        
        };

        var edit_agentGroup = jqxhr('put', 'api/agentgroupapi/put', agentGroupModel);

        edit_agentGroup.done(function (r) {
            $('#dlg').html(CoreBankingSuccessDialog());
            $("#myModal").modal();  
        });

        edit_agentGroup.fail(function (r) {
            $('#dlg').html(CoreBankingErrorDialog(r.responseJSON.ExceptionMessage));
            $("#myModal").modal();
        });
    }
});

$('#btnAgentGroupReset').click(function () {
    document.getElementById("agentGroupForm").reset();
});
$("#btnAgentGroupClose").click(function () {
    var url = "/Dashboard/Index";
    window.location.href = url;
});