﻿
$(document).ready(function () {
    onGroupFormLoad();
});

function onGroupFormLoad() {

    $('#btnGroupEdit').hide();
    $('#btnGroupSave').show();

    var get_GroupNames = jqxhr('get', 'api/groupTypeapi/GroupTypeNames');

    get_GroupNames.done(function (response) {
        $("#GroupTypeID").select2({ 
            placeholder: "Select Group",
            data: response  
        });
        //$("#GroupTypeID").select2("val", "-1"); 
    });

    if (location.search != "") {
        var groupId = new URLSearchParams(location.search).get('groupId');
        if (groupId != null) {
            console.log('GroupId' + groupId);
            var get_groups = jqxhr('get', 'api/groupapi/Get?id=' + groupId + '&fromScreen=reg');

            get_groups.done(function (data) {
                console.log(data); // response object
                $('#GroupName').val(data.GroupName);    
                $('#GroupTypeID').val(data.GroupTypeID);    
                $('#Notes').val(data.Notes);                
                $('#btnGroupSave').hide();
                $('#btnGroupEdit').show();
            });

            get_group.fail(function (r) {
                console.log(r);
            });
        }
    }
}

$('#btnGroupSave').click(function () {
    if ($('#groupForm').valid()) {
        var groupModel = {
            GroupName: $('#GroupName').val(),
            GroupTypeID: $('#GroupTypeID').val(),
            Notes: $('#Notes').val()
        };

        var addGroup = jqxhr('post', 'api/groupapi/post', groupModel);

        addGroup.done(function (r) {

            $('#dlg').html(CoreBankingSuccessDialog());
            $("#myModal").modal();
            document.getElementById("groupForm").reset();

        });

        addGroup.fail(function (r) {
            $('#dlg').html(CoreBankingErrorDialog(r.responseJSON.ExceptionMessage));
            $("#myModal").modal();
        });
    }

});

$('#btnGroupEdit').click(function () {
    if ($('#groupForm').valid()) {
        var groupId = new URLSearchParams(location.search).get('groupId');
        var groupModel = {
            GroupId: groupId,
            GroupName: $('#GroupName').val(),
            GroupTypeID: $('#GroupTypeID').val(),
            Notes: $('#Notes').val()
        };

        var edit_group = jqxhr('put', 'api/groupapi/put', groupModel);

        edit_group.done(function (r) {
            $('#dlg').html(CoreBankingSuccessDialog());
            $("#myModal").modal();
        });

        edit_group.fail(function (r) {
            $('#dlg').html(CoreBankingErrorDialog(r.responseJSON.ExceptionMessage));
            $("#myModal").modal();
        });
    }
});

$('#btnGroupReset').click(function () {
    document.getElementById("groupForm").reset();
});
$("#btnGroupClose").click(function () {
    var url = "/Dashboard/Index";
    window.location.href = url;
});