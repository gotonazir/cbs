﻿var personalOrCorpData =
    [
        {
            "id": 1,
            "text": "P"
        },
        {
            "id": 2,
            "text": "C"
        }
    ];

var treeViewData = [];
var selectedAccountTreeTypeId = 0;


$(document).ready(function () {
    onAccountFormLoad();
    // TODO Random number logic 
    $('#AccountNo').val(Math.floor(Math.random() * 1E12));
    bindCurrency();
    $('#btnAccountSubmit').show();
    $('#btnAccountUpdate').hide();
    
});

var jsonCurrency = [
    { CurrencyID: 1, Name: 'Saudi Riyal', ShortNameEn: 'SRA', icon: 'fa fa-money' },
    { CurrencyID: 2, Name: 'Rupees', ShortNameEn: 'Rs', icon: 'fa fa-rupee' },
    { CurrencyID: 3, Name: 'Dollar', ShortNameEn: 'doll', icon: 'fa fa-dollar' },
    { CurrencyID: 4, Name: 'Pounds', ShortNameEn: '£', icon: 'fa fa-euro' }
];

function bindCurrency() {
    var get_currencies = jqxhr('get', 'api/currencyapi/Get');

    get_currencies.done(function (data) {
        jsonCurrency = [];
        jsonCurrency = data;
        debugger;
        for (var i = 0; i < jsonCurrency.length; i++) {
            if (jsonCurrency[i].Name == 'Saudi Riyal')
                jsonCurrency[i].IconName = 'fa fa-money';
            else if (jsonCurrency[i].Name == 'Rupees') {
                jsonCurrency[i].IconName = 'fa fa-rupee';
            }
            else if (jsonCurrency[i].Name == 'Dollar') {
                jsonCurrency[i].IconName = 'fa fa-dollar';
            }
            else if (jsonCurrency[i].Name == 'Pounds') {
                jsonCurrency[i].IconName = 'fa fa-euro';
            } else {
                // default
                jsonCurrency[i].IconName = 'fa fa-money';
            }
        }
        loadCurrency();
        GetAccountDetailsById();
    });

    get_currencies.fail(function (r) {
        console.log(r);
    });
}

function loadCurrency() {
    for (var i = 0; i < jsonCurrency.length; i++) {

        $('#currency').append(' <div class="row"> \
        <div class= "custom-control custom-checkbox mb-2 col-md-1" > \
        <input type="checkbox" class="custom-control-input" id="customCheck'+ i + '" name="example1" /> \
        <label class="custom-control-label" for="customCheck'+ i + '"> </label> \
          <input type="hidden" id="hdncurrencyId'+ i + '" value="' + jsonCurrency[i].CurrencyID + '" />  \
          <input type="hidden" id="hdnacccurrencyId'+ i + '" value="0" />  \
        </div> \
            <div class="col-md-2"> \
         <label id="currencyName'+ i + '" class="">' + jsonCurrency[i].ShortNameEn + '<i style="color:red;" class="' + jsonCurrency[i].IconName + '"></i>&nbsp;&nbsp;</label> \
        </div> \
        <div class="col-md-2"> \
        <label style="float: right;" for="minbal'+ i + '">Min Balance</label> \
        </div> \
        <div class="col-md-2"> \
        <input class="form-control text-box single-line" id="minbal'+ i + '" name="minbal' + i + '" type="number" value="" /> \
        <span class="field-validation-valid text-danger" data-valmsg-for="minbal'+ i + '" data-valmsg-replace="true"></span> \
        </div> \
        <div class="col-md-2"> \
        <label for="draft">Over Draft</label> \
        </div> \
        <div class="col-md-2"> \
        <input class="form-control text-box single-line" id="draft'+ i + '" name="draft' + i + '" type="number" value="" /> \
        <span class="field-validation-valid text-danger" data-valmsg-for="draft'+ i + '" data-valmsg-replace="true"></span> \
        </div> \
        </div>');
    }

}

function onAccountFormLoad() {
    $("#OpenDt").datepicker({ minDate: 0 });
    $("#CloseDt").datepicker({ minDate: 0 });
    BindClientDropDown();
    var get_BranchNames = jqxhr('get', 'api/branchapi/BranchNames');

    get_BranchNames.done(function (response) {
        $("#BranchID").select2({
            placeholder: "Select Branch",
            data: response
        });
        $("#BranchID").select2("val", "-1");
    });
    var get_cities = jqxhr('get', 'api/CityCountryApi/CityNames');
    get_cities.done(function (response) {
        $("#CityID").select2({
            placeholder: "Select City",
            data: response
        });
        $("#CityID").select2("val", "-1");
        $("#ACityID").select2({
            placeholder: "Select City",
            data: response
        });
        $("#ACityID").select2("val", "-1");
        $("#BCityID").select2({
            placeholder: "Select City",
            data: response
        });
        $("#BCityID").select2("val", "-1");
    });
}

function buildtreeview(jsonData) {
    $("#jstree-tree").jstree({
        "json_data": {
            "data": treeViewData
        },
        "plugins": ["themes", "json_data", "ui"]
    });
};

function createJSTree(jsondata) {
    $('#jstree-tree').jstree({
        'core': {
            'data': jsondata
        }
    });
}

function BindClientDropDown() {
    var get_clientNames = jqxhr('get', 'api/ClientApi/ClientNames');
    get_clientNames.done(function (response) {
        $("#ClientID").select2({
            placeholder: "Select Client",
            data: response
        }).on('select2:select', function (event) {
            var get_clients = jqxhr('get', 'api/clientapi/Get?id=' + $('#ClientID').val());

            get_clients.done(function (data) {
                $('#ClientName').val(data.FullName);
                $('#Address').val(data.Address);
                if (data.CityID != '' && data.CityID != null) {
                    $("#CityID").select2("val", data.CityID.toString());
                }
                $('#Mobile').val(data.Mobile);
                if (data.PersonOrCorp != '' && data.PersonOrCorp != null) {
                    $("#PersonOrCorp").select2("val", data.PersonOrCorp.toString());
                }
            });
        });
        $("#ClientID").select2("val", "-1");
        $("#BeneficiaryID").select2({
            placeholder: "Select Beneficiary",
            data: response
        }).on('select2:select', function (event) {
            var get_clients = jqxhr('get', 'api/clientapi/Get?id=' + $('#BeneficiaryID').val());

            get_clients.done(function (data) {
                $('#BeneficiaryName').val(data.FullName);
                $('#BAddress').val(data.Address);
                if (data.CityID != '' && data.CityID != null) {
                    $("#BCityID").select2("val", data.CityID.toString());
                }
                $('#BMobile').val(data.Mobile);
                if (data.PersonOrCorp != '' && data.PersonOrCorp != null) {
                    $("#BPersonOrCorp").select2("val", data.PersonOrCorp.toString());
                }
            });
        });
        $("#BeneficiaryID").select2("val", "-1");
        $("#AuthorizedID").select2({
            placeholder: "Select Authorized",
            data: response
        }).on('select2:select', function (event) {
            var get_clients = jqxhr('get', 'api/clientapi/Get?id=' + $('#AuthorizedID').val());

            get_clients.done(function (data) {
                $('#Authorized').val(data.FullName);
                $('#AAddress').val(data.Address);
                if (data.CityID != '' && data.CityID != null) {
                    $("#ACityID").select2("val", data.CityID.toString());
                }
                $('#AMobile').val(data.Mobile);
                if (data.PersonOrCorp != '' && data.PersonOrCorp != null) {
                    $("#APersonOrCorp").select2("val", data.PersonOrCorp.toString());
                }
            });
        });
        $("#AuthorizedID").select2("val", "-1");

        $("#PersonOrCorp").select2({
            placeholder: "Select Personal Or Corporation",
            data: personalOrCorpData
        });
        $("#PersonOrCorp").select2("val", "-1");

        $("#BPersonOrCorp").select2({
            placeholder: "Select Personal Or Corporation",
            data: personalOrCorpData
        });
        $("#BPersonOrCorp").select2("val", "-1");

        $("#APersonOrCorp").select2({
            placeholder: "Select Personal Or Corporation",
            data: personalOrCorpData
        });
        $("#APersonOrCorp").select2("val", "-1");

    });
};

$('#btnClientSubmit').click(function () {
    var clientModel = {
        FullName: $('#ClientName').val(),
        Address: $('#Address').val(),
        CityID: $('#CityID').val(),
        Mobile: $('#Mobile').val(),
        PersonOrCorp: $('#PersonOrCorp').val()
    }
    clientAjaxApi(clientModel);
});

$('#btnBeneficiarySubmit').click(function () {

    var clientModel = {
        FullName: $('#BeneficiaryName').val(),
        Address: $('#BAddress').val(),
        CityID: $('#BCityID').val(),
        Mobile: $('#BMobile').val(),
        PersonOrCorp: $('#BPersonOrCorp').val()
    }
    clientAjaxApi(clientModel);
});

$('#btnAuthorizeSubmit').click(function () {

    var clientModel = {
        FullName: $('#Authorized').val(),
        Address: $('#AAddress').val(),
        CityID: $('#ACityID').val(),
        Mobile: $('#AMobile').val(),
        PersonOrCorp: $('#APersonOrCorp').val()
    }
    clientAjaxApi(clientModel);
});

function clientAjaxApi(clientModel) {
    var add_Client = jqxhr('post', 'api/clientApi/post', clientModel);

    add_Client.done(function (r) {
        BindClientDropDown();
        $('#dlg').html(CoreBankingSuccessDialog());
        $("#myModal").modal();
        // Clear fields
        $('#ClientName').val('');
        $('#Address').val('');
        $("#CityID").select2("val", "-1");
        $('#Mobile').val();
        $("#PersonOrCorp").select2("val", "-1");

        $('#BeneficiaryName').val('');
        $('#BAddress').val('');
        $("#BCityID").select2("val", "-1");
        $('#BMobile').val();
        $("#BPersonOrCorp").select2("val", "-1");

        $('#Authorized').val('');
        $('#AAddress').val('');
        $("#ACityID").select2("val", "-1");
        $('#AMobile').val();
        $("#APersonOrCorp").select2("val", "-1");
    });

    add_Client.fail(function (r) {
        $('#dlg').html(CoreBankingErrorDialog(r.responseJSON.ExceptionMessage));
        $("#myModal").modal();
    });
}

$('#btnClientCancel').click(function () {
    $('#ClientName').val('');
    $('#Address').val('');
    $("#CityID").select2("val", "-1");
    $('#Mobile').val('');
    $("#PersonOrCorp").select2("val", "-1");
});

$('#btnBeneficiaryCancel').click(function () {
    $('#BeneficiaryName').val('');
    $('#BAddress').val('');
    $("#BCityID").select2("val", "-1");
    $('#BMobile').val('');
    $("#BPersonOrCorp").select2("val", "-1");
});

$('#btnAuthorizeCancel').click(function () {
    $('#Authorized').val('');
    $('#AAddress').val('');
    $("#ACityID").select2("val", "-1");
    $('#AMobile').val('');
    $("#APersonOrCorp").select2("val", "-1");
});

$('#btnAccountSubmit').click(function () {
    var accountCurrency = [];
    for (var i = 0; i < 5; i++) {
        if ($('#currency').children().eq(i).children().find('#customCheck' + i + '').length !== 0) {
            var isCurrencyChecked = $('#currency').children().eq(i).children().find('#customCheck' + i + '')[0].checked;
            var minBalance = $('#currency').children().eq(i).children().find('#minbal' + i + '').val();
            var overDraft = $('#currency').children().eq(i).children().find('#draft' + i + '').val();
            var currencyId = $('#currency').children().eq(i).children().find('#hdncurrencyId' + i + '').val();
            if (isCurrencyChecked) {
                if (minBalance == "") {
                    alert("Please give Minimum balance for " +
                        $('#currency').children().eq(i).children().find('#currencyName' + i + '')[0].innerText);
                    return;
                }
                if (overDraft == "") {
                    alert("Please give Over Draft for" +
                        $('#currency').children().eq(i).children().find('#currencyName' + i + '')[0].innerText);
                    return;
                }
                accountCurrency.push({
                    CurrencyID: currencyId,
                    Balance: '',
                    MinBalance: minBalance,
                    OverDraft: overDraft,
                    RecordStatus: 'A'
                });
            }
        }
    }

    if ($('#BranchID').val() === '') {
        alert("Please Select Branch");
        return;
    }
    else if ($('#ClientID').val() === '') {
        alert("Please Select Client");
        return;
    }

    else if (selectedAccountTreeTypeId === 0) {
        alert("Please Select Account Type");
        return;
    }

    // if ($('#accountForm').valid()) {
    var model = {
        AccountNo: $('#AccountNo').val(),
        BranchID: $('#BranchID').val(),
        ClientID: $('#ClientID').val(),
        OpenDt: $('#OpenDt').val(),
        VIPNo: $('#VIPNo').val(),
        BeneficiaryID: $('#BeneficiaryID').val(),
        AuthorizedID: $('#AuthorizedID').val(),
        Notes: $('#Notes').val(),
        CloseDt: $('#CloseDt').val(),
        RecordStatus: 'A',
        AccountTypeTreeId: selectedAccountTreeTypeId,
        AccountCurrencies: accountCurrency
    };

    var insert_Account = jqxhr('Post', 'api/accountApi/Post', model);

    insert_Account.done(function (r) {
        $('#dlg').html(CoreBankingSuccessDialog());
        $("#myModal").modal();
    });

    insert_Account.fail(function (r) {
        $('#dlg').html(CoreBankingErrorDialog(r.responseJSON.ExceptionMessage));
        $("#myModal").modal();
    });
    //}
});


var accounttreeData = [
    { "id": 1, "parent": "#", "text": "Assets" }, { "id": 2, "parent": 1, "text": "Fixed Assets" },
    { "id": 3, "parent": 2, "text": "Lands" }, { "id": 4, "parent": 2, "text": "Buildings" },
    { "id": 5, "parent": 2, "text": "Cars" }, { "id": 6, "parent": 2, "text": "Furniture" },
    { "id": 7, "parent": 2, "text": "Establishment Expenses" },
    { "id": 8, "parent": 2, "text": "Projects in Progress" }, { "id": 9, "parent": 1, "text": "Movable Assets" },
    { "id": 10, "parent": 9, "text": "Available Cash" }, { "id": 11, "parent": 10, "text": "Sandooqs" },
    { "id": 12, "parent": 10, "text": "Banks" }, { "id": 13, "parent": 10, "text": "Cheques" },
    { "id": 14, "parent": 9, "text": "Branches & Agents" }, { "id": 15, "parent": 14, "text": "Branches" },
    { "id": 16, "parent": 14, "text": "Local Agents" }, { "id": 17, "parent": 14, "text": "International Agents" },
    { "id": 18, "parent": 14, "text": "Money Exchange and Traders" },
    { "id": 19, "parent": 14, "text": "Other Agents" }, { "id": 20, "parent": 14, "text": "Blocked Agents" },
    { "id": 21, "parent": 9, "text": "Debitors" }, { "id": 22, "parent": 21, "text": "Individual Debitors" },
    { "id": 23, "parent": 21, "text": "Comany Debitors" }, { "id": 24, "parent": 21, "text": "Labours & Employees" },
    { "id": 25, "parent": "#", "text": "Liabilities" }, { "id": 27, "parent": 25, "text": "Copyrights" },
    { "id": 28, "parent": 25, "text": "Creditors" }, { "id": 29, "parent": 27, "text": "Individual Creditors" },
    { "id": 30, "parent": 27, "text": "Company Creditors" }, { "id": 31, "parent": 27, "text": "Other Creditors" },
    { "id": 32, "parent": 27, "text": "Savings" }, { "id": 33, "parent": 27, "text": "Dormant Accounts" },
    { "id": 34, "parent": 27, "text": "Results Accounts" }, { "id": 35, "parent": "#", "text": "Expenses" },
    { "id": 36, "parent": 35, "text": "Management Expenses" }, { "id": 37, "parent": 35, "text": "General Expenses" },
    { "id": 38, "parent": "#", "text": "Revenue" }, { "id": 39, "parent": 38, "text": "Activity Revenue" },
    { "id": 40, "parent": 38, "text": "Transfer Revenue" }, { "id": 41, "parent": 38, "text": "Exchange Revenue" },
    { "id": 42, "parent": 38, "text": "Other Revenue" }
];
$('#jstree-tree')
    .on('changed.jstree', function (e, data) {
        selectedAccountTreeTypeId = 0;
        var objNode = data.instance.get_node(data.selected);
        $('#jstree-result').html('Selected: <br /><strong>' + objNode.id + '-' + objNode.text + '</strong>');
        selectedAccountTreeTypeId = objNode.id;
    })
    .on('loaded.jstree', function (e, data) {
        setTimeout(function () {
             data.instance.set_state({ core: { open: [0], selected: [selectedAccountTreeTypeId] } });
        },6000);
        
    })
    .jstree({
        core: {
            data: accounttreeData //jsonData
        }
    });


$('#btnClose').click(function () {
    window.location.href($('#hdnBackToDashboard').val());
});

function GetAccountDetailsById() {
    if (location.search != "") {
        var accountId = new URLSearchParams(location.search).get('accountId');
        if (accountId != null) {
            $('#btnAccountSubmit').hide();
            $('#btnAccountUpdate').show();
            var getaccount = jqxhr('get', 'api/accountApi/Get?id=' + accountId + '&fromScreen=reg');

            getaccount.done(function (data) {
                $('#AccountNo').val(data.AccountNo);
                $('#OpenDt').val(data.OpenDt);
                $('#VIPNo').val(data.VIPNo);
                $('#Notes').val(data.Notes);
                $('#CloseDt').val(data.CloseDt);
                $('#Fax').val(data.Fax);

                for (var i = 0; i < data.AccountCurrencies.length; i++) {

                    var curr = $.grep(jsonCurrency, function (value, index) {
                        return value.CurrencyID == data.AccountCurrencies[i].CurrencyID;
                    });
                    if (curr.length > 0) {
                        $('#customCheck'+(curr[0].CurrencyID-1)+'')[0].checked = true;
                        $('#minbal'+(curr[0].CurrencyID-1)+'').val(data.AccountCurrencies[i].MinBalance);
                        $('#draft'+(curr[0].CurrencyID-1)+'').val(data.AccountCurrencies[i].OverDraft);
                        $('#hdnacccurrencyId' + (curr[0].CurrencyID - 1) + '').val(data.AccountCurrencies[i].AccountCurrencyID);
                    }
                }



                if (data.BranchID != '' && data.BranchID != null) {
                    $("#BranchID").select2("val", data.BranchID.toString());
                }
                if (data.ClientID != '' && data.ClientID != null) {
                    $("#ClientID").select2("val", data.ClientID.toString());
                }
                if (data.BeneficiaryID != '' && data.BeneficiaryID != null) {
                    $("#BeneficiaryID").select2("val", data.BeneficiaryID.toString());
                }
                if (data.AuthorizedID != '' && data.AuthorizedID != null) {
                    $("#AuthorizedID").select2("val", data.AuthorizedID.toString());
                }
                selectedAccountTreeTypeId = data.AccountTypeTreeId;

                // Bind Client Info based on Client Name
                var get_clients = jqxhr('get', 'api/clientapi/Get?id=' + data.ClientID);

                get_clients.done(function (data) {
                    $('#ClientName').val(data.FullName);
                    $('#Address').val(data.Address);
                    if (data.CityID != '' && data.CityID != null) {
                        $("#CityID").select2("val", data.CityID.toString());
                    }
                    $('#Mobile').val(data.Mobile);
                    if (data.PersonOrCorp != '' && data.PersonOrCorp != null) {
                        $("#PersonOrCorp").select2("val", data.PersonOrCorp.toString());
                    }
                });

                //Bind  Beneficiary info based on Beneficiary Name
                var get_Beneficiary = jqxhr('get', 'api/clientapi/Get?id=' + data.BeneficiaryID);
                get_Beneficiary.done(function (data) {
                    $('#BeneficiaryName').val(data.FullName);
                    $('#BAddress').val(data.Address);
                    if (data.CityID != '' && data.CityID != null) {
                        $("#BCityID").select2("val", data.CityID.toString());
                    }
                    $('#BMobile').val(data.Mobile);
                    if (data.PersonOrCorp != '' && data.PersonOrCorp != null) {
                        $("#BPersonOrCorp").select2("val", data.PersonOrCorp.toString());
                    }
                });

                // Bind Authorized info based on Authorized Name
                var get_clients = jqxhr('get', 'api/clientapi/Get?id=' + data.AuthorizedID);

                get_clients.done(function (data) {
                    $('#Authorized').val(data.FullName);
                    $('#AAddress').val(data.Address);
                    if (data.CityID != '' && data.CityID != null) {
                        $("#ACityID").select2("val", data.CityID.toString());
                    }
                    $('#AMobile').val(data.Mobile);
                    if (data.PersonOrCorp != '' && data.PersonOrCorp != null) {
                        $("#APersonOrCorp").select2("val", data.PersonOrCorp.toString());
                    }
                });
                // Account Currency Map

            });
        }
    }
}

$('#btnAccountUpdate').click(function () {

    var accountCurrency = [];  //TODO remove 5 number
    for (var i = 0; i < 5; i++) {
        if ($('#currency').children().eq(i).children().find('#customCheck' + i + '').length !== 0) {
            var isCurrencyChecked = $('#currency').children().eq(i).children().find('#customCheck' + i + '')[0].checked;
            var minBalance = $('#currency').children().eq(i).children().find('#minbal' + i + '').val();
            var overDraft = $('#currency').children().eq(i).children().find('#draft' + i + '').val(); 
            var currencyId = $('#currency').children().eq(i).children().find('#hdncurrencyId' + i + '').val();
            var acccurrencyId = $('#currency').children().eq(i).children().find('#hdnacccurrencyId' + i + '').val();
            if (isCurrencyChecked) {
                if (minBalance == "") {
                    alert("Please give Minimum balance for " +
                        $('#currency').children().eq(i).children().find('#currencyName' + i + '')[0].innerText);
                    return;
                }
                if (overDraft == "") {
                    alert("Please give Over Draft for" +
                        $('#currency').children().eq(i).children().find('#currencyName' + i + '')[0].innerText);
                    return;
                }
                accountCurrency.push({
                    AccountCurrencyID:acccurrencyId,
                    CurrencyID: currencyId,
                    Balance: '',
                    MinBalance: minBalance,
                    OverDraft: overDraft,
                    RecordStatus: 'A'
                });
            }
        }
    }

    if ($('#BranchID').val() === '') {
        alert("Please Select Branch");
        return;
    }
    else if ($('#ClientID').val() === '') {
        alert("Please Select Client");
        return;
    }

    else if (selectedAccountTreeTypeId === 0) {
        alert("Please Select Account Type");
        return;
    }
    var accountId = new URLSearchParams(location.search).get('accountId');
    // if ($('#accountForm').valid()) {
    var model = {
        AccountID: accountId,
        AccountNo: $('#AccountNo').val(),
        BranchID: $('#BranchID').val(),
        ClientID: $('#ClientID').val(),
        OpenDt: $('#OpenDt').val(),
        VIPNo: $('#VIPNo').val(),
        BeneficiaryID: $('#BeneficiaryID').val(),
        AuthorizedID: $('#AuthorizedID').val(),
        Notes: $('#Notes').val(),
        CloseDt: $('#CloseDt').val(),
        RecordStatus: 'A',
        AccountTypeTreeId: selectedAccountTreeTypeId,
        AccountCurrencies: accountCurrency
    };

    var update_Account = jqxhr('Put', 'api/accountApi/Put', model);

    update_Account.done(function (r) {
        $('#dlg').html(CoreBankingSuccessDialog());
        $("#myModal").modal();
    });

    update_Account.fail(function (r) {
        $('#dlg').html(CoreBankingErrorDialog(r.responseJSON.ExceptionMessage));
        $("#myModal").modal();
    });

});
