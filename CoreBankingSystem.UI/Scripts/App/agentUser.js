﻿
$(document).ready(function () {
    onAgentUserFormLoad();
});

function onAgentUserFormLoad() {

    $('#btnAgentUserEdit').hide();
    $('#btnAgentUserSave').show();

    var get_AgentNames = jqxhr('get', 'api/agentapi/AgentNames');

    get_AgentNames.done(function (response) {
        $("#AgentID").select2({
            placeholder: "Select Agent",
            data: response
        });
        //$("#AgentID").select2("val", "-1");
    });

    if (location.search !== "") {
        var agentUserId = new URLSearchParams(location.search).get('agentUserId');
        if (agentUserId !== null) {
            var get_agentUsers = jqxhr('get', 'api/agentUserapi/Get?id=' + agentUserId + '&fromScreen=reg');
             get_agentUsers.done(function (data) {
                console.log(data); // response object                
                $('#AgentUserID').val(data.AgentUserID);
                $('#AgentID').val(data.AgentID);
                $('#UserName').val(data.UserName);
                $('#Notes').val(data.Notes);
                $('#btnAgentUserSave').hide();
                $('#btnAgentUserEdit').show();
            });

            get_agentUsers.fail(function (r) {
                console.log(r);
            });
        }
    }
}

$('#btnAgentUserSave').click(function () {
    if ($('#agentUserForm').valid()) {
        var agentUserModel = {           
            AgentID: $('#AgentID').val(),
            UserName: $('#UserName').val(),
            Notes: $('#Notes').val()
        };

        var addUserAgent = jqxhr('post', 'api/agentUserapi/post', agentUserModel);

        addUserAgent.done(function (r) {

            $('#dlg').html(CoreBankingSuccessDialog());
            $("#myModal").modal();
            document.getElementById("agentUserForm").reset();

        });

        addAgentUser.fail(function (r) {
            $('#dlg').html(CoreBankingErrorDialog(r.responseJSON.ExceptionMessage));
            $("#myModal").modal();
        });
    }

});

$('#btnAgentUserEdit').click(function () {
    if ($('#agentUserForm').valid()) {
        var agentUserId = new URLSearchParams(location.search).get('agentUserId');
        var agentUserModel = {
            AgentUserID: agentUserId,
            AgentID: $('#AgentID').val(),
            UserName: $('#UserName').val(),
            Notes: $('#Notes').val()
        };

        var edit_agentUser = jqxhr('put', 'api/agentUserapi/put', agentUserModel);

        edit_agentUser.done(function (r) {
            $('#dlg').html(CoreBankingSuccessDialog());
            $("#myModal").modal();  
        }); 

        edit_agentUser.fail(function (r) {
            $('#dlg').html(CoreBankingErrorDialog(r.responseJSON.ExceptionMessage));
            $("#myModal").modal();
        });
    }
});

$('#btnAgentUserReset').click(function () {
    document.getElementById("agentUserForm").reset();
});
$("#btnAgentUserClose").click(function () {
    var url = "/Dashboard/Index";
    window.location.href = url;
});