﻿
$(document).ready(function () { 
    debugger;
    onAgentFormLoad();
});

function onAgentFormLoad() {
    $('#btnAgentEdit').hide();
    $('#btnAgentSave').show();

    var get_Accounts = jqxhr('get', 'api/accountapi/AccountNumbers');

    get_Accounts.done(function (response) {
        $("#AccountID").select2({
            placeholder: "Select Account",
            data: response
        });
        //$("#AccountID").select2("val", "-1");
    });
    
    if (location.search != "") {
        var agentId = new URLSearchParams(location.search).get('agentId');
        if (agentId != null) {
            console.log('AgentId' + agentId);
            var get_agentes = jqxhr('get', 'api/agentapi/Get?id=' + agentId + '&fromScreen=reg');

            get_agentes.done(function (data) {
                console.log(data); // response object                
                $('#AgentNo').val(data.AgentNo);
                $('#AgentName').val(data.AgentName);
                $('#AccountID').val(data.AccountID);
                $('#EnglishName').val(data.EnglishName);
                $('#Phone').val(data.Phone);
                $('#Fax').val(data.Fax);
                $('#Adress').val(data.Adress);
                $('#Notes').val(data.Notes);                
                $('#btnAgentSave').hide();
                $('#btnAgentEdit').show();
            });

            get_users.fail(function (r) {
                console.log(r); 
            });
        }
    }
}

$('#btnAgentSave').click(function () {
    if ($('#agentForm').valid()) {
        var agentModel = {
            AgentNo: $('#AgentNo').val(),
            AgentName: $('#AgentName').val(),
            AccountID: $('#AccountID').val(),
            EnglishName: $('#EnglishName').val(),
            Phone: $('#Phone').val(),
            Fax: $('#Fax').val(),
            Adress: $('#Adress').val(),
            Notes: $('#Notes').val()
        };

        var addAgent = jqxhr('post', 'api/agentapi/post', agentModel);

        addAgent.done(function (r) {
            
            $('#dlg').html(CoreBankingSuccessDialog());          
            $("#myModal").modal();           
            document.getElementById("agentForm").reset();

        });

        addAgent.fail(function (r) {
            $('#dlg').html(CoreBankingErrorDialog(r.responseJSON.ExceptionMessage));
            $("#myModal").modal();
        });
    }

});

$('#btnAgentEdit').click(function () {
    if ($('#agentForm').valid()) {
        var agentId = new URLSearchParams(location.search).get('agentId');
        var agentModel = {
            AgentID: agentId,
            AgentNo: $('#AgentNo').val(),
            AgentName: $('#AgentName').val(),
            AccountID: $('#AccountID').val(),
            EnglishName: $('#EnglishName').val(),
            Phone: $('#Phone').val(),
            Fax: $('#Fax').val(),
            Adress: $('#Adress').val(),
            Notes: $('#Notes').val()           
        };

        var edit_agent = jqxhr('put', 'api/agentapi/put', agentModel);

        edit_agent.done(function (r) {
            $('#dlg').html(CoreBankingSuccessDialog());
            $("#myModal").modal();  
        });

        edit_agent.fail(function (r) {
            $('#dlg').html(CoreBankingErrorDialog(r.responseJSON.ExceptionMessage));
            $("#myModal").modal();
        });
    }
});

$('#btnAgentReset').click(function () {
    document.getElementById("agentForm").reset();
});
$("#btnAgentClose").click(function () {
    var url = "/Dashboard/Index";
    window.location.href = url;
});