﻿
var personalOrCorpData =
    [
        {
            "id": 1,
            "text": "P"
        },
        {
            "id": 2,
            "text": "C"
        }
    ];


var signature = null;
var base64 = '';
var currentRow = null;
var docBase64 = '';
var docId = 0;

$(document).ready(function () {
    //$('div[onload]').trigger('onload');
    //$("#ExpiryDt").datepicker();
   // $("#IssueDt").datepicker();
    //$("#ExpiryDt1").datepicker();
    //$("#IssueDt1").datepicker();
    //$("#ExpiryDt2").datepicker();
    //$("#IssueDt2").datepicker();
    $("#ExpiryDt").datepicker({ minDate: 0 });

    var date = new Date();
    var currentMonth = date.getMonth();
    var currentDate = date.getDate();
    var currentYear = date.getFullYear();
    //$("#IssueDt").datepicker("option", "maxDate", new Date(currentYear, currentMonth, currentDate));

    $("#IssueDt").datepicker({ maxDate: 0 });
    // or $( "#ExpiryDt" ).datepicker({ minDate: new Date()});
    onClientFormLoad();
});

function onClientFormLoad() {

    document.querySelector('#SignImage').addEventListener('change', function () {
        var reader = new FileReader();
        var selectedFile = this.files[0];

        reader.onload = function () {
            var comma = this.result.indexOf(',');
            base64 = this.result.substr(comma + 1);
            signature = base64;
            //console.log(base64);
        }
        reader.readAsDataURL(selectedFile);
    }, false);

    document.querySelector('#DocumentImage').addEventListener('change', function () {
        var reader = new FileReader();
        var selectedFile = this.files[0];

        reader.onload = function () {
            var comma = this.result.indexOf(',');
            base64 = this.result.substr(comma + 1);
            signature = base64;
            //console.log(base64);
        }
        reader.readAsDataURL(selectedFile);
    }, false);

    var get_users = jqxhr('get', 'api/ClientApi/ClientNames');
    var get_cities = jqxhr('get', 'api/CityCountryApi/CityNames');

    get_users.done(function (response) {
        $("#GuarantorID").select2({
            placeholder: "Select Guarantor",
            data: response
        });
        $("#PrivilegeID").select2({
            placeholder: "Select Privilege",
            data: response
        });
        

        //$("#GuarantorID").select2("val", "-1");

        //$("#PrivilegeID").select2("val", "-1");

        
    });

    get_cities.done(function (response) {
        $("#CityID").select2({
            placeholder: "Select City",
            data: response
        });
        //$("#CityID").select2("val", "-1");
    
    });

    get_users.fail(function (error) {
        console.log(error); // response object
    });

    get_cities.fail(function (error) {
        console.log(error); // response object
    });

    $("#PersonOrCorp").select2({
        placeholder: "Select Person Or Corp",
        data: personalOrCorpData
    });

    $("#PersonOrCorp").select2("val", "-1");

    $('#btnClientEdit').hide();
    $('#btnClientSubmit').show();

    if (location.search != "") {
        var clientId = new URLSearchParams(location.search).get('clientId');
        if (clientId != null) {
            var get_clients = jqxhr('get', 'api/clientapi/Get?id=' + clientId + '&fromScreen=reg');

            get_clients.done(function (data) {
                $('#FullName').val(data.FullName);
                $('#Address').val(data.Address);
                $('#CityID').val(data.CityID);
                $('#Phone').val(data.Phone);
                $('#Mobile').val(data.Mobile);
                $('#Fax').val(data.Fax);
                $('#PersonOrCorp').val(data.PersonOrCorp);
                if (data.GuarantorID != '' && data.GuarantorID != null) {
                    $("#GuarantorID").select2("val", data.GuarantorID.toString());
                }
                $('#PrivilegeID').val(data.PrivilegeID);
                $('#Notes').val(data.Notes);
               // $('#SignImage').val(data.SignImage); TODO
                $('#signPhoto').prop('src', 'data:image/*;base64,' + data.SignBase64);
                $('#btnClientSubmit').hide();
                $('#btnClientEdit').show();
                $.each(data.ClientDocuments, function (key, value) {
                    var DocumentId = value.ClientDocumentID;
                    var DocumentType = value.DocumentType != null ? value.DocumentType : '';
                    var PlaceOfIssue = value.PlaceOfIssue != null ? value.PlaceOfIssue : '';
                    var IssueDt = value.IssueDt != null ? value.IssueDt : '';
                    var ExpiryDt = value.ExpiryDt != null ? value.ExpiryDt : '';
                    var DocumentBase64 = value.DocumentBase64 != null ? value.DocumentBase64 : '';
               
                var imgElement = '<img id="docImg" class="rounded" width="200" height="200" onClick=test()/>';
                    var img = $(imgElement).prop('src', 'data:image/*;base64,' + DocumentBase64);
               
                var newRow = "<tr><td><input type='checkbox' name='record'></td><td class='DocumentType'>" + DocumentType + "</td><td class='PlaceOfIssue'>" + PlaceOfIssue + "</td>" +
                    "<td class='IssueDt'>" + IssueDt + "</td>" +
                    "<td class='ExpiryDt'>" + ExpiryDt + "</td>" +
                    "<td class='photo'>" + img[0].outerHTML + "</td>" +
                    "<td><span class='editrow'><a class='fa fa-edit' href='javascript: void(0);'>Edit</a></span></td>" +
                    "<td class='DocumentId'>" + DocumentId + "</td>" +
                    "</tr>";
               
                    $("table tbody").append(newRow);
                });
            });

            get_clients.fail(function (r) {
                console.log(r);
            });
        }
    }

}

$('#btnClientSubmit').click(function () {

    if ($('#clientForm').valid()) {

        var clientDocuments = [];

        var table = $("#clientDocumentTable tbody");

        table.find('tr').each(function (i) {
            var $tds = $(this).find('td');
            clientDocuments.push(
                {
                    DocumentType: $tds.eq(1).text(),
                    PlaceOfIssue: $tds.eq(2).text(),
                    IssueDt: $tds.eq(3).text(),
                    ExpiryDt: $tds.eq(4).text(),
                    DocumentImage: $tds.eq(5).text(),
                    RecordStatus: 'A'
                });

        });

        var clientModel = {
            FullName: $('#FullName').val(),
            Address: $('#Address').val(),
            CityID: $('#CityID').val(),
            Phone: $('#Phone').val(),
            Mobile: $('#Mobile').val(),
            Fax: $('#Fax').val(),
            PersonOrCorp: $('#PersonOrCorp').val(),
            GuarantorID: $('#GuarantorID').val(),
            PrivilegeID: $('#PrivilegeID').val(),
            Notes: $('#Notes').val(),
            SignBase64: base64,
            DocumentBase64: base64,
            ClientDocuments: clientDocuments
        }
        var add_Client = jqxhr('post', 'api/clientApi/post', clientModel);

        add_Client.done(function (r) {
            signature = null;
            console.log('sucess');
            console.log(base64ToByteArray(base64));
            $('#dlg').html(CoreBankingSuccessDialog());
            $("#myModal").modal();
        });

        add_Client.fail(function (r) {
            console.log(r.responseJSON.ExceptionMessage);
            $('#dlg').html(CoreBankingErrorDialog(r.responseJSON.ExceptionMessage));
            $("#myModal").modal();
        });

    }

});

function base64ToByteArray(base64String) {
    try {
        var sliceSize = 1024;
        var byteCharacters = atob(base64String);
        var bytesLength = byteCharacters.length;
        var slicesCount = Math.ceil(bytesLength / sliceSize);
        var byteArrays = new Array(slicesCount);

        for (var sliceIndex = 0; sliceIndex < slicesCount; ++sliceIndex) {
            var begin = sliceIndex * sliceSize;
            var end = Math.min(begin + sliceSize, bytesLength);

            var bytes = new Array(end - begin);
            for (var offset = begin, i = 0; offset < end; ++i, ++offset) {
                bytes[i] = byteCharacters[offset].charCodeAt(0);
            }
            byteArrays[sliceIndex] = new Uint8Array(bytes);
        }
        return byteArrays;
    } catch (e) {
        console.log("Couldn't convert to byte array: " + e);
        alert("Couldn't convert to byte array: " + e);
        return undefined;
    }
}

$("#btnAddClientDocument").click(function () {
    var clientId = new URLSearchParams(location.search).get('clientId');
    //if (!$('#clientForm').valid()) {
    //    return;
    //}
    var DocumentId = 0;
    var DocumentType = $('#DocumentType').val();
    var PlaceOfIssue = $('#PlaceOfIssue').val();
    var IssueDt = $('#IssueDt').val();
    var ExpiryDt = $('#ExpiryDt').val();
    var DocumentImage = $('#DocumentImage').val();
    var RecordStatus = 'A';
    if (base64 == "") {
        base64 = docBase64.replace('data:image/*;base64,', '');
    }

    var imgElement = '<img id="docImg" class="rounded" width="200" height="200" />';
    var img;
    if (currentRow == null) {
         img = $(imgElement).prop('src', 'data:image/*;base64,' + base64 );
    } else {
         img = $(imgElement).prop('src', 'data:image/*;base64,' + base64);
    }
   

    //var markup = "<tr><td><input type='checkbox' name='record'></td><td>" + name + "</td><td>" + email + "</td></tr>";
    var newRow = "<tr><td><input type='checkbox' name='record'></td><td class='DocumentType'>" + DocumentType + "</td><td class='PlaceOfIssue'>" + PlaceOfIssue + "</td>" +
        "<td class='IssueDt'>" + IssueDt + "</td>" +
        "<td class='ExpiryDt'>" + ExpiryDt + "</td>" +
        //"<td class='DocumentImage'>" + DocumentImage + "</td>" +
        "<td class='photo'>" + img[0].outerHTML + "</td>" +
        "<td><span class='editrow'><a class='fa fa-edit' href='javascript: void(0);'>Edit</a></span></td>" +
        "<td class='DocumentId'>" + docId +"</td>" +
        "</tr>";
    if (currentRow) {

        $("table tbody").find($(currentRow)).replaceWith(newRow);
        currentRow = null;
        $('#btnAddClientDocument').val("Add");
    }
    else {
        if (DocumentType == '' && PlaceOfIssue == '') {
            return;
        }
        $("table tbody").append(newRow);
    }
    $('#DocumentType').val("");
    $('#PlaceOfIssue').val("");
    $('#IssueDt').val("");
    $('#ExpiryDt').val("");
    $('#DocumentImage').val("");
});

// Find and remove selected table rows
$(".delete-row").click(function () {
    $("table tbody").find('input[name="record"]').each(function () {
        if ($(this).is(":checked")) {
            $(this).parents("tr").remove();
        }
    });
});

$("#a").click(function () {
    $("table tbody").find('input[name="record"]').each(function () {
        if ($(this).is(":checked")) {
            $(this).parents("tr").remove();
        }
    });
});
$(document).on('click', 'span.editrow', function () {
    docBase64 = '';
    currentRow = $(this).parents('tr');
    $('#DocumentType').val($(this).closest('tr').find('td.DocumentType').text());
    $('#PlaceOfIssue').val($(this).closest('tr').find('td.PlaceOfIssue').text());
    $('#IssueDt').val($(this).closest('tr').find('td.IssueDt').text());
    $('#ExpiryDt').val($(this).closest('tr').find('td.ExpiryDt').text());
    $('#DocumentImage').val($(this).closest('tr').find('td.Photo').text());
    debugger;
    docId = $(this).closest('tr').find('td.DocumentId').text();
    if ($(this).closest('tr').find('td.photo').length > 0)
    docBase64 = $(this).closest('tr').find('td.photo')[0].childNodes[0].src;
    $('#btnAddClientDocument').val("Edit");
    // var RecordStatus = 'A';
});

$('#btnClientEdit').click(function() {
    if ($('#clientForm').valid()) {
        var clientDocuments = [];
        var clientId = new URLSearchParams(location.search).get('clientId');
        var table = $("#clientDocumentTable tbody");
        table.find('tr').each(function(i) {
            var $tds = $(this).find('td');
            clientDocuments.push(
                {
                    ClientId: clientId,
                    DocumentType: $tds.eq(1).text(),
                    PlaceOfIssue: $tds.eq(2).text(),
                    IssueDt: $tds.eq(3).text(),
                    ExpiryDt: $tds.eq(4).text(),
                    DocumentBase64: $tds.eq(5)[0].childNodes[0].src.replace('data:image/*;base64,', ''), //$tds.eq(5).text(),
                    ClientDocumentID: $tds.eq(7).text(),
                    RecordStatus: 'A'
                });

        });
        
        var imageSignSrcBase64 = '';
       // $('#signPhoto').prop('src');
        if ($('#signPhoto').prop('src') != undefined) {
            imageSignSrcBase64 = $('#signPhoto').prop('src').replace('data:image/*;base64,', '');
        } else {
            imageSignSrcBase64 = base64;
        }
        var clientModel = {
            ClientID: clientId != null ? clientId : 0,
            FullName: $('#FullName').val(),
            Address: $('#Address').val(),
            CityID: $('#CityID').val(),
            Phone: $('#Phone').val(),
            Mobile: $('#Mobile').val(),
            Fax: $('#Fax').val(),
            PersonOrCorp: $('#PersonOrCorp').val(),
            GuarantorID: $('#GuarantorID').val(),
            PrivilegeID: $('#PrivilegeID').val(),
            Notes: $('#Notes').val(),
            SignBase64: imageSignSrcBase64,
            DocumentBase64: base64,
            ClientDocuments: clientDocuments
        };
        var update_Client = jqxhr('put', 'api/clientApi/put', clientModel);

        update_Client.done(function(r) {
            signature = null;
            $('#dlg').html(CoreBankingSuccessDialog());
            $("#myModal").modal();
        });

        update_Client.fail(function(r) {
            $('#dlg').html(CoreBankingErrorDialog(r.responseJSON.ExceptionMessage));
            $("#myModal").modal();
        });
    };
});



$("#btnClientCancel").click(function () {
    resetClient();
    resetClientDoc();
    $("#clientDocumentTable tr").remove();
});



$("#btnClientClose").click(function () {
    var url = "/Dashboard/Index";
    window.location.href = url;
});

function resetClientDoc() {
    $('#DocumentType').val('');
    $('#PlaceOfIssue').val('');
    $('#IssueDt').val('');
    $('#ExpiryDt').val('');
    $('#DocumentImage').val('');
}

function resetClient() {
    $('#FullName').val('');
    $('#Address').val('');
    $('#Phone').val('');
    $('#Mobile').val('');
    $('#Fax').val('');
    $('#Notes').val('');
    $('#SignImage').val('');
    $("#GuarantorID").select2("val", "-1");
    $("#PrivilegeID").select2("val", "-1");
    $("#CityID").select2("val", "-1");
    $('#PersonOrCorp').select2("val", "-1");
    $('#signPhoto').attr('src', '');
}

