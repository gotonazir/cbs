﻿$('#btnLogin').click(function () {
    $('#spin').show();
    var key = CryptoJS.enc.Utf8.parse('8080808080808080');
    var iv = CryptoJS.enc.Utf8.parse('8080808080808080');
    
    var encryptedlogin = CryptoJS.AES.encrypt(CryptoJS.enc.Utf8.parse($('#UserName').val()), key,
        {
            keySize: 128 / 8,
            iv: iv,
            mode: CryptoJS.mode.CBC,
            padding: CryptoJS.pad.Pkcs7
        });
    var encryptedpassword = CryptoJS.AES.encrypt(CryptoJS.enc.Utf8.parse($('#Password').val()), key,
        {
            keySize: 128 / 8,
            iv: iv,
            mode: CryptoJS.mode.CBC,
            padding: CryptoJS.pad.Pkcs7
        });
    alert('encrypted Username :' + encryptedlogin);
    alert('encrypted password :' + encryptedpassword);
    $.ajax({
        url: 'http://localhost:61693/token',
        method: 'POST',
        contentType: 'application/x-www-form-urlencoded',
        data: {
            username: $('#UserName').val(),
            password: $('#Password').val(),
            grant_type: 'password'
        },
        success: function (response) {
            sessionStorage.setItem("accessToken", response.access_token);
            window.location.href = '@Url.Content("~/Dashboard/Index")';
        },
        error: function (jqXHR) {
            $('#divErrorText').text(jqXHR.responseText);
            $('#divError').show('fade');
        }
    });
});