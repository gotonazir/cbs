﻿
$(document).ready(function () { 
    onSandooqFormLoad();
});

function onSandooqFormLoad() {
    
    $('#btnSandooqEdit').hide();
    $('#btnSandooqSave').show();

    var get_Accounts = jqxhr('get', 'api/accountapi/AccountNumbers');

    get_Accounts.done(function (response) {
        $("#AccountID").select2({
            placeholder: "Select Account",
            data: response
        });
        //$("#AccountID").select2("val", "-1");
    });
    
    if (location.search !== "") {
        var sandooqId = new URLSearchParams(location.search).get('sandooqId');
        if (sandooqId !== null) {
            console.log('SandooqId' + sandooqId);
            var get_sandooqes = jqxhr('get', 'api/sandooqapi/Get?id=' + sandooqId + '&fromScreen=reg');

            get_sandooqes.done(function (data) {
                console.log(data); // response object
                $('#AccountID').val(data.AccountID);
                $('#TransactionType').val(data.TransactionType);
                $('#Notes').val(data.Notes);                                
                $('#btnSandooqSave').hide();
                $('#btnSandooqEdit').show();
            });

            get_users.fail(function (r) {
                console.log(r); 
            });
        }
    }
}

$('#btnSandooqSave').click(function () {
    if ($('#sandooqForm').valid()) {
        var sandooqModel = {
            AccountID: $('#AccountID').val(),
            TransactionType: $('#TransactionType').val(),
            Notes: $('#Notes').val()            
        };

        var addSandooq = jqxhr('post', 'api/sandooqapi/post', sandooqModel);

        addSandooq.done(function (r) {            
            $('#dlg').html(CoreBankingSuccessDialog());          
            $("#myModal").modal();           
            document.getElementById("sandooqForm").reset();

        });

        addSandooq.fail(function (r) {
            $('#dlg').html(CoreBankingErrorDialog(r.responseJSON.ExceptionMessage));
            $("#myModal").modal();
        });
    }

});

$('#btnSandooqEdit').click(function () {
    if ($('#sandooqForm').valid()) {
        var sandooqId = new URLSearchParams(location.search).get('sandooqId');
        var sandooqModel = {
            SandooqId: sandooqId,
            AccountID: $('#AccountID').val(),
            TransactionType: $('#TransactionType').val(),
            Notes: $('#Notes').val()                     
        };

        var edit_sandooq = jqxhr('put', 'api/sandooqapi/put', sandooqModel);

        edit_sandooq.done(function (r) {
            $('#dlg').html(CoreBankingSuccessDialog());
            $("#myModal").modal();  
        });

        edit_sandooq.fail(function (r) {
            $('#dlg').html(CoreBankingErrorDialog(r.responseJSON.ExceptionMessage));
            $("#myModal").modal();
        });
    }
});

$('#btnSandooqReset').click(function () {
    document.getElementById("sandooqForm").reset();
});
$("#btnSandooqClose").click(function () {
    var url = "/Dashboard/Index";
    window.location.href = url;
});