﻿
$(document).ready(function () { 
    onBranchFormLoad();
});

function onBranchFormLoad() {
    
    $('#btnBranchEdit').hide();
    $('#btnBranchSave').show();
    
    if (location.search != "") {
        var branchId = new URLSearchParams(location.search).get('branchId');
        if (branchId != null) {
            console.log('BranchId' + branchId);
            var get_branches = jqxhr('get', 'api/branchapi/Get?id=' + branchId + '&fromScreen=reg');

            get_branches.done(function (data) {
                console.log(data); // response object
                $('#BranchName').val(data.BranchName);
                $('#Address').val(data.Address);
                $('#Phone').val(data.Phone);
                $('#Fax').val(data.Fax);
                $('#BranchType').val(data.BranchType);                
                $('#btnBranchSave').hide();
                $('#btnBranchEdit').show();
            });

            get_users.fail(function (r) {
                console.log(r); 
            });
        }
    }
}

$('#btnBranchSave').click(function () {
    if ($('#branchForm').valid()) {
        var branchModel = {
            BranchName: $('#BranchName').val(),
            Address: $('#Address').val(),
            Phone: $('#Phone').val(),
            Fax: $('#Fax').val(),
            BranchType: $('#BranchType').val()
        };

        var addBranch = jqxhr('post', 'api/branchapi/post', branchModel);

        addBranch.done(function (r) {
            
            $('#dlg').html(CoreBankingSuccessDialog());          
            $("#myModal").modal();           
            document.getElementById("branchForm").reset();

        });

        addBranch.fail(function (r) {
            $('#dlg').html(CoreBankingErrorDialog(r.responseJSON.ExceptionMessage));
            $("#myModal").modal();
        });
    }

});

$('#btnBranchEdit').click(function () {
    if ($('#branchForm').valid()) {
        var branchId = new URLSearchParams(location.search).get('branchId');
        var branchModel = {
            BranchId: branchId,
            BranchName: $('#BranchName').val(),
            Address: $('#Address').val(),
            Phone: $('#Phone').val(),
            Fax: $('#Fax').val(),
            BranchType: $('#BranchType').val()            
        };

        var edit_branch = jqxhr('put', 'api/branchapi/put', branchModel);

        edit_branch.done(function (r) {
            $('#dlg').html(CoreBankingSuccessDialog());
            $("#myModal").modal();  
        });

        edit_branch.fail(function (r) {
            $('#dlg').html(CoreBankingErrorDialog(r.responseJSON.ExceptionMessage));
            $("#myModal").modal();
        });
    }
});

$('#btnBranchReset').click(function () {
    document.getElementById("branchForm").reset();
});
$("#btnBranchClose").click(function () {
    var url = "/Dashboard/Index";
    window.location.href = url;
});