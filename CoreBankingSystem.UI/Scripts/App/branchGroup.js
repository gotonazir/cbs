﻿
$(document).ready(function () {
    onBranchGroupFormLoad();
});

function onBranchGroupFormLoad() {

    $('#btnBranchGroupEdit').hide();
    $('#btnBranchGroupSave').show();

    var get_BranchNames = jqxhr('get', 'api/branchapi/BranchNames');

    get_BranchNames.done(function (response) {
        $("#BranchID").select2({
            placeholder: "Select Branch",
            data: response
        });
        //$("#BranchID").select2("val", "-1");
    });

    var get_GroupNames = jqxhr('get', 'api/groupapi/GroupNames');

    get_GroupNames.done(function (response) {
        $("#GroupID").select2({
            placeholder: "Select Group",
            data: response
        });
        //$("#GroupID").select2("val", "-1");
    });

    if (location.search !== "") {
        var branchGroupId = new URLSearchParams(location.search).get('branchGroupId');
        if (branchGroupId !== null) {
            console.log('BranchGroupId' + branchGroupId);
            var get_branchGroups = jqxhr('get', 'api/branchgroupapi/Get?id=' + branchGroupId + '&fromScreen=reg');

            get_branchGroups.done(function (data) {
                console.log(data); // response object
                $('#BranchGroupID').val(data.BranchGroupID);
                $('#BranchID').val(data.BranchID);
                $('#GroupID').val(data.GroupID);
                $('#btnBranchGroupSave').hide();
                $('#btnBranchGroupEdit').show();
            });

            get_branchGroups.fail(function (r) {
                console.log(r);
            });
        }
    }
}

$('#btnBranchGroupSave').click(function () {
    if ($('#branchGroupForm').valid()) {
        var branchGroupModel = {
            BranchID: $('#BranchID').val(),
            GroupID: $('#GroupID').val()
        };

        var addBranchGroup = jqxhr('post', 'api/branchgroupapi/post', branchGroupModel);

        addBranchGroup.done(function (r) {

            $('#dlg').html(CoreBankingSuccessDialog());
            $("#myModal").modal();
            document.getElementById("branchGroupForm").reset();

        });

        addBranchGroup.fail(function (r) {
            $('#dlg').html(CoreBankingErrorDialog(r.responseJSON.ExceptionMessage));
            $("#myModal").modal();
        });
    }

});

$('#btnBranchGroupEdit').click(function () {
    if ($('#branchGroupForm').valid()) {
        var BranchGroupID = new URLSearchParams(location.search).get('branchGroupId');
        var branchGroupModel = {
            BranchGroupID: BranchGroupID,
            BranchID: $('#BranchID').val(),
            GrouID: $('#GrouID').val()
        };

        var edit_branchGroup = jqxhr('put', 'api/branchgroupapi/put', branchGroupModel);

        edit_branchGroup.done(function (r) {
            $('#dlg').html(CoreBankingSuccessDialog());
            $("#myModal").modal();  
        });

        edit_branchGroup.fail(function (r) {
            $('#dlg').html(CoreBankingErrorDialog(r.responseJSON.ExceptionMessage));
            $("#myModal").modal();
        });
    }
});

$('#btnBranchGroupReset').click(function () {
    document.getElementById("branchGroupForm").reset();
});
$("#btnBranchGroupClose").click(function () {
    var url = "/Dashboard/Index";
    window.location.href = url;
});