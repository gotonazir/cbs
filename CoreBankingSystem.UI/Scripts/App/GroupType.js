﻿
$(document).ready(function () { 
    onGroupTypeFormLoad();
});

function onGroupTypeFormLoad() {
    
    $('#btnGroupTypeEdit').hide();
    $('#btnGroupTypeSave').show();
    
    if (location.search != "") {
        var groupTypeId = new URLSearchParams(location.search).get('groupTypeId');
        if (groupTypeId != null) {
            console.log('GroupTypeId' + groupTypeId);
            var get_groupTypees = jqxhr('get', 'api/groupTypeapi/Get?id=' + groupTypeId + '&fromScreen=reg');

            get_groupTypees.done(function (data) {
                console.log(data); // response object
                $('#GroupTypeName').val(data.GroupTypeName);                                
                $('#btnGroupTypeSave').hide();
                $('#btnGroupTypeEdit').show();
            });

            get_users.fail(function (r) {
                console.log(r); 
            });
        }
    }
}

$('#btnGroupTypeSave').click(function () {
    if ($('#groupTypeForm').valid()) {
        var groupTypeModel = {
            GroupTypeName: $('#GroupTypeName').val(),            
        };

        var addGroupType = jqxhr('post', 'api/groupTypeapi/post', groupTypeModel);

        addGroupType.done(function (r) {
            
            $('#dlg').html(CoreBankingSuccessDialog());          
            $("#myModal").modal();           
            document.getElementById("groupTypeForm").reset();

        });

        addGroupType.fail(function (r) {
            $('#dlg').html(CoreBankingErrorDialog(r.responseJSON.ExceptionMessage));
            $("#myModal").modal();
        });
    }

});

$('#btnGroupTypeEdit').click(function () {
    if ($('#groupTypeForm').valid()) {
        var groupTypeId = new URLSearchParams(location.search).get('groupTypeId');
        var groupTypeModel = {
            GroupTypeId: groupTypeId,
            GroupTypeName: $('#GroupTypeName').val()          
        };

        var edit_groupType = jqxhr('put', 'api/groupTypeapi/put', groupTypeModel);

        edit_groupType.done(function (r) {
            $('#dlg').html(CoreBankingSuccessDialog());
            $("#myModal").modal();  
        });

        edit_groupType.fail(function (r) {
            $('#dlg').html(CoreBankingErrorDialog(r.responseJSON.ExceptionMessage));
            $("#myModal").modal();
        });
    }
});

$('#btnGroupTypeReset').click(function () {
    document.getElementById("groupTypeForm").reset();
});
$("#btnGroupTypeClose").click(function () {
    var url = "/Dashboard/Index";
    window.location.href = url;
});