﻿$(document).ready(function() {
    if (sessionStorage['accessToken'] == undefined) {
        window.location.href = $('#hdnLogout').val();
    }
})

$('#btnLogout').click(function () {

    sessionStorage.removeItem('accessToken');
    window.location.href = $('#hdnLogout').val();
})