﻿$(document).ready(function () {
    $.fn.select2.defaults.set("theme", "bootstrap");
    $.fn.select2.defaults.set("width", "100%");
    CoreBankingSuccessDialog();
});


function baseAddressEndPoint() {
    return 'http://localhost:61693/';
}

function jqxhr(method, endpoint, postdata) {
    var base_url = baseAddressEndPoint();
    return $.ajax({ // return the promise that `$.ajax` returns
        type: method,
        beforeSend: function (request) {
            // example with API authentication via request header
            //request.setRequestHeader('X-AUTHORIZATION-KEY', '1234-1234-1234-1234');
            request.setRequestHeader('Authorization', 'bearer' + ' ' + sessionStorage["accessToken"]);
        },
        url: base_url + endpoint,
        data: postdata,
        dataType: "json"
    });
}

function Get(url) {
    alert('get');
    return $.ajax({
        url: 'http://localhost:61693/api/values',
        method: 'GET',
        contentType: 'application/json'
    });
}

// Invoke Get method
//Get(url, onSuccess);

Post = function (model, url, successCallback) {
    $.ajax({
        url: url,
        method: 'POST',
        contentType: 'application/json',
        data: model,
        // async: false,
        // cache: false,
        success: successCallback,
        error: function (jqXHR) {

        }
    });
};

Put = function (model, url, successCallback) {
    $.ajax({
        url: url,
        method: 'PUT',
        contentType: 'application/json',
        data: model,
        //async: false,
        success: successCallback,

        error: function (jqXHR) {

        }
    });
};

Delete = function (id, url, successCallback) {
    $.ajax({
        url: url,
        method: 'Delete',
        contentType: 'application/json',
        data: id,
        async: false,
        success: successCallback,

        error: function (jqXHR) {

        }
    });
};


//function CoreBankingSuccessDialog() {
//    $('<div />').html('Saved Successfully').dialog();
//}


function CoreBankingSuccessDialog() {
    return '<div class="modal" id="myModal">\
        <div class="modal-dialog"> \
        <div class="modal-content"> \
        <div class="modal-header"> \
        <h4 class="modal-title">Notification</h4> \
        <button type="button" class="close" data-dismiss="modal">&times;</button> \
        </div> \
        <div class="modal-body"> \
        Saved Successfully!!! \
        </div> \
        <div class="modal-footer"> \
        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button> \
        </div> \
        </div> \
        </div> \
        </div>';
};

function CoreBankingErrorDialog(message) {
    return '<div class="modal" id="myModal">\
        <div class="modal-dialog"> \
        <div class="modal-content"> \
        <div class="modal-header"> \
        <h4 class="modal-title text-danger">Warning</h4> \
        <button type="button" class="close" data-dismiss="modal">&times;</button> \
        </div> \
        <div class="modal-body"> \
        '+ message + ' \
        </div> \
        <div class="modal-footer"> \
        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button> \
        </div> \
        </div> \
        </div> \
        </div>';
};


