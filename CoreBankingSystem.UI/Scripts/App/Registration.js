﻿var userRoles =
    [
        {
            "id": 1,
            "text": "Admin"
        },
        {
            "id": 2,
            "text": "Branch Manager"
        },
        {
            "id": 3,
            "text": "Manager"
        },
        {
            "id": 4,
            "text": "User"
        }
    ];
var dialog = '';
$(document).ready(function () {
  //  $('div[onload]').trigger('onload');
    $("#ChangePasswordDt").datepicker();
    onRegistrationFormLoad1();
});

function onRegistrationFormLoad1() {

    $('#btnUserEdit').hide();
    $('#btnRegister').show();

    $("#UserRole").select2({
        placeholder: "select Role",
        //minimumResultsForSearch: -1,
        data: userRoles
    });

    var get_users = jqxhr('get', 'api/branchapi/BranchNames');

    get_users.done(function (response) {
        $("#DefaultBranchID").select2({
            placeholder: "Select Branch",
            data: response
        });
    });

    if (location.search != "") {
        var userId = new URLSearchParams(location.search).get('userId');
        if (userId != null) {
            console.log('UserId' + userId);
            var get_users = jqxhr('get', 'api/userApi/Get?id=' + userId + '&fromScreen=reg');

            get_users.done(function (data) {
                console.log(data); // response object
                $('#UserNo').val(data.UserNo);
                $('#UserName').val(data.UserName);
                $('#PassWord').val(data.PassWord);
                // $("#UserRole").select2('data')[0].text = data.UserRole;
                var roles = $.grep(userRoles,
                    function(element, index) {
                       return element.text == data.UserRole;
                       
                    });
                if (roles.length > 0) {
                    $("#UserRole").select2("val", roles[0].id.toString());
                }
               // $('#DefaultBranchID').val(data.DefaultBranchID);
                if (data.DefaultBranchID != '' && data.DefaultBranchID != null) {
                    $("#DefaultBranchID").select2("val", data.DefaultBranchID.toString());
                }
                $('#BlockedStatus').val(data.BlockedStatus);
                $('#ChangePasswordDays').val(data.ChangePasswordDays);
                $('#ChangePasswordDt').val(new Date(data.ChangePasswordDt).toLocaleDateString());
                $('#Title').val(data.Title);
                $('#Notes').val(data.Notes);
                $('#Salary').val(data.Salary);
                $('#hdnUserId').val(data.UserID);
                $('#btnRegister').hide();
                $('#btnUserEdit').show();
            });

            get_users.fail(function (r) {
                console.log(r); // response object
            });
        }

    }
}

$('#btnRegister').click(function () {
    if ($('#registrationForm').valid()) {
        var userModel = {
            UserNo: $('#UserNo').val(),
            UserName: $('#UserName').val(),
            PassWord: $('#PassWord').val(),
            UserRole: $("#UserRole").select2('data')[0].text,
            DefaultBranchID: $('#DefaultBranchID').val(),
            BlockedStatus: $('#BlockedStatus').val(),
            ChangePasswordDays: $('#ChangePasswordDays').val(),
            ChangePasswordDt: $('#ChangePasswordDt').val(),
            Title: $('#Title').val(),
            Salary: $('#Salary').val(),
            Notes: $('#Notes').val()
        };

        var add_user = jqxhr('post', 'api/userapi/post', userModel);

        add_user.done(function (r) {
            // console.log('sucess');
            $('#dlg').html(CoreBankingSuccessDialog());
            $("#myModal").modal();
        });

        add_user.fail(function (r) {
            $('#dlg').html(CoreBankingErrorDialog(r.responseJSON.ExceptionMessage));
            $("#myModal").modal();
        });
    }

});

$('#btnUserEdit').click(function () {

    if ($('#registrationForm').valid()) {
        var userId = new URLSearchParams(location.search).get('userId');
        var userModel = {
            UserID: userId,
            UserNo: $('#UserNo').val(),
            UserName: $('#UserName').val(),
            PassWord: $('#PassWord').val(),
            UserRole: $("#UserRole").select2('data')[0].text,
            DefaultBranchID: $('#DefaultBranchID').val(),
            BlockedStatus: $('#BlockedStatus').val(),
            ChangePasswordDays: $('#ChangePasswordDays').val(),
            ChangePasswordDt: $('#ChangePasswordDt').val(),
            Title: $('#Title').val(),
            Salary: $('#Salary').val(),
            Notes: $('#Notes').val()
        };

        var edit_user = jqxhr('put', 'api/userapi/put', userModel);

        edit_user.done(function (r) {
            $('#dlg').html(CoreBankingSuccessDialog());
            $("#myModal").modal();
        });

        edit_user.fail(function (r) {
            $('#dlg').html(CoreBankingErrorDialog(r.responseJSON.ExceptionMessage));
            $("#myModal").modal();
        });
    }

});

