﻿using System.Web;
using System.Web.Mvc;

namespace CoreBankingSystem.UI
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new CustomActionFilter());
            filters.Add(new HandleErrorAttribute());
        }
    }
}
