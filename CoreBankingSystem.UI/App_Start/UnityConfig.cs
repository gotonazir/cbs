using DataAccess.Repository;
using DataAccess.IRepository;
using System;
using BusinessLogic.IServices;
using Unity;
using System.Web.Http;
using DataAccess;
using Unity.AspNet.WebApi;
using BusinessLogic.Services;

namespace CoreBankingSystem.UI
{
    /// <summary>
    /// Specifies the Unity configuration for the main container.
    /// </summary>
    public static class UnityConfig
    {
        #region Unity Container
        private static Lazy<IUnityContainer> container =
          new Lazy<IUnityContainer>(() =>
          {
              var container = new UnityContainer();
              RegisterTypes(container);
              return container;
          });

        /// <summary>
        /// Configured Unity Container.
        /// </summary>
        public static IUnityContainer Container => container.Value;
        #endregion

        /// <summary>
        /// Registers the type mappings with the Unity container.
        /// </summary>
        /// <param name="container">The unity container to configure.</param>
        /// <remarks>
        /// There is no need to register concrete types such as controllers or
        /// API controllers (unless you want to change the defaults), as Unity
        /// allows resolving a concrete type even if it was not previously
        /// registered.
        /// </remarks>
        public static void RegisterTypes(IUnityContainer container)
        {
            //var container =   new UnityContainer()
            // NOTE: To load from web.config uncomment the line below.
            // Make sure to add a Unity.Configuration to the using statements.
            // container.LoadConfiguration();

            // TODO: Register your type's mappings here.
            container.RegisterType(typeof(IGenericRepository<>), typeof(GenericRepository<>));
            container.RegisterType<IUserRepository, UserRepository>();
            container.RegisterType<IUserService, UserService>();
            container.RegisterType<IClientRepository, ClientRepository>();
            container.RegisterType<IClientService, ClientService>();
            container.RegisterType<IBranchService, BranchService>();
            container.RegisterType<IBranchRepository, BranchRepository>();
            container.RegisterType<IAccountRepository, AccountRepository>();
            container.RegisterType<IAccountService, AccountService>();

            container.RegisterType<IClientDocumentRepository, ClientDocumentRepository>();
            container.RegisterType<IClientDocumentService, ClientDocumentService>();
            container.RegisterType<IAccountCurrencyRepository, AccountCurrencyRepository>();
            container.RegisterType<IAccountTreeRepository, AccountTreeRepository>();
            container.RegisterType<IAccountTypeRepository, AccountTypeRepository>();
            container.RegisterType<IAccountSub1Repository, AccountSub1Repository>();
            container.RegisterType<IAccountSub2Repository, AccountSub2Repository>();
            container.RegisterType<IAccountSub2Service, AccountSub2Service>();
            container.RegisterType<IAccountSub1Service, AccountSub1Service>();
            container.RegisterType<IAccountTypeService, AccountTypeService>(); 
            container.RegisterType<IAccountTreeService, AccountTreeService>(); 
            container.RegisterType<ICityCountryService, CityCountryService>();
            container.RegisterType<ICityCountryRepository, CityCountryRepository>();
            container.RegisterType<IAccountTypeTreeRepository, AccountTypeTreeRepository>();
            container.RegisterType<IAccountTypeTreeService, AccountTypeTreeService>();

            container.RegisterType<ICurrencyService, CurrencyService>();
            container.RegisterType<ICurrencyRepository, CurrencyRepository>();

            container.RegisterType<IAgentService, AgentService>();
            container.RegisterType<IAgentRepository, AgentRepository>();

            container.RegisterType<IAgentGroupService, AgentGroupService>();
            container.RegisterType<IAgentGroupRepository, AgentGroupRepository>();

            container.RegisterType<IAgentUserService, AgentUserService>();
            container.RegisterType<IAgentUserRepository, AgentUserRepository>();

            container.RegisterType<IBranchGroupService, BranchGroupService>();
            container.RegisterType<IBranchGroupRepository, BranchGroupRepository>();

            container.RegisterType<IGroupService, GroupService>();
            container.RegisterType<IGroupRepository, GroupRepository>();

            container.RegisterType<IGroupTypeService, GroupTypeService>();
            container.RegisterType<IGroupTypeRepository, GroupTypeRepository>();

            container.RegisterType<ISandooqService, SandooqService>();
            container.RegisterType<ISandooqRepository, SandooqRepository>();

            container.RegisterType<IAgentAreaService, AgentAreaService>();
            container.RegisterType<IAgentAreaRepository, AgentAreaRepository>();

            GlobalConfiguration.Configuration.DependencyResolver = new UnityDependencyResolver(container);
        }
    }
}