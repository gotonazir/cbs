﻿using System.Web;
using System.Web.Optimization;

namespace CoreBankingSystem.UI
{
    public class BundleConfig
    {
        // For more information on bundling, visit https://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"
                ));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at https://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));
            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                "~/Scripts/jquery-ui.min.js",
                //"~/Scripts/jstree.min.js",
                "~/Scripts/jquery.dataTables.min.js",
                "~/Scripts/dataTables.bootstrap4.min.js",
            "~/Scripts/bootstrap.js", 
            "~/Scripts/select2.min.js",
            "~/Scripts/aes.js",
            "~/Scripts/jqListbox.plugin-1.3.js",
            "~/Scripts/respond.js"));

            bundles.Add(new ScriptBundle("~/bundles/app").Include(
                "~/Scripts/App/common.js",
                "~/Scripts/App/login.js",
                "~/Scripts/App/dashboard.js",
                "~/Scripts/App/Registration.js",
                "~/Scripts/App/userProfile.js"
               ));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/jquery-ui.min.css",
                      "~/Content/bootstrap.css",
                      "~/Content/jsTree.css",
                      "~/Content/select2.min.css",
                      "~/Content/dataTables.bootstrap4.min.css",
            "~/Content/select2-bootstrap.min.css",
                      "~/Content/font-awesome.min.css",
                      "~/Content/site.css"));
           // BundleTable.EnableOptimizations = true;
        }
    }
}
