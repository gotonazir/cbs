﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CoreBankingSystem.UI.Controllers
{
    [Authorize]
    public class DashboardController : BaseMVCController
    {
        // GET: Dashboard
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult LeftPanel()
        {
            return View();
        }

        public PartialViewResult UserProfile()
        {
            return PartialView();
        }

        public PartialViewResult LogOut()
        {
            return PartialView();
        }
    }
}