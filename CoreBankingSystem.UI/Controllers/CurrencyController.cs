﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CoreBankingSystem.UI.Controllers
{
    public class CurrencyController : BaseMVCController
    {
        public PartialViewResult CurrencyIndex()
        {
            return PartialView();
        }
        public PartialViewResult CurrencyDetails()
        {
            return PartialView();
        }
    }
}