﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CoreBankingSystem.UI.Controllers
{
    public class POCController : Controller
    {
        // GET: POC
        public ActionResult TestTree()
        {
            List<SiteMenu> all = new List<SiteMenu>();
            all.Add(new SiteMenu { MenuID = 1, MenuName = "Node1", NavURL = "", ParentMenuID = 0 });
            all.Add(new SiteMenu { MenuID = 2, MenuName = "Node2", NavURL = "", ParentMenuID = 0 });
            all.Add(new SiteMenu { MenuID = 3, MenuName = "Node3", NavURL = "", ParentMenuID = 0 });
            all.Add(new SiteMenu { MenuID = 4, MenuName = "Child Node1", NavURL = "", ParentMenuID = 1 });
            all.Add(new SiteMenu { MenuID = 5, MenuName = "Sub child Node 1", NavURL = "", ParentMenuID = 4 });
            all.Add(new SiteMenu { MenuID = 6, MenuName = "child node2", NavURL = "", ParentMenuID = 2 });
            return View(all);
        }

        public ActionResult Simple()
        {


            return View();
        }

        public ActionResult TestTreeView()
        {
            return View();
        }
    }

    public class SiteMenu
    {
        public int MenuID { get; set; }
        public string MenuName { get; set; }
        public string NavURL { get; set; }
        public int ParentMenuID { get; set; }
    }
}