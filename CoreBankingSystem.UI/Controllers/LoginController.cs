﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CoreBankingSystem.UI.Controllers
{
    public class LoginController : BaseMVCController
    {
        [AllowAnonymous]
        // GET: Login
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Registration()
        {
            return View();
        }

        public PartialViewResult UserDetails()
        {
            return PartialView();
        }
        [AllowAnonymous]
        public ActionResult Sample()
        {
            return View();
        }


    }
}
