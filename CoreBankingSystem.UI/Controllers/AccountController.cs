﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CoreBankingSystem.UI.Controllers
{
    public class AccountController : BaseMVCController
    {
        // GET: Account
        public ActionResult AccountIndex()
        {
            return View();
        }

        public ActionResult AccountDetails()
        {
            return View();
        }
    }
}