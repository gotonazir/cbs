﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CoreBankingSystem.UI.Controllers
{
    public class MasterController : BaseMVCController
    {
        // GET: Master
        public PartialViewResult BranchIndex()
        {
            return PartialView();
        }
        public PartialViewResult BranchDetails()
        {
            return PartialView();
        }

        public PartialViewResult AgentIndex()
        {
            return PartialView();
        }
        public PartialViewResult AgentDetails()
        {
            return PartialView();
        }

        public PartialViewResult AgentGroupIndex()
        {
            return PartialView();
        }
        public PartialViewResult AgentGroupDetails()
        {
            return PartialView();
        }
        public PartialViewResult AgentUserIndex()
        {
            return PartialView();
        }
        public PartialViewResult AgentUserDetails()
        {
            return PartialView();
        }

        public PartialViewResult BranchGroupIndex()
        {
            return PartialView();
        }
        public PartialViewResult BranchGroupDetails()
        {
            return PartialView();
        }

        public PartialViewResult AgentAreaIndex()
        {
            return PartialView();
        }

        public PartialViewResult AgentAreaDetails()
        {
            return PartialView();
        }

        public PartialViewResult GroupTypeIndex()
        {
            return PartialView();
        }

        public PartialViewResult GroupTypeDetails()
        {
            return PartialView();
        }
        public PartialViewResult GroupIndex()
        {
            return PartialView();
        }

        public PartialViewResult GroupDetails()
        {
            return PartialView();
        }

        public PartialViewResult SandooqIndex()
        {
            return PartialView();
        }

        public PartialViewResult SandooqDetails()
        {
            return PartialView();
        }

    }
        
}