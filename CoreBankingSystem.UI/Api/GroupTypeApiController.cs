﻿using BusinessLogic.IServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using DomainModels;

namespace CoreBankingSystem.UI.Api
{
    public class GroupTypeApiController : ApiController
    {
        private readonly IGroupTypeService _groupTypeService = null;
        public GroupTypeApiController(IGroupTypeService groupTypeService)
        {
            _groupTypeService = groupTypeService;
        }
        // GET api/<controller>
        public IEnumerable<GroupTypeModel> Get()
        {
          return  _groupTypeService.GetGroupTypes();
        }

        // GET api/<controller>/5
        public GroupTypeModel Get(int id)
        {
            return _groupTypeService.GetGroupTypeById(id);
        }

        public GroupTypeModel Get(int id, string fromScreen)
        {
            return _groupTypeService.GetGroupTypeById(id);
        }

        // POST api/<controller>
        public void Post([FromBody]GroupTypeModel GroupTypeModel)
        {
            _groupTypeService.SaveGroupType(GroupTypeModel);
        }

        // PUT api/<controller>/5
        public void Put( [FromBody]GroupTypeModel GroupTypeModel)
        {
            try
            {
                GroupTypeModel.RecordStatus = "A";
                int a = _groupTypeService.UpdateGroupType(GroupTypeModel);

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        // DELETE api/<controller>/5
        public void Delete(long id)
        {
            _groupTypeService.Delete(id);
        }

        [Route("api/GroupTypeapi/GroupTypeNames")]
        [ActionName("GroupTypeNames")]
        public IEnumerable<ChoiceOption> GetGroupTypeNames()
        {
            return _groupTypeService.GetGroupTypeNames();
        }
    }
}