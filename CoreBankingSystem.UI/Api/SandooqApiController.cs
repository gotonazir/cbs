﻿using BusinessLogic.IServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using DomainModels;

namespace CoreBankingSystem.UI.Api
{
    public class SandooqApiController : ApiController
    {
        private readonly ISandooqService _sandooqService = null;
        public SandooqApiController(ISandooqService sandooqService)
        {
            _sandooqService = sandooqService;
        }
        // GET api/<controller>
        public IEnumerable<SandooqModel> Get()
        {
          return  _sandooqService.GetSandooqs();
        }

        // GET api/<controller>/5
        public SandooqModel Get(int id)
        {
            return _sandooqService.GetSandooqsById(id);
        }

        public SandooqModel Get(int id, string fromScreen)
        {
            return _sandooqService.GetSandooqsById(id);
        }

        // POST api/<controller>
        public void Post([FromBody]SandooqModel sandooqModel)
        {
            _sandooqService.SaveSandooq(sandooqModel);
        }

        // PUT api/<controller>/5
        public void Put( [FromBody]SandooqModel sandooqModel)
        {
            try
            {
                sandooqModel.RecordStatus = "A";
                int a = _sandooqService.UpdateSandooq(sandooqModel);

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        // DELETE api/<controller>/5
        public void Delete(long id)
        {
            _sandooqService.Delete(id);
        }       
    }
}