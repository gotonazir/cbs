﻿using BusinessLogic.IServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using DomainModels;

namespace CoreBankingSystem.UI.Api
{
    public class AgentAreaApiController : ApiController
    {
        private readonly IAgentAreaService _agentAreaService = null;
        public AgentAreaApiController(IAgentAreaService agentAreaService)
        {
            _agentAreaService = agentAreaService;
        }
        // GET api/<controller>
        public IEnumerable<AgentAreaModel> Get()
        {
          return  _agentAreaService.GetAgentAreas();
        }

        // GET api/<controller>/5
        public AgentAreaModel Get(int id)
        {
            return _agentAreaService.GetAgentAreaById(id);
        }

        public AgentAreaModel Get(int id, string fromScreen)
        {
            return _agentAreaService.GetAgentAreaById(id);
        }

        // POST api/<controller>
        public void Post([FromBody]AgentAreaModel AgentAreaModel)
        {
            _agentAreaService.SaveAgentArea(AgentAreaModel);
        }

        // PUT api/<controller>/5
        public void Put( [FromBody]AgentAreaModel AgentAreaModel)
        {
            try
            {
                int a = _agentAreaService.UpdateAgentArea(AgentAreaModel);

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        // DELETE api/<controller>/5
        public void Delete(long id)
        {
            _agentAreaService.Delete(id);
        }
        
    }
}