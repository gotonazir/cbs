﻿using BusinessLogic.IServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using DomainModels;

namespace CoreBankingSystem.UI.Api
{
    public class BranchApiController : ApiController
    {
        private readonly IBranchService _branchService = null;
        public BranchApiController(IBranchService branchService)
        {
            _branchService = branchService;
        }
        // GET api/<controller>
        public IEnumerable<BranchModel> Get()
        {
          return  _branchService.GetBranches();
        }

        // GET api/<controller>/5
        public BranchModel Get(int id)
        {
            return _branchService.GetBranchesById(id);
        }

        public BranchModel Get(int id, string fromScreen)
        {
            return _branchService.GetBranchesById(id);
        }

        // POST api/<controller>
        public void Post([FromBody]BranchModel branchModel)
        {
            _branchService.SaveBranch(branchModel);
        }

        // PUT api/<controller>/5
        public void Put( [FromBody]BranchModel branchModel)
        {
            try
            {
                branchModel.RecordStatus = "A";
                int a = _branchService.UpdateBranch(branchModel);

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        // DELETE api/<controller>/5
        public void Delete(long id)
        {
            _branchService.Delete(id);
        }
        [Route("api/branchapi/BranchNames")]
        [ActionName("BranchNames")]
        public IEnumerable<ChoiceOption> GetBranchNames()
        {
            return _branchService.GetBranchNames();
        }
    }
}