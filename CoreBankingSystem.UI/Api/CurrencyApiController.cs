﻿using BusinessLogic.IServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using DomainModels;

namespace CoreBankingSystem.UI.Api
{
    public class CurrencyApiController : BaseController
    {
                          
        private readonly ICurrencyService _currencyService = null;
        public CurrencyApiController(ICurrencyService currencyService)
        {
            _currencyService = currencyService;
        }
        // GET api/<controller>
        public IEnumerable<CurrencyModel> Get()
        {
          return  _currencyService.GetCurrencies();
        }

        // GET api/<controller>/5
        public CurrencyModel Get(int id)
        {
            return _currencyService.GetCurrenciesById(id);
        }

        public CurrencyModel Get(int id, string fromScreen)
        {
            return _currencyService.GetCurrenciesById(id);
        }

        // POST api/<controller>
        public void Post([FromBody]CurrencyModel currencyModel)
        {
            _currencyService.SaveCurrency(currencyModel);
        }

        // PUT api/<controller>/5
        public void Put( [FromBody]CurrencyModel currencyModel)
        {
            try
            {
                int a = _currencyService.UpdateCurrency(currencyModel);

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        // DELETE api/<controller>/5
        public void Delete(long id)
        {
            _currencyService.Delete(id);
        }
        [Route("api/currencyapi/CurrencyNames")]
        [ActionName("CurrencyNames")]
        public IEnumerable<ChoiceOption> GetCurrencyNames()
        {
            return _currencyService.GetCurrencyNames();
        }
    }
}