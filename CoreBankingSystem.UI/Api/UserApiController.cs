﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using BusinessLogic.IServices;
using DomainModels;

namespace CoreBankingSystem.UI.Api
{
   // [Authorize]
    public class UserApiController : BaseController
    {
        private readonly IUserService _userService = null;
        public UserApiController(IUserService userService)
        {
            _userService = userService;
        }
        // GET api/<controller>
        public IEnumerable<UserModel> Get()
        {
            return _userService.GetUsers();
        }

        // GET api/<controller>/5
        public UserModel Get(int id)
        {
            string loggedInuserid = UserIdentity;
            return _userService.GetUsersById(int.Parse(loggedInuserid));
        }

        public UserModel Get(int id, string fromScreen)
        {
            return _userService.GetUsersById(id);
        }

        // POST api/<controller>
        public void Post([FromBody]UserModel userModel)
        {
            try
            {
                //UserModel model = new UserModel();
                //model.UserID = 2;
                //model.UserNo = 2;
                //model.UserName = "abdul Rehman";
                //model.PassWord = "abdul";
                //model.DefaultBranchID = 2;
                //model.BlockedStatus = "A";
                //model.ChangePasswordDays = 20;
                //model.ChangePasswordDt = DateTime.Now.AddDays(-2);
                //model.Title = "Title1";
                //model.Salary = 2000;
                //model.RecordStatus = "A";
                userModel.RecordStatus = "A";
                int a = _userService.Save(userModel);

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        // PUT api/<controller>/5
        public void Put([FromBody]UserModel userModel)
        {
            try
            {
                userModel.RecordStatus = "A";
                int a = _userService.Update(userModel);

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        // DELETE api/<controller>/5
        public void Delete(int id)
        {
            int a = _userService.Delete(id);
        }
    }
}