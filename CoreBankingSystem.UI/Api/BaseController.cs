﻿using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using CoreBankingSystem.UI.Extensions;
using DomainModels;
using Microsoft.Ajax.Utilities;

namespace CoreBankingSystem.UI.Api
{
   // [Authorize]
    public class BaseController : ApiController
    {
        public string UserIdentity
        {
            get
            {
                var usr = HttpContext.Current.User.Identity.GetUserId();
                var usr1 = HttpContext.Current.User.Identity.GetUserById();
                var user = User.Identity.GetUserId();
                return usr1;//user.Email
            }
        }

        /// <summary>
        /// Fetches the user ip.
        /// var hostname = HttpContext.Current.Request.UserHostAddress;
        /// IPAddress ipAddress = IPAddress.Parse(hostname);
        /// IPHostEntry ipHostEntry = Dns.GetHostEntry(ipAddress);
        /// </summary>
        /// <returns></returns>
        protected string Fetch_UserIP()
        {

           
            string VisitorsIPAddress = string.Empty;
            try
            {
                if (HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"] != null)
                {
                    VisitorsIPAddress = HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"].ToString();
                }
                else if (HttpContext.Current.Request.UserHostAddress.Length != 0)
                {
                    VisitorsIPAddress = HttpContext.Current.Request.UserHostAddress;
                }
            }
            catch (Exception ex)
            {

                //Handle Exceptions  
            }
            return VisitorsIPAddress;
        }

        public string GetIPAddress()
        {
            string ipaddress;

            ipaddress = HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"]; //Request.ServerVariables["HTTP_X_FORWARDED_FOR"];

            //if (ipaddress == "" || ipaddress == null)

            //    ipaddress = Request.ServerVariables["REMOTE_ADDR"];

            return ipaddress;
        }

    }

    public static class HttpRequestMessageExtensions
    {
        private const string HttpContext = "MS_HttpContext";
        private const string RemoteEndpointMessage = "System.ServiceModel.Channels.RemoteEndpointMessageProperty";

        /// <summary>
        /// Gets the client ip address.
        /// invoke var ipAddress = request.GetClientIpAddress();
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        public static string GetClientIpAddress(this HttpRequestMessage request)
        {
            if (request.Properties.ContainsKey(HttpContext))
            {
                dynamic ctx = request.Properties[HttpContext];
                if (ctx != null)
                {
                    return ctx.Request.UserHostAddress;
                }
            }

            if (request.Properties.ContainsKey(RemoteEndpointMessage))
            {
                dynamic remoteEndpoint = request.Properties[RemoteEndpointMessage];
                if (remoteEndpoint != null)
                {
                    return remoteEndpoint.Address;
                }
            }

            return null;
        }
    }


    //public class SearchRequest
    //{
    //    public int Draw { get; set; }
    //}

    //public abstract class SearchDetail
    //{
    //}

    ////public class CustomerSearchDetail : SearchDetail
    ////{
    ////    public string CompanyName { get; set; }
    ////    public string Address { get; set; }
    ////    public string Postcode { get; set; }
    ////    public string Telephone { get; set; }
    ////}

    //public abstract class SearchResponse<T> where T : SearchDetail
    //{
    //    public int Draw { get; set; }

    //    public int RecordsTotal { get; set; }

    //    public int RecordsFiltered { get; set; }

    //    public IList<T> Data { get; set; }
    //}

    //public class ModelSearchResponse : SearchResponse<CustomerSearchDetail>
    //{
    //}

    //public abstract class SearchController : ApiController
    //{
    //    protected static IList<TDetail> PageResults<TDetail>
    //        (IEnumerable<TDetail> results, UserModel request) where TDetail : UserModel
    //    {
    //        var skip = request.Start;
    //        var pageSize = request.Length;
    //        var orderedResults = OrderResults(results, request);
    //        return pageSize > 0 ? orderedResults.Skip(skip).Take(pageSize).ToList() :
    //            orderedResults.ToList();
    //    }

    //    private static IEnumerable<TDetail> OrderResults<TDetail>
    //        (IEnumerable<TDetail> results, SearchRequest request) where TDetail : SearchDetail
    //    {
    //        if (request.Order == null) return results;
    //        var columnIndex = request.Order[0].Column;
    //        var sortDirection = request.Order[0].Dir;
    //        var columnName = request.Columns[columnIndex].Data.AsPropertyName();
    //        var prop = typeof(TDetail).GetProperty(columnName);
    //        return sortDirection == "asc" ? results.OrderBy(prop.GetValue) :
    //            results.OrderByDescending(prop.GetValue);
    //    }
    //}
}