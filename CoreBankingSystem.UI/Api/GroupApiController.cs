﻿using BusinessLogic.IServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using DomainModels;

namespace CoreBankingSystem.UI.Api
{
    public class GroupApiController : ApiController
    {
        private readonly IGroupService _groupService = null;
        public GroupApiController(IGroupService groupService)
        {
            _groupService = groupService;
        }
        // GET api/<controller>
        public IEnumerable<GroupModel> Get()
        {
          return  _groupService.GetGroups();
        }

        // GET api/<controller>/5
        public GroupModel Get(int id)
        {
            return _groupService.GetGroupsById(id);
        }

        public GroupModel Get(int id, string fromScreen)
        {
            return _groupService.GetGroupsById(id);
        }

        // POST api/<controller>
        public void Post([FromBody]GroupModel GroupModel)
        {
            _groupService.SaveGroup(GroupModel);
        }

        // PUT api/<controller>/5
        public void Put( [FromBody]GroupModel GroupModel)
        {
            try
            {
                GroupModel.RecordStatus = "A";
                int a = _groupService.UpdateGroup(GroupModel);

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        // DELETE api/<controller>/5
        public void Delete(long id)
        {
            _groupService.Delete(id);
        }
        [Route("api/Groupapi/GroupNames")]
        [ActionName("GroupNames")]
        public IEnumerable<ChoiceOption> GetGroupNames()
        {
            return _groupService.GetGroupNames();
        }
    }
}