﻿using BusinessLogic.IServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using DomainModels;


namespace CoreBankingSystem.UI.Api
{
    public class AccountApiController : BaseController
    {
        private readonly IAccountService _accountService = null;
        public AccountApiController(IAccountService accountService)
        {
            _accountService = accountService;
        }
        // GET api/<controller>
        public IEnumerable<AccountModel> Get()
        {
            return _accountService.GetAccountDetails();
        }


        // GET api/<controller>/5
        public AccountModel Get(long id)
        {
            return _accountService.GetAccountDetailsById(id);
        }

        // POST api/<controller>
        public void Post([FromBody]AccountModel accountModel)
        {
            _accountService.Save(accountModel);
        }

        // PUT api/<controller>/5
        public void Put([FromBody]AccountModel accountModel)
        {
            _accountService.Update(accountModel);
        }

        // DELETE api/<controller>/5
        public void Delete(int id)
        {
            _accountService.Delete(id);
        }
        [Route("api/accountApi/AccountNumbers")]
        [ActionName("AccountNumbers")]
        public IEnumerable<ChoiceOption> GetAccountNumbers()
        {
            return _accountService.GetAccountNumbers();
        }
    }
}