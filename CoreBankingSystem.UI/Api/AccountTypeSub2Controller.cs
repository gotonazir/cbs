﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using BusinessLogic.IServices;

namespace CoreBankingSystem.UI.Api
{
    public class AccountTypeSub2Controller : ApiController
    {
        private readonly IAccountSub2Service _accountSub2Service = null;

        public AccountTypeSub2Controller(IAccountSub2Service accountSub2Service)
        {
            _accountSub2Service = accountSub2Service;
        }
        // GET api/<controller>
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET api/<controller>/5
        public string Get(int id)
        {
            return "value";
        }

        // POST api/<controller>
        public void Post([FromBody]string value)
        {
        }

        // PUT api/<controller>/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/<controller>/5
        public void Delete(int id)
        {
        }
    }
}