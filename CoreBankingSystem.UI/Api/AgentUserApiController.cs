﻿using BusinessLogic.IServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using DomainModels;

namespace CoreBankingSystem.UI.Api
{
    public class AgentUserApiController : ApiController
    {
        private readonly IAgentUserService _agentUserService = null;
        public AgentUserApiController(IAgentUserService agentUserService)
        {
            _agentUserService = agentUserService;
        }
        // GET api/<controller>
        public IEnumerable<AgentUserModel> Get()
        {
          return  _agentUserService.GetAgentUsers();
        }

        // GET api/<controller>/5
        public AgentUserModel Get(int id)
        {
            return _agentUserService.GetAgentUsersById(id);
        }

        public AgentUserModel Get(int id, string fromScreen)
        {
            return _agentUserService.GetAgentUsersById(id);
        }

        // POST api/<controller>
        public void Post([FromBody]AgentUserModel agentUserModel)
        {
            _agentUserService.SaveAgentUser(agentUserModel);
        }

        // PUT api/<controller>/5
        public void Put( [FromBody]AgentUserModel agentUserModel)
        {
            try
            {
                agentUserModel.RecordStatus = "A";
                int a = _agentUserService.UpdateAgentUser(agentUserModel);

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        // DELETE api/<controller>/5
        public void Delete(long id)
        {
            _agentUserService.Delete(id);
        }
        [Route("api/agentUserapi/AgentUserNames")]
        [ActionName("AgentUserNames")]
        public IEnumerable<ChoiceOption> GetAgentNames()
        {
            return _agentUserService.GetAgentNames();
        }
    }
}