﻿using BusinessLogic.IServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using DomainModels;

namespace CoreBankingSystem.UI.Api
{
    public class AgentGroupApiController : ApiController
    {
        private readonly IAgentGroupService _agentGroupService = null;
        public AgentGroupApiController(IAgentGroupService agentGroupService)
        {
            _agentGroupService = agentGroupService;
        }
        // GET api/<controller>
        public IEnumerable<AgentGroupModel> Get()
        {
          return  _agentGroupService.GetAgentGroups();
        }

        // GET api/<controller>/5
        public AgentGroupModel Get(int id)
        {
            return _agentGroupService.GetAgentGroupById(id);
        }

        public AgentGroupModel Get(int id, string fromScreen)
        {
            return _agentGroupService.GetAgentGroupById(id);
        }

        // POST api/<controller>
        public void Post([FromBody]AgentGroupModel agentGroupModel)
        {
            _agentGroupService.SaveAgentGroup(agentGroupModel);
        }

        // PUT api/<controller>/5
        public void Put( [FromBody]AgentGroupModel agentGroupModel)
        {
            try
            {
                agentGroupModel.RecordStatus = "A";
                int a = _agentGroupService.UpdateAgentGroup(agentGroupModel);

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        // DELETE api/<controller>/5
        public void Delete(long id)
        {
            _agentGroupService.Delete(id);
        }
        [Route("api/agentGroupapi/AgentGroupNames")]
        [ActionName("AgentNames")]
        public IEnumerable<ChoiceOption> GetAgentNames()
        {
            return _agentGroupService.GetAgentNames();
        }
    }
}