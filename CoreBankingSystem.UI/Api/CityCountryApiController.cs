﻿using BusinessLogic.IServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using DomainModels;

namespace CoreBankingSystem.UI.Api
{
    public class CityCountryApiController : ApiController
    {
        private readonly ICityCountryService _cityCountryService = null;
        public CityCountryApiController(ICityCountryService cityCountryService)
        {
            _cityCountryService = cityCountryService;
        }
      
        // POST api/<controller>
      
        [Route("api/cityCountryapi/CityNames")]
        [ActionName("BranchNames")]
        public IEnumerable<ChoiceOption> GetCityCountryNames()
        {
            return _cityCountryService.GetCityCountryNames();
        }
    }
}