﻿using BusinessLogic.IServices;
using DomainModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace CoreBankingSystem.UI.Api
{
    public class ClientDocumentController : ApiController
    {
        private readonly IClientDocumentService _clientDocService = null;

        public ClientDocumentController(IClientDocumentService clientDocService)
        {
            _clientDocService = clientDocService;
        }
        // GET api/values
        public List<ClientDocumentModel> Get()
        {
            var data = _clientDocService.GetClientDocuments();

            return data;
        }

        // GET api/values/5
        public string Get(int id)
        {
            // POC TODO 
            try
            {
                var data = _clientDocService.GetClientDocuments();

                int a = _clientDocService.Delete(data[0]);

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }

            return "value";
        }



        // POST api/values
        public void Post([FromBody]string value)
        {
        }

        // PUT api/values/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/values/5
        // [Authorize(Roles = "Admin")]
        public void Delete(int id)
        {

        }


    }
}