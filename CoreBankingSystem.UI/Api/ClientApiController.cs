﻿using BusinessLogic.IServices;
using DomainModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WebGrease.Css.Extensions;

namespace CoreBankingSystem.UI.Api
{
    public class ClientApiController : BaseController
    {
        private readonly IClientService _clientService = null;

        public ClientApiController(IClientService clientService)
        {
            _clientService = clientService;
        }
        // GET api/values
        public List<ClientModel> Get()
        {
            var data = _clientService.GetClient();

            return data;
        }

        // GET api/values/5
        public ClientModel Get(int id)
        {
            return _clientService.GetClientsById(id);
        }

        public ClientModel Get(int id, string fromScreen)
        {
            var clientModel = _clientService.GetClientsById(id);
            clientModel.SignBase64 = Base64EncodeBytes(clientModel.SignImage);
            foreach (var item in clientModel.ClientDocuments)
            {
                item.DocumentBase64 = Base64EncodeBytes(item.DocumentImage);
            }
            //  clientModel.DocumentBase64 = Base64EncodeBytes(clientModel.ClientDocuments.ForEach((x)=> x.DocumentImage = ).DocumentImage);
            return clientModel;
        }

        // POST api/values
        public void Post([FromBody]ClientModel clientModel)
        {

            var data = clientModel.ClientDocuments;
            bool hasClient = _clientService.HasClient(clientModel.FullName);
            if (hasClient)
            {
                throw new Exception("Client Name Already Exists");
            }

            if (clientModel.SignBase64 != null)
            {
                clientModel.SignImage = System.Convert.FromBase64String(clientModel.SignBase64);
            }
            
            //clientModel.DocumentImage = System.Convert.FromBase64String(clientModel.DocumentBase64);
            clientModel.RecordStatus = "A";
            if (clientModel.ClientDocuments != null)
            {
                foreach (var item in clientModel.ClientDocuments)
                {
                    item.RecordStatus = "A";
                    item.DocumentImage = System.Convert.FromBase64String(item.DocumentBase64);
                }
            }
            _clientService.Save(clientModel);
        }

        // PUT api/values/5
        public void Put([FromBody]ClientModel clientModel)
        {
            try
            {
                var data = clientModel.ClientDocuments;
                //bool hasClient = _clientService.HasClient(clientModel.FullName);
                //if (hasClient)
                //{
                //    throw new Exception("Client Name Already Exists");
                //}
                clientModel.SignImage = System.Convert.FromBase64String(clientModel.SignBase64);
               // clientModel.DocumentImage = System.Convert.FromBase64String(clientModel.DocumentBase64);
                clientModel.RecordStatus = "A";
                if (clientModel.ClientDocuments != null)
                {
                    foreach (var item in clientModel.ClientDocuments)
                    {
                        item.RecordStatus = "A";
                        item.DocumentImage = System.Convert.FromBase64String(item.DocumentBase64);
                    }
                }
                int a = _clientService.Update(clientModel);
            }

            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }
        public static byte[] ConvertInt32ToByteArray(Int32 I32)
        {
            return BitConverter.GetBytes(I32);
        }
        // DELETE api/values/5
        // [Authorize(Roles = "Admin")]
        public void Delete(int id)
        {
            _clientService.Delete(id);
        }

        [HttpGet]
        [Route("api/clientapi/ClientNames")]
        [ActionName("ClientNames")]
        public IEnumerable<ChoiceOption> ClientNames()
        {
            var choiceOptions = _clientService.GetNames().ToList();
            return choiceOptions;
        }

        public static string Base64EncodeBytes(byte[] inputBytes)
        {
            // Each 3-byte sequence in inputBytes must be converted to a 4-byte 
            // sequence 
            long arrLength = (long)(4.0d * inputBytes.Length / 3.0d);
            if ((arrLength % 4) != 0)
            {
                // increment the array length to the next multiple of 4 
                //    if it is not already divisible by 4
                arrLength += 4 - (arrLength % 4);
            }

            char[] encodedCharArray = new char[arrLength];
            Convert.ToBase64CharArray(inputBytes, 0, inputBytes.Length, encodedCharArray, 0);

            return (new string(encodedCharArray));
        }
    }
}