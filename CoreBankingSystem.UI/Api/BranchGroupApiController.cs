﻿using BusinessLogic.IServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using DomainModels;

namespace CoreBankingSystem.UI.Api
{
    public class BranchGroupApiController : ApiController
    {
        private readonly IBranchGroupService _branchGroupService = null;
        public BranchGroupApiController(IBranchGroupService branchGroupService)
        {
            _branchGroupService = branchGroupService;
        }
        // GET api/<controller>
        public IEnumerable<BranchGroupModel> Get()
        {
          return _branchGroupService.GetBranchGroups();
        }

        // GET api/<controller>/5
        public BranchGroupModel Get(int id)
        {
            return _branchGroupService.GetBranchGroupsById(id);
        }

        public BranchGroupModel Get(int id, string fromScreen)
        {
            return _branchGroupService.GetBranchGroupsById(id);
        }

        // POST api/<controller>
        public void Post([FromBody]BranchGroupModel branchGroupModel)
        {
            _branchGroupService.SaveBranchGroup(branchGroupModel);
        }

        // PUT api/<controller>/5
        public void Put( [FromBody]BranchGroupModel branchGroupModel)
        {
            try
            {
                branchGroupModel.RecordStatus = "A";
                int a = _branchGroupService.UpdateBranchGroup(branchGroupModel);

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        // DELETE api/<controller>/5
        public void Delete(long id)
        {
            _branchGroupService.Delete(id);
        }
        [Route("api/branchGroupapi/BranchNames")]
        [ActionName("BranchNames")]
        public IEnumerable<ChoiceOption> GetBranchNames()
        {
            return _branchGroupService.GetBranchNames();
        }
    }
}