﻿using BusinessLogic.IServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using DomainModels;

namespace CoreBankingSystem.UI.Api
{
    public class AgentApiController : ApiController
    {
        private readonly IAgentService _agetntService = null;
        public AgentApiController(IAgentService agentService)
        {
            _agetntService = agentService;
        }
        // GET api/<controller>
        public IEnumerable<AgentModel> Get()
        {
          return  _agetntService.GetAgents();
        }

        // GET api/<controller>/5
        public AgentModel Get(int id)
        {
            return _agetntService.GetAgentById(id);
        }

        public AgentModel Get(int id, string fromScreen)
        {
            return _agetntService.GetAgentById(id);
        }

        // POST api/<controller>
        public void Post([FromBody]AgentModel agentModel)
        {
            _agetntService.SaveAgent(agentModel);
        }

        // PUT api/<controller>/5
        public void Put( [FromBody]AgentModel agentModel)
        {
            //_agentService.UpdateAgent(agentModel);
            try
            {
                agentModel.RecordStatus = "A";
                int a = _agetntService.UpdateAgent(agentModel);

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        // DELETE api/<controller>/5
        public void Delete(long id)
        {
            _agetntService.Delete(id);
        }
        [Route("api/agentapi/AgentNames")]
        [ActionName("AgentNames")]
        public IEnumerable<ChoiceOption> GetAgentNames()
        {
            return _agetntService.GetAgentNames();
        }
    }
}