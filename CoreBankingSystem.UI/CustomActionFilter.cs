﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using Newtonsoft.Json.Linq;

namespace CoreBankingSystem.UI
{
    public class CustomActionFilter : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            var token = filterContext.HttpContext.Request.Cookies.Get("accessToken");

            //string t = "K2HTe2ZoR5XP9Dh4RMwPQYFbnBRc1Qru0wookOm8zFSBzqVA6Q-8IhjL1IF0VLmqBbDTjJo8gINYlMt7XVGe93etbazChTSIaRPEHQweWkxXiizNlKYiGtalYBhEXvXoRWFxZd3vgncx13_eYCFWlcxHC_ql_qcYezZW7kMA4GhRmmEKutiOrowMBF3h1mIpit8NYbbV5tcK050xzFd0-juvmkdvu5650PPyvajN0fC9akpLn9cm-VwrKoCO0sYc0fzqh8VF8329-Rr0F2ycktKxWrp7F30LhzCW-m0isWExK1LWz1qZQ0ygToZbyHQ5QaxTaPdDnw2-y50WmOpteDZaLt9GDrBItlbVKF3WiGw";
                
            //var parts = t.Split('.');
            //var decoded = Convert.FromBase64String(parts[1]);
            //var part = Encoding.UTF8.GetString(decoded);
            //var jwt = JObject.Parse(part);
            //var name = jwt["name"].Value<string>();


            //if (token != null)
            //    filterContext.HttpContext.Request.Headers.Add("Authorization", $"Bearer {token}");

            base.OnActionExecuting(filterContext);
        }
    }
}