﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using BusinessLogic.IServices;
using DomainModels;

namespace CoreBankingSystem.WebApi.Controllers
{
    [Authorize]
    public class ValuesController : ApiController
    {
        private readonly IUserService _userService = null;
        public ValuesController(IUserService userService)
        {
            _userService = userService;
        }
        // GET api/values
        public IEnumerable<string> Get()
        {
            var data = _userService.GetUsers();
            return new string[] { "value1", "value2" };
        }

        // GET api/values/5
        public string Get(int id)
        {
            // POC TODO 
            try
            {
              
             int a=   _userService.Delete(1);
              
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }

            return "value";
        }

        // POST api/values
        public void Post([FromBody]string value)
        {
        }

        // PUT api/values/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/values/5
       // [Authorize(Roles = "Admin")]
        public void Delete(int id)
        {
           
        }
    }
}
