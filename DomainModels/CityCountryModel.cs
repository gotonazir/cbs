using System;
using System.Collections.Generic;

namespace  DomainModels
{
    public partial class CityCountryModel
    {
        public int CityID { get; set; }
        public string CityName { get; set; }
        public string Country { get; set; }
    }
}
