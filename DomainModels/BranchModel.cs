using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace  DomainModels
{
    public partial class BranchModel
    {
        public BranchModel()
        {
        }

        [Required(ErrorMessage = "The Required field is empty")]
        [DisplayName("Branch")]
        public int BranchID { get; set; }
        [Required(ErrorMessage = "The Required field is empty")]
        [DisplayName("Branch Name")]
        public string BranchName { get; set; }
        [DisplayName("Address")]
        public string Address { get; set; }
        [DisplayName("Phone")]
        public string Phone { get; set; }

        public string Fax { get; set; }
        public Nullable<short> BranchType { get; set; }
        public byte[] TimeStamp { get; set; }
        public string RecordStatus { get; set; }
    }
}
