using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace  DomainModels
{
    public partial class AgentModel
    {
        public AgentModel()
        {
        }
        public long AccountNumber { get; set; }
        [Required(ErrorMessage = "The Required field is empty")]
        [DisplayName("Agent")]
        public int AgentID { get; set; }
        [Required(ErrorMessage = "The Required field is empty")]
        [DisplayName("Agent Number")]
        public int AgentNo { get; set; }
        [Required(ErrorMessage = "The Required field is empty")]
        [DisplayName("Agent Name")]
        public string AgentName { get; set; }
        [Required(ErrorMessage = "The Required field is empty")]
        [DisplayName("Account ID")]
        public long AccountID { get; set; }
        [DisplayName("EnglishName")]
        public string EnglishName { get; set; }
        [DisplayName("Phone")]
        public string Phone { get; set; }
        [DisplayName("Fax")]
        public string Fax { get; set; }
        [DisplayName("Adress")]
        public string Adress { get; set; }
        [DisplayName("Notes")]
        public string Notes { get; set; }
        public string RecordStatus { get; set; }

    }
}
