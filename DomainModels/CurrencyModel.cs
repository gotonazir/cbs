using System;
using System.Collections.Generic;

namespace  DomainModels
{
    public partial class CurrencyModel
    {
        public CurrencyModel()
        {
        }

        public int CurrencyID { get; set; }
        public string Name { get; set; }
        public string ShortNameAr { get; set; }
        public string ShortNameEn { get; set; }
        public string Notes { get; set; }
        public string IconName { get; set; }
    }
}
