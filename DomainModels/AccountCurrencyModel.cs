using System;
using System.Collections.Generic;

namespace  DomainModels
{
    public partial class AccountCurrencyModel
    {
        public long AccountCurrencyID { get; set; }
        public long AccountID { get; set; }
        public int CurrencyID { get; set; }
        public decimal Balance { get; set; }
        public Nullable<decimal> MinBalance { get; set; }
        public Nullable<decimal> OverDraft { get; set; }
        public byte[] TimeStamp { get; set; }
        public string RecordStatus { get; set; }
    }
}
