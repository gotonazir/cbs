﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DomainModels
{
    public class AccountTypeTreeModel
    {
        public int AccountTypeTreeId { get; set; }
        public int ParentId { get; set; }
        public string Description { get; set; }
        public Nullable<int> Value { get; set; }
        public Nullable<long> AccountRange { get; set; }
        public Nullable<long> CurrentAccountNo { get; set; }
        public string RecordStatus { get; set; }
    }
}
