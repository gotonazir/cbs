using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace  DomainModels
{
    public partial class BranchGroupModel
    {
        public BranchGroupModel()
        {
        }        
        public long BranchGroupID { get; set; }

        [Required(ErrorMessage = "The Required field is empty")]
        [DisplayName("Branch")]
        public int BranchID { get; set; }

        [Required(ErrorMessage = "The Required field is empty")]
        [DisplayName("Group ID")]
        public int GroupID { get; set; }        
        public string RecordStatus { get; set; }
    }
}
