using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace  DomainModels
{
    public partial class AgentGroupModel
    {
        public AgentGroupModel()
        {
        }

        [Required(ErrorMessage = "The Required field is empty")]
        [DisplayName("Agent Group ID ")]
        public long AgentGroupID { get; set; }
        [Required(ErrorMessage = "The Required field is empty")]
        [DisplayName("Agent ID")]
        public int AgentID { get; set; }
        [Required(ErrorMessage = "The Required field is empty")]
        [DisplayName("Group ID")]
        public int GroupID { get; set; }       
        public string RecordStatus { get; set; }
        public string GroupName { get; set; }
        public string AgentName { get; set; }
    }
}
