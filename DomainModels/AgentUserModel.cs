using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace  DomainModels
{
    public partial class AgentUserModel
    {
        public AgentUserModel()
        {
        }

        [Required(ErrorMessage = "The Required field is empty")]
        [DisplayName("Agent Group ID ")]
        public long AgentUserID { get; set; }
        [Required(ErrorMessage = "The Required field is empty")]
        [DisplayName("Agent ID")]
        public int AgentID { get; set; }
        [Required(ErrorMessage = "The Required field is empty")]
        [DisplayName("User Name")]
        public string UserName { get; set; }
        [DisplayName("Notes")]
        public string Notes { get; set; }
        public string AgentName { get; set; }
        
        public string RecordStatus { get; set; }
    }
}
