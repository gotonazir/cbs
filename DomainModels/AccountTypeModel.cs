using System;
using System.Collections.Generic;

namespace  DomainModels
{
    public partial class AccountTypeModel
    {
        public AccountTypeModel()
        {
        }

        public int AccountTypeID { get; set; }
        public string Name { get; set; }
        public string Notes { get; set; }
        public string RecordStatus { get; set; }
    }
}
