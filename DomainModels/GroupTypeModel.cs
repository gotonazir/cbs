using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace  DomainModels
{
    public partial class GroupTypeModel
    {
        public GroupTypeModel()
        {
        }

        [Required(ErrorMessage = "The Required field is empty")]
        [DisplayName("Group Type ID")]
        public int GroupTypeID { get; set; }

        [Required(ErrorMessage = "The Required field is empty")]
        [DisplayName("Group Type Name")]
        public string GroupTypeName { get; set; }

        public string RecordStatus { get; set; }
    }
}
