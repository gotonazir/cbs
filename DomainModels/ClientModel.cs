using System;
using System.Collections.Generic;
using System.ComponentModel;
using  System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Runtime.CompilerServices;

namespace  DomainModels
{
    public partial class ClientModel
    {
        public ClientModel()
        {
            ClientDocuments = new List<ClientDocumentModel>();
        }
        public long ClientID { get; set; }
        [Required]
        public string FullName { get; set; }
        public string CityName { get; set; }
        public string GuarantorFullName { get; set; }
        public string PrivilegeFullName { get; set; }
        [Required]
        public string Address { get; set; }
        [Required]
        public int CityID { get; set; }
        public string Phone { get; set; }
        public string Mobile { get; set; }
        public string Fax { get; set; }
        public string PersonOrCorp { get; set; }
        public Nullable<long> GuarantorID { get; set; }
        public Nullable<int> PrivilegeID { get; set; }
        public string Notes { get; set; }
        [NotMapped]
        public byte[] TimeStamp { get; set; }
        public string RecordStatus { get; set; }
        public byte[] SignImage { get; set; }

        public string DocumentType { get; set; }
        public string PlaceOfIssue { get; set; }
        public Nullable<System.DateTime> IssueDt { get; set; }
        public Nullable<System.DateTime> ExpiryDt { get; set; }
        public byte[] DocumentImage { get; set; }

        public string SignBase64 { get; set; }
        public string DocumentBase64 { get; set; }
        public List<ClientDocumentModel> ClientDocuments { get;set;}
    }
}
