using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace  DomainModels
{
    public partial class GroupModel
    {
        public GroupModel()
        {
        }

        [Required(ErrorMessage = "The Required field is empty")]
        [DisplayName("GroupID")]
        public int GroupID { get; set; }

        [Required(ErrorMessage = "The Required field is empty")]
        [DisplayName("Group Name")]
        public string GroupName { get; set; }

        [Required(ErrorMessage = "The Required field is empty")]
        [DisplayName("Group Type ID")]
        public int GroupTypeID { get; set; }

        [Required(ErrorMessage = "The Required field is empty")]
        [DisplayName("Notes")]
        public string Notes { get; set; }

        public string RecordStatus { get; set; }
        public string GroupTypeName { get; set; }
        
    }
}
