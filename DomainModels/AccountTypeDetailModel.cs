using System;
using System.Collections.Generic;

namespace  DomainModels
{
    public partial class AccountTypeDetailModel
    {
        public long AccountID { get; set; }
        public int AccountTreeID { get; set; }
        public int AccountTypeID { get; set; }
        public int AccountTypeSub1ID { get; set; }
        public int AccountTypeSub2ID { get; set; }
        public byte[] TimeStamp { get; set; }
        public string RecordStatus { get; set; }
    }
}
