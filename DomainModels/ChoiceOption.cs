﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DomainModels
{
    public class ChoiceOption
    {
        public long id { get; set; }
        public string text { get; set; }
    }
}
