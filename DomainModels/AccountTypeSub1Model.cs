using System;
using System.Collections.Generic;

namespace  DomainModels
{
    public partial class AccountTypeSub1Model
    {
        public AccountTypeSub1Model()
        {
        }

        public int AccountTypeSub1ID { get; set; }
        public string Name { get; set; }
        public string Notes { get; set; }
        public string RecordStatus { get; set; }
    }
}
