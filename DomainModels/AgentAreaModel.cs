using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace  DomainModels
{
    public partial class AgentAreaModel
    {
        public AgentAreaModel()
        {
        }

        [Required(ErrorMessage = "The Required field is empty")]
        [DisplayName("Agent Area")]
        public int AgentAreaID { get; set; }

        [Required(ErrorMessage = "The Required field is empty")]
        [DisplayName("Area Number")]
        public int AreaNo { get; set; }

        [Required(ErrorMessage = "The Required field is empty")]
        [DisplayName("Area Name")]
        public string AreaName { get; set; }

        [Required(ErrorMessage = "The Required field is empty")]
        [DisplayName("Agent ID")]
        public int AgentID { get; set; }

        [DisplayName("City ID")]
        public int? CityID { get; set; }

        [DisplayName("Tranaction Type ID")]
        public int? TranactionTypeID { get; set; }

        [DisplayName("Local Or intl")]
        public string LocalOrintl { get; set; }

        [DisplayName("Phone")]
        public string Phone { get; set; }
        [DisplayName("Fax")]
        public string Fax { get; set; }
        [DisplayName("Adress")]
        public string Adress { get; set; }

        [DisplayName("Required Approval")]
        public string RequiredApproval { get; set; }

        [DisplayName("Direct Exchange")]
        public string DirectExchange { get; set; }

        [DisplayName("Freez Area")]
        public string FreezArea { get; set; }

        public string Notes { get; set; }
        
        public string AgentName { get; set; }
        
        public string CityName { get; set; }

    }
}
