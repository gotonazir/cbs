using System;
using System.Collections.Generic;

namespace  DomainModels
{
    public partial class UserBranchModel
    {
        public int UserID { get; set; }
        public int BranchID { get; set; }
        public Nullable<bool> DefaultBranch { get; set; }
        public string RecordStatus { get; set; }
    }
}
