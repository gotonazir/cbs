using System;
using System.Collections.Generic;

namespace  DomainModels
{
    public partial class AccountTypeSub2Model
    {
        public AccountTypeSub2Model()
        {
        }

        public int AccountTypeSub2ID { get; set; }
        public string Name { get; set; }
        public string Notes { get; set; }
        public string RecordStatus { get; set; }
    }
}
