using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace  DomainModels
{
    public partial class SandooqModel
    {
        public SandooqModel()
        {
        }

        [Required(ErrorMessage = "The Required field is empty")]
        [DisplayName("SandooqID")]
        public int SandooqID { get; set; }

        [Required(ErrorMessage = "The Required field is empty")]
        [DisplayName("Account ID")]
        public long AccountID { get; set; }

        [DisplayName("TransactionType")]
        public string TransactionType { get; set; }

        [DisplayName("Notes")]
        public string Notes { get; set; }

        public long AccountNo { get; set; }
        public string RecordStatus { get; set; }
        
    }
}
