using System;
using System.Collections.Generic;

namespace  DomainModels
{
    public partial class BankTransactionModel
    {
        public long BTID { get; set; }
        public int TransactionMode { get; set; }
        public long ReceiptNo { get; set; }
        public decimal AmountReceived { get; set; }
        public int CurrencyIDReceived { get; set; }
        public Nullable<long> HomeBranchID { get; set; }
        public long AccountID { get; set; }
        public string PersonName { get; set; }
        public Nullable<long> SandooqID { get; set; }
        public Nullable<System.DateTime> TransactionDate { get; set; }
        public Nullable<long> TrasactionBranchID { get; set; }
        public Nullable<decimal> CommissionAmount { get; set; }
        public Nullable<int> CommissionCurrencyID { get; set; }
        public Nullable<long> ToBranchID { get; set; }
        public Nullable<long> ToAccountID { get; set; }
        public Nullable<decimal> ToAmount { get; set; }
        public Nullable<int> ToCurrencyID { get; set; }
        public Nullable<bool> AmountCounted { get; set; }
        public Nullable<short> PrintReceipt { get; set; }
        public string Notes { get; set; }
        public string RecordStatus { get; set; }
    }
}
