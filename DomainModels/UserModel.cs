using System;
using System.Collections.Generic;

namespace  DomainModels
{
    public partial class UserModel
    {
        public int UserID { get; set; }
        public Nullable<int> UserNo { get; set; }
        public string UserName { get; set; }
        public string PassWord { get; set; }
        public string UserRole { get; set; }
        public int DefaultBranchID { get; set; }
        public string BlockedStatus { get; set; }
        public short ChangePasswordDays { get; set; }
        public System.DateTime ChangePasswordDt { get; set; }
        public string Title { get; set; }
        public Nullable<decimal> Salary { get; set; }
        public string Notes { get; set; }
        public string RecordStatus { get; set; }
    }
}
