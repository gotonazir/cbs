using System;
using System.Collections.Generic;

namespace  DomainModels
{
    public partial class AccountTreeModel
    {
        public AccountTreeModel()
        {
            AccountTypes = new List<AccountTypeModel>();
        }

        public int AccountTreeID { get; set; }
        public string Name { get; set; }
        public IList<AccountTypeModel> AccountTypes { get; set; }

    }
}
