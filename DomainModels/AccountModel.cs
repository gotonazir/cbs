using System;
using System.Collections.Generic;

namespace DomainModels
{
    public partial class AccountModel
    {
        public AccountModel()
        {
        }

        public long AccountID { get; set; }
        public int AccountTypeTreeId { get; set; }
        public long AccountNo { get; set; }
        public int BranchID { get; set; }
        public long ClientID { get; set; }
        public System.DateTime OpenDt { get; set; }
        public Nullable<long> VIPNo { get; set; }
        public Nullable<long> BeneficiaryID { get; set; }
        public Nullable<long> AuthorizedID { get; set; }
        public string Notes { get; set; }
        public Nullable<System.DateTime> CloseDt { get; set; }
        public byte[] timestamp { get; set; }
        public string RecordStatus { get; set; }

        public string AccountType { get; set; }
        public string BranchName { get; set; }
        public string ClientName { get; set; }
        public string Beneficiary { get; set; }
        public string Authorized { get; set; }
        public IList<AccountCurrencyModel> AccountCurrencies { get; set; }
    }
}
