﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessLogic.IServices;
using DataAccess.IRepository;
using DomainModels;
using  AutoMapper;
using DataAccess.Models;

namespace BusinessLogic.Services
{
    public class ClientDocumentService : IClientDocumentService
    {
        private readonly IClientDocumentRepository _clientDocumentRepositor;
       
        public ClientDocumentService(IClientDocumentRepository clientDocumnetRepository)
        {
            _clientDocumentRepositor = clientDocumnetRepository;
        }
        public List<ClientDocumentModel> GetClientDocuments()
        {
            var userDetails = Mapper.DynamicMap<List<ClientDocumentModel>>(_clientDocumentRepositor.GetAll().ToList());

            return userDetails;
            
        }

        public int Save(ClientDocumentModel userModel)
        {
            var user = Mapper.DynamicMap<ClientDocument>(userModel);
            _clientDocumentRepositor.Add(user);
            return _clientDocumentRepositor.SaveChanges();

        }
                
        public int Update(ClientDocumentModel userModel)
        {
            // should be a primary key(UserID) value check 
            var usr = _clientDocumentRepositor.Find(x => x.ClientID == userModel.ClientID).FirstOrDefault();
            if (usr != null)
            {
                Mapper.CreateMap<ClientDocumentModel, ClientDocument>()
                    .ForMember(dest => dest.ClientID, opt => opt.Ignore()); // ignore primary key while update/delete
                ClientDocument user = (ClientDocument)Mapper.Map(userModel, usr);

                return _clientDocumentRepositor.SaveChanges();
            }

            return 0;
        }

        public int Delete(ClientDocumentModel userModel)
        {
            // should be a primary key(UserID) value check 
            var usr = _clientDocumentRepositor.Find(x => x.ClientID == userModel.ClientID).FirstOrDefault();
            if (usr != null)
            {
                Mapper.CreateMap<ClientDocumentModel, ClientDocument>()
                    .ForMember(dest => dest.RecordStatus, opt => opt.MapFrom(x => "D"))
                    .ForMember(dest => dest.ClientID, opt => opt.Ignore()); // ignore primary key while update/delete
                ClientDocument user = (ClientDocument)Mapper.Map(userModel, usr);

                return _clientDocumentRepositor.SaveChanges();
            }

            return 0;
        }

    }
}
