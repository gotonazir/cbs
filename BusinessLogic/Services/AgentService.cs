﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using BusinessLogic.IServices;
using DataAccess.IRepository;
using DataAccess.Models;
using DomainModels;

namespace BusinessLogic.Services
{
    public class AgentService : IAgentService
    {

        private readonly IAgentRepository _agentRepository;
        public AgentService(IAgentRepository agentRepository)
        {
            _agentRepository = agentRepository;
        }

        public int Delete(long id)
        {
            var agent = _agentRepository.Find(x => x.AgentID == id).FirstOrDefault();

            if (agent != null)
            {
                agent.RecordStatus = "D";
                return _agentRepository.SaveChanges();
            }
            return 0;
        }

        public List<AgentModel> GetAgents()
        {
            var agentDetails = Mapper.DynamicMap<List<AgentModel>>(_agentRepository.GetAgents().ToList());

            return agentDetails;
        }

        public AgentModel GetAgentById(long id)
        {         
            var AgentModel = Mapper.DynamicMap<AgentModel>(_agentRepository.Find(x => x.AgentID == id).FirstOrDefault());

            return AgentModel;
        }

        public int SaveAgent(AgentModel agentModel)
        {
            if (_agentRepository.Find(x => x.AgentName == agentModel.AgentName).Any())
            {
                throw new Exception("Agent Name already exists");
            }

            agentModel.RecordStatus = "A";
            var Agent = Mapper.DynamicMap<Agent>(agentModel);
            _agentRepository.Add(Agent);
            return _agentRepository.SaveChanges();
        }

        public int UpdateAgent(AgentModel AgentModel)
        {
            // should be a primary key(UserID) value check 
            var Agent = _agentRepository.Find(x => x.AgentID == AgentModel.AgentID).FirstOrDefault();
            if (Agent != null)
            {
                Mapper.CreateMap<AgentModel, Agent>()
                    .ForMember(dest => dest.AgentID, opt => opt.Ignore()); // ignore primary key while update/delete
                Agent user = (Agent)Mapper.Map(AgentModel, Agent);

                return _agentRepository.SaveChanges();
            }

            return 0;
        }

        public IEnumerable<ChoiceOption> GetAgentNames()
        {
            return _agentRepository.GetAll().Select(x => new ChoiceOption
            {
                id = x.AgentID,
                text = x.AgentName
            }).ToList();
        }
    }
}
