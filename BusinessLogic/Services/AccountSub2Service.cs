﻿using BusinessLogic.IServices;
using DomainModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataAccess.IRepository;

namespace BusinessLogic.Services
{
    public class AccountSub2Service : IAccountSub2Service
    {
        private readonly IAccountSub2Repository _accountSub2Repository = null;
        public AccountSub2Service(IAccountSub2Repository accountSub2Repository)
        {
            _accountSub2Repository = accountSub2Repository;
        }
        public int DeleteAccountSub2(long id)
        {
            throw new NotImplementedException();
        }


        public IEnumerable<ChoiceOption> GetAccountSub2ChoiceOptions()
        {
            return _accountSub2Repository.GetAll().Select(x => new ChoiceOption
            {
                id = x.AccountTypeSub2ID,
                text = x.Name
            }).OrderBy(x => x.text).Distinct();

        }

        public IEnumerable<ChoiceOption> GetAccountSub2ChoiceOptions(long id)
        {
            return _accountSub2Repository.GetAll().Where(x=>x.AccountTypeSub2ID == id).Select(x => new ChoiceOption
            {
                id = x.AccountTypeSub2ID,
                text = x.Name
            }).OrderBy(x=>x.text).Distinct();

        }

        public List<AccountTypeSub2Model> GetAccountTypesSub2()
        {
            throw new NotImplementedException();
        }

        public AccountTypeSub2Model GetAccountTypeSub2ById(long id)
        {
            throw new NotImplementedException();
        }

        public int Save(AccountTypeSub2Model accountTypeSub2Model)
        {
            throw new NotImplementedException();
        }

        public int Update(AccountTypeSub2Model accountTypeSub2Model)
        {
            throw new NotImplementedException();
        }
    }
}
