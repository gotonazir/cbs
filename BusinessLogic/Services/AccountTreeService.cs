﻿using AutoMapper;
using BusinessLogic.IServices;
using DataAccess.IRepository;
using DomainModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogic.Services
{
    public class AccountTreeService : IAccountTreeService
    {
        private readonly IAccountTreeRepository _accountTreeRepository = null;
        public AccountTreeService(IAccountTreeRepository accountTreeRepository)
        {
            _accountTreeRepository = accountTreeRepository;
        }
        public int Delete(long id)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<ChoiceOption> GetAccountChoiceOptions()
        {
            return _accountTreeRepository.GetAll().Select(x => new ChoiceOption
            {
                id = x.AccountTreeID,
                text = x.Name
            }).OrderBy(x=>x.text).Distinct();
        }

        public AccountTreeModel GetAccountTreeById(long id)
        {
            throw new NotImplementedException();
        }

        public List<AccountTreeModel> GetAccountTrees()
        {
           var data = Mapper.DynamicMap<List<AccountTreeModel>>(_accountTreeRepository.GetAccountTrees().ToList());
            return data;
        }

        public int Save(AccountTreeModel accountTreeModel)
        {
            throw new NotImplementedException();
        }

        public int Update(AccountTreeModel accountTreeModel)
        {
            throw new NotImplementedException();
        }


    }
}
