﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using BusinessLogic.IServices;
using DataAccess.IRepository;
using DataAccess.Models;
using DomainModels;

namespace BusinessLogic.Services
{
    public class AgentAreaService : IAgentAreaService
    {

        private readonly IAgentAreaRepository _agentAreaRepository;
        private readonly IAgentRepository _agentRepository;

        public AgentAreaService(IAgentAreaRepository agentAreaRepository, IAgentRepository agentRepository)
        {
            _agentAreaRepository = agentAreaRepository;
            _agentRepository = agentRepository;

        }

        public int Delete(long id)
        {
            var AgentArea = _agentAreaRepository.Find(x => x.AgentAreaID == id).FirstOrDefault();
            if (AgentArea != null)
            {
                return _agentAreaRepository.SaveChanges();
            }
            return 0;
        }

        public List<AgentAreaModel> GetAgentAreas()
        {
            var AgentAreaDetails = Mapper.DynamicMap<List<AgentAreaModel>>(_agentAreaRepository.GetAgentAreas().ToList());

            return AgentAreaDetails;
        }

        public AgentAreaModel GetAgentAreaById(long id)
        {
            var AgentAreaModel = Mapper.DynamicMap<AgentAreaModel>(_agentAreaRepository.Find(x => x.AgentAreaID == id).FirstOrDefault());

            return AgentAreaModel;
        }

        public int SaveAgentArea(AgentAreaModel AgentAreaModel)
        {
            if (_agentAreaRepository.Find(x => x.AgentAreaID == AgentAreaModel.AgentAreaID).Any())
            {
                throw new Exception("Agent Group Name already exists");
            }
            var AgentArea = Mapper.DynamicMap<AgentArea>(AgentAreaModel);
            _agentAreaRepository.Add(AgentArea);
            return _agentAreaRepository.SaveChanges();
        }

        public int UpdateAgentArea(AgentAreaModel AgentAreaModel)
        {
            // should be a primary key(UserID) value check 
            var AgentArea = _agentAreaRepository.Find(x => x.AgentAreaID == AgentAreaModel.AgentAreaID).FirstOrDefault();
            if (AgentArea != null)
            {
                Mapper.CreateMap<AgentAreaModel, AgentArea>()
                    .ForMember(dest => dest.AgentAreaID, opt => opt.Ignore()); // ignore primary key while update/delete
                AgentArea user = (AgentArea)Mapper.Map(AgentAreaModel, AgentArea);

                return _agentAreaRepository.SaveChanges();
            }

            return 0;
        }

    }
}
