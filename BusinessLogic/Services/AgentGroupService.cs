﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using BusinessLogic.IServices;
using DataAccess.IRepository;
using DataAccess.Models;
using DomainModels;

namespace BusinessLogic.Services
{
    public class AgentGroupService : IAgentGroupService
    {

        private readonly IAgentGroupRepository _agentGroupRepository;
        private readonly IAgentRepository _agentRepository;

        public AgentGroupService(IAgentGroupRepository agentGroupRepository, IAgentRepository agentRepository)
        {
            _agentGroupRepository = agentGroupRepository;
            _agentRepository = agentRepository;

        }

        public int Delete(long id)
        {
            var AgentGroup = _agentGroupRepository.Find(x => x.AgentGroupID == id).FirstOrDefault();
          
            if (AgentGroup != null)
            {
                AgentGroup.RecordStatus = "D";
                return _agentGroupRepository.SaveChanges();
            }
            return 0;
        }

        public List<AgentGroupModel> GetAgentGroups()
        {
            var AgentGroupDetails = Mapper.DynamicMap<List<AgentGroupModel>>(_agentGroupRepository.GetAgentGroups().ToList());

            return AgentGroupDetails;
        }

        public AgentGroupModel GetAgentGroupById(long id)
        {
            var AgentGroupModel = Mapper.DynamicMap<AgentGroupModel>(_agentGroupRepository.Find(x => x.AgentGroupID == id).FirstOrDefault());

            return AgentGroupModel;
        }

        public int SaveAgentGroup(AgentGroupModel AgentGroupModel)
        {
            if (_agentGroupRepository.Find(x => x.AgentGroupID == AgentGroupModel.AgentGroupID).Any())
            {
                throw new Exception("Agent Group Name already exists");
            }

            AgentGroupModel.RecordStatus = "A";
            var AgentGroup = Mapper.DynamicMap<AgentGroup>(AgentGroupModel);
            _agentGroupRepository.Add(AgentGroup);
            return _agentGroupRepository.SaveChanges();
        }

        public int UpdateAgentGroup(AgentGroupModel AgentGroupModel)
        {
            // should be a primary key(UserID) value check 
            var AgentGroup = _agentGroupRepository.Find(x => x.AgentGroupID == AgentGroupModel.AgentGroupID).FirstOrDefault();
            if (AgentGroup != null)
            {
                Mapper.CreateMap<AgentGroupModel, AgentGroup>()
                    .ForMember(dest => dest.AgentGroupID, opt => opt.Ignore()); // ignore primary key while update/delete
                AgentGroup user = (AgentGroup)Mapper.Map(AgentGroupModel, AgentGroup);

                return _agentGroupRepository.SaveChanges();
            }

            return 0;
        }

        public IEnumerable<ChoiceOption> GetAgentNames()
        {
            return _agentRepository.GetAll().Select(x => new ChoiceOption
            {
                id = x.AgentID,
                text = x.AgentName
            }).ToList();
        }
    }
}
