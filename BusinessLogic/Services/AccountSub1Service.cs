﻿using BusinessLogic.IServices;
using DomainModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataAccess.IRepository;

namespace BusinessLogic.Services
{
    public class AccountSub1Service : IAccountSub1Service
    {
        private readonly IAccountSub1Repository _accountSub1Repository = null;
        public AccountSub1Service(IAccountSub1Repository accountSub1Repository)
        {
            _accountSub1Repository = accountSub1Repository;
        }
        public int DeleteAccountSub1(long id)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<ChoiceOption> GetAccountSub1ChoiceOptions()
        {
            return _accountSub1Repository.GetAll().Select(x => new ChoiceOption
            {
                id = x.AccountTypeSub1ID,
                text = x.Name
            }).OrderBy(x => x.text).Distinct();

        }

        public IEnumerable<ChoiceOption> GetAccountSub1ChoiceOptions(long id)
        {
            return _accountSub1Repository.GetAll().Where(x => x.AccountTypeSub1ID == id).Select(x => new ChoiceOption
            {
                id = x.AccountTypeSub1ID,
                text = x.Name
            }).OrderBy(x => x.text).Distinct();

        }


        public List<AccountTypeSub1Model> GetAccountTypesSub1()
        {
            throw new NotImplementedException();
        }

        public AccountTypeSub1Model GetAccountTypeSub1ById(long id)
        {
            throw new NotImplementedException();
        }

        public int Save(AccountTypeSub1Model accountTypeSub1Model)
        {
            throw new NotImplementedException();
        }

        public int Update(AccountTypeSub1Model accountTypeSub1Model)
        {
            throw new NotImplementedException();
        }
    }
}
