﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using BusinessLogic.IServices;
using DataAccess.IRepository;
using DataAccess.Models;
using DomainModels;

namespace BusinessLogic.Services
{
    public class SandooqService : ISandooqService
    {

        private readonly ISandooqRepository _sandooqRepository;
        public SandooqService(ISandooqRepository sandooqRepository)
        {
            _sandooqRepository = sandooqRepository;
        }

        public int DeleteSandooq(long id)
        {
            var Sandooq = _sandooqRepository.Find(x => x.SandooqID == id).FirstOrDefault();
            Sandooq.RecordStatus = "D";
            _sandooqRepository.Remove(Sandooq);
            return _sandooqRepository.SaveChanges();
        }

        public List<SandooqModel> GetSandooqs()
        {
            var SandooqDetails = Mapper.DynamicMap<List<SandooqModel>>(_sandooqRepository.GetSandooqs().ToList());

            return SandooqDetails;
        }

        public SandooqModel GetSandooqsById(long id)
        {
            var SandooqModel = Mapper.DynamicMap<SandooqModel>(_sandooqRepository.Find(x => x.SandooqID == id)
                .FirstOrDefault());

            return SandooqModel;
        }

        public int SaveSandooq(SandooqModel SandooqModel)
        {
            if (_sandooqRepository.Find(x => x.AccountID == SandooqModel.AccountID).Any())
            {
                throw new Exception("Sandooq  already exists for this account");
            }

            SandooqModel.RecordStatus = "A";
            var Sandooq = Mapper.DynamicMap<Sandooq>(SandooqModel);
            _sandooqRepository.Add(Sandooq);
            return _sandooqRepository.SaveChanges();
        }

        public int UpdateSandooq(SandooqModel SandooqModel)
        {
            // should be a primary key(UserID) value check 
            var Sandooq = _sandooqRepository.Find(x => x.SandooqID == SandooqModel.SandooqID).FirstOrDefault();
            if (Sandooq != null)
            {
                Mapper.CreateMap<SandooqModel, Sandooq>()
                    .ForMember(dest => dest.SandooqID, opt => opt.Ignore()); // ignore primary key while update/delete
                Sandooq user = (Sandooq)Mapper.Map(SandooqModel, Sandooq);

                return _sandooqRepository.SaveChanges();
            }

            return 0;
        }

        public IEnumerable<ChoiceOption> GetAccountNames()
        {
            return _sandooqRepository.GetAll().Select(x => new ChoiceOption
            {
                id = x.SandooqID,
                text = x.Notes
            }).ToList();
        }
        public int Delete(long id)
        {
            // should be a primary key(UserID) value check 
            var usr = _sandooqRepository.Find(x => x.SandooqID == id).FirstOrDefault();
            if (usr != null)
            {
                usr.RecordStatus = "D";
                return _sandooqRepository.SaveChanges();
            }

            return 0;
        }
    }
}
