﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using BusinessLogic.IServices;
using DataAccess.IRepository;
using DataAccess.Models;
using DomainModels;

namespace BusinessLogic.Services
{
    public class GroupTypeService : IGroupTypeService
    {

        private readonly IGroupTypeRepository _groupTypeRepository;
        public GroupTypeService(IGroupTypeRepository groupTypeRepository)
        {
            _groupTypeRepository = groupTypeRepository;
        }

        public int DeleteGroupType(long id)
        {
            var GroupType = _groupTypeRepository.Find(x => x.GroupTypeID == id).FirstOrDefault();
            GroupType.RecordStatus = "D";
            _groupTypeRepository.Remove(GroupType);
            return _groupTypeRepository.SaveChanges();
        }

        public List<GroupTypeModel> GetGroupTypes()
        {
            var GroupTypeDetails = Mapper.DynamicMap<List<GroupTypeModel>>(_groupTypeRepository.GetAll().ToList());

            return GroupTypeDetails;
        }

        public GroupTypeModel GetGroupTypeById(long id)
        {
            var GroupTypeModel = Mapper.DynamicMap<GroupTypeModel>(_groupTypeRepository.Find(x => x.GroupTypeID == id)
                .FirstOrDefault());

            return GroupTypeModel;
        }

        public int SaveGroupType(GroupTypeModel GroupTypeModel)
        {
            if (_groupTypeRepository.Find(x => x.GroupTypeName == GroupTypeModel.GroupTypeName).Any())
            {
                throw new Exception("GroupType Name already exists");
            }

            GroupTypeModel.RecordStatus = "A";
            var GroupType = Mapper.DynamicMap<GroupType>(GroupTypeModel);
            _groupTypeRepository.Add(GroupType);
            return _groupTypeRepository.SaveChanges();
        }

        public int UpdateGroupType(GroupTypeModel GroupTypeModel)
        {
            // should be a primary key(UserID) value check 
            var GroupType = _groupTypeRepository.Find(x => x.GroupTypeID == GroupTypeModel.GroupTypeID).FirstOrDefault();
            if (GroupType != null)
            {
                Mapper.CreateMap<GroupTypeModel, GroupType>()
                    .ForMember(dest => dest.GroupTypeID, opt => opt.Ignore()); // ignore primary key while update/delete
                GroupType user = (GroupType)Mapper.Map(GroupTypeModel, GroupType);

                return _groupTypeRepository.SaveChanges();
            }

            return 0;
        }

        public IEnumerable<ChoiceOption> GetGroupTypeNames()
        {
            return _groupTypeRepository.GetAll().Select(x => new ChoiceOption
            {
                id = x.GroupTypeID,
                text = x.GroupTypeName
            }).ToList();
        }

        public int Delete(long id)
        {
            // should be a primary key(UserID) value check 
            var usr = _groupTypeRepository.Find(x => x.GroupTypeID == id).FirstOrDefault();
            if (usr != null)
            {
                usr.RecordStatus = "D";
                return _groupTypeRepository.SaveChanges();
            }

            return 0;
        }
    }
}
