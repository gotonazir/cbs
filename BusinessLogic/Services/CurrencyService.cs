﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using BusinessLogic.IServices;
using DataAccess.IRepository;
using DataAccess.Models;
using DomainModels;

namespace BusinessLogic.Services
{
    public class CurrencyService : ICurrencyService
    {

        private readonly ICurrencyRepository _currencyRepository;
        public CurrencyService(ICurrencyRepository currencyRepository)
        {
            _currencyRepository = currencyRepository;
        }

        public int DeleteCurrency(long id)
        {
            var currency = _currencyRepository.Find(x => x.CurrencyID == id).FirstOrDefault();
         //    currency.RecordStatus = "D";
            _currencyRepository.Remove(currency);
            return _currencyRepository.SaveChanges();
        }

        public List<CurrencyModel> GetCurrencies()
        {
            var currencyDetails = Mapper.DynamicMap<List<CurrencyModel>>(_currencyRepository.GetAll().ToList());

            return currencyDetails;
        }

        public CurrencyModel GetCurrenciesById(long id)
        {
            var currencyModel = Mapper.DynamicMap<CurrencyModel>(_currencyRepository.Find(x => x.CurrencyID == id).FirstOrDefault());

            return currencyModel;
        }

        public int SaveCurrency(CurrencyModel currencyModel)
        {
            if (_currencyRepository.Find(x => x.Name == currencyModel.Name).Any())
            {
                throw new Exception("Currency Name already exists");
            }

           //  currencyModel.RecordStatus = "A";
            var currency = Mapper.DynamicMap<Currency>(currencyModel);
            _currencyRepository.Add(currency);
            return _currencyRepository.SaveChanges();
        }

        public int UpdateCurrency(CurrencyModel currencyModel)
        {
            // should be a primary key(UserID) value check 
            var currency = _currencyRepository.Find(x => x.CurrencyID == currencyModel.CurrencyID).FirstOrDefault();
            if (currency != null)
            {
                Mapper.CreateMap<CurrencyModel, Currency>()
                    .ForMember(dest => dest.CurrencyID, opt => opt.Ignore()); // ignore primary key while update/delete
                Currency user = (Currency)Mapper.Map(currencyModel, currency);

                return _currencyRepository.SaveChanges();
            }

            return 0;
        }

        public IEnumerable<ChoiceOption> GetCurrencyNames()
        {
            return _currencyRepository.GetAll().Select(x => new ChoiceOption
            {
                id = x.CurrencyID,
                text = x.Name
            }).ToList();
        }
        public int Delete(long id)
        {
            // should be a primary key(UserID) value check 
            var usr = _currencyRepository.Find(x => x.CurrencyID == id).FirstOrDefault();
           
            if (usr != null)
            {
                return _currencyRepository.SaveChanges();
            }

            return 0;
        }
    }
}
