﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using BusinessLogic.IServices;
using DataAccess.IRepository;
using DataAccess.Models;
using DomainModels;

namespace BusinessLogic.Services
{
    public class AccountService : IAccountService
    {
        private readonly IAccountRepository _accountRepository = null;
        private readonly IAccountCurrencyRepository _accountCurrencyRepository = null;

        public AccountService(IAccountRepository accountRepository, IAccountCurrencyRepository accountCurrencyRepository)
        {
            _accountRepository = accountRepository;
            _accountCurrencyRepository = accountCurrencyRepository;

        }
        public int Delete(long id)
        {
            var accountInformation = _accountRepository.GetAccountById(id);
            if (accountInformation != null)
            {
                accountInformation.RecordStatus = "D";
                foreach (var accountcurrency in accountInformation.AccountCurrencies)
                {
                    accountcurrency.RecordStatus = "D";
                }
                _accountRepository.Update(accountInformation);
                _accountRepository.SaveChanges();
                return 1;
            }

            return 0;
        }

        public List<AccountModel> GetAccountDetails()
        {
            var accountDetails = Mapper.DynamicMap<List<AccountModel>>(_accountRepository.GetAccountDetails().Where(x=>x.RecordStatus != "D").ToList());

            return accountDetails;
        }

        public AccountModel GetAccountDetailsById(long id)
        {
            return Mapper.DynamicMap<AccountModel>(_accountRepository.GetAccountById(id));
        }

        public bool HasDuplicateAccount(long accountNumber)
        {
            throw new NotImplementedException();
        }

        public int Save(AccountModel accountModel)
        {
            var account = Mapper.DynamicMap<Account>(accountModel);
            _accountRepository.Add(account);
            return _accountRepository.SaveChanges();
        }

        public int Update(AccountModel accountModel)
        {
            var accountInformation = _accountRepository.GetAccountById(accountModel.AccountID);
            if (accountInformation != null)
            {
                accountInformation.AccountTypeTreeId = accountModel.AccountTypeTreeId;
                accountInformation.AccountNo = accountModel.AccountNo;
                accountInformation.VIPNo = accountModel.VIPNo;
                accountInformation.OpenDt = accountModel.OpenDt;
                accountInformation.BranchID = accountModel.BranchID;
                accountInformation.CloseDt = accountModel.CloseDt;
                accountInformation.Notes = accountModel.Notes;
                accountInformation.ClientID = accountModel.ClientID;
                accountInformation.BeneficiaryID = accountModel.BeneficiaryID;
                accountInformation.AuthorizedID = accountModel.AuthorizedID;
                var accountCurrenyList = new List<AccountCurrencyModel>();
                for (int i = 0; i < accountModel.AccountCurrencies.Count; i++)
                {
                    if (accountModel.AccountCurrencies[i].AccountCurrencyID == 0)
                    {
                        accountCurrenyList.Add(accountModel.AccountCurrencies[i]);
                    }
                    else
                    {
                        accountInformation.AccountCurrencies[i].CurrencyID = accountModel.AccountCurrencies[i].CurrencyID;
                        accountInformation.AccountCurrencies[i].Balance = accountModel.AccountCurrencies[i].Balance;
                        accountInformation.AccountCurrencies[i].MinBalance = accountModel.AccountCurrencies[i].MinBalance;
                        accountInformation.AccountCurrencies[i].OverDraft = accountModel.AccountCurrencies[i].OverDraft;
                    }
                }
                _accountRepository.Update(accountInformation);
                _accountRepository.SaveChanges();
                if (accountCurrenyList.Count > 0)
                {
                    foreach (var item in accountCurrenyList)
                    {
                        var accCurrency = Mapper.DynamicMap<AccountCurrency>(item);
                        _accountCurrencyRepository.Add(accCurrency);
                        _accountCurrencyRepository.SaveChanges();
                    }
                }

                return 1;
            }

            return 0;
        }

        public IEnumerable<ChoiceOption> GetAccountNumbers()
        {
            return _accountRepository.GetAll().Select(x => new ChoiceOption
            {
                id = x.AccountID,
                text = x.Notes
            }).ToList();
        }
    }
}
