﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessLogic.IServices;
using DataAccess.IRepository;
using DomainModels;
using  AutoMapper;
using DataAccess.Models;

namespace BusinessLogic.Services
{
    public class UserService : IUserService
    {
        private readonly IUserRepository _userRepository;
        public UserService(IUserRepository userRepository)
        {
            _userRepository = userRepository;
        }
        public List<UserModel> GetUsers()
        {
            var userDetails = Mapper.DynamicMap<List<UserModel>>(_userRepository.GetAll().Where(x=>x.RecordStatus == "A").ToList());

            return userDetails;
        }

        public UserModel GetUsersById(long id)
        {
            var userDetails = Mapper.DynamicMap<UserModel>(_userRepository.Find(x=>x.UserID == id).FirstOrDefault());

            return userDetails;
        }

        public int Save(UserModel userModel)
        {
            var user = Mapper.DynamicMap<User>(userModel);
            _userRepository.Add(user);
            return _userRepository.SaveChanges();
        }

        //public int Update(UserModel userModel)
        //{

        //  var usr =  _userRepository.Find(x => x.UserID == userModel.UserID).FirstOrDefault();

        //    //Mapper.CreateMap<UserModel, User>()
        //    //    .ForMember(dest => dest.ChangePasswordDt, opt => opt.Ignore())
        //    //    .ForMember(dest => dest.UserID, opt => opt.Ignore());
        //    User user = (User)Mapper.DynamicMap<User>(userModel);

        //  //  usr = user;
        //    usr.ChangePasswordDt = userModel.ChangePasswordDt;

        //    _userRepository.Update(usr);
        //  return  _userRepository.SaveChanges();
        //}


        public int Update(UserModel userModel)
        {
            // should be a primary key(UserID) value check 
            var usr = _userRepository.Find(x => x.UserID == userModel.UserID).FirstOrDefault();
            if (usr != null)
            {
                Mapper.CreateMap<UserModel, User>()
                    .ForMember(dest => dest.UserID, opt => opt.Ignore()); // ignore primary key while update/delete
                User user = (User) Mapper.Map(userModel, usr);

                return _userRepository.SaveChanges();
            }

            return 0;
        }

        public int Delete(long userId)
        {
            // should be a primary key(UserID) value check 
            var usr = _userRepository.Find(x => x.UserID == userId).FirstOrDefault();
            if (usr != null)
            {
                usr.RecordStatus = "D";
                return _userRepository.SaveChanges();
            }

            return 0;
        }
    }
}
