﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessLogic.IServices;
using DataAccess.IRepository;
using DomainModels;
using AutoMapper;
using DataAccess.Models;

namespace BusinessLogic.Services
{
    public class ClientService : IClientService
    {
        private readonly IClientRepository _clientRepository;
        private readonly IClientDocumentRepository _clientDocumentRepository;

        public ClientService(IClientRepository clientRepository, IClientDocumentRepository clientDocumentRepository)
        {
            _clientRepository = clientRepository;
            _clientDocumentRepository = clientDocumentRepository;
        }
        public List<ClientModel> GetClient()
        {
            var userDetails = Mapper.DynamicMap<List<ClientModel>>(_clientRepository.GetClients().ToList());

            return userDetails;

        }

        public ClientModel GetClientsById(long id)
        {
            var data = _clientRepository.GetClientById(id);
            var clientModel = Mapper.DynamicMap<ClientModel>(_clientRepository.GetClientById(id));
            return clientModel;
        }

        public int Save(ClientModel clientModel)
        {
            var user = Mapper.DynamicMap<Client>(clientModel);
            _clientRepository.Add(user);
            return _clientRepository.SaveChanges();
        }

        public int Update(ClientModel clientModel)
        {
            List<ClientDocumentModel> documentModels = new List<ClientDocumentModel>();
            var client = _clientRepository.GetClientById(clientModel.ClientID);
            if (client != null)
            {
                client.FullName = clientModel.FullName;
                client.Address = clientModel.Address;
                client.CityID = clientModel.CityID;
                client.Phone = clientModel.Phone;
                client.Mobile = clientModel.Mobile;
                client.Fax = clientModel.Fax;
                client.PersonOrCorp = clientModel.PersonOrCorp;
                client.GuarantorID = clientModel.GuarantorID;
                client.PrivilegeID = clientModel.PrivilegeID;
                client.Notes = clientModel.Notes;
                client.SignImage = clientModel.SignImage;

                for (int i = 0; i < clientModel.ClientDocuments.Count; i++)
                {
                    if (clientModel.ClientDocuments[i].ClientDocumentID == 0)
                    {
                        documentModels.Add(clientModel.ClientDocuments[i]);
                    }
                    else
                    {
                        client.ClientDocuments[i].DocumentType = clientModel.ClientDocuments[i].DocumentType;
                        client.ClientDocuments[i].PlaceOfIssue = clientModel.ClientDocuments[i].PlaceOfIssue;
                        client.ClientDocuments[i].IssueDt = clientModel.ClientDocuments[i].IssueDt;
                        client.ClientDocuments[i].ExpiryDt = clientModel.ClientDocuments[i].ExpiryDt;
                        client.ClientDocuments[i].DocumentImage = clientModel.ClientDocuments[i].DocumentImage;
                    }

                  
                }
                _clientRepository.Update(client);
                _clientRepository.SaveChanges();
                if (documentModels.Count > 0)
                {
                    foreach (var item in documentModels)
                    {
                        var clientDocument = Mapper.DynamicMap<ClientDocument>(item);
                        _clientDocumentRepository.Add(clientDocument);
                        _clientDocumentRepository.SaveChanges();
                    }
                }

                return 1;
            }

            return 0;
        }

        public int Delete(long id)
        {
            // should be a primary key(UserID) value check 
            var client = _clientRepository.Find(x => x.ClientID == id).FirstOrDefault();
            if (client != null)
            {
                client.RecordStatus = "D";
                return _clientRepository.SaveChanges();
            }


            var clientDocument = _clientDocumentRepository.Find(x => x.ClientID == id).AsEnumerable();
            // if(clientDocument ! = null){
            foreach (var item in clientDocument)
            {
                item.RecordStatus = "D";
            }

            _clientDocumentRepository.SaveChanges();
            //  }
            return 0;
        }

        public IEnumerable<ChoiceOption> GetNames()
        {
            return _clientRepository.GetAll().Select(x => new ChoiceOption
            {
                id = x.ClientID,
                text = x.FullName
            }).AsEnumerable();
        }

        public bool HasClient(string clientName)
        {
            return _clientRepository.Find(x => x.FullName.ToLower() == clientName.ToLower()).Any();
        }

    }
}
