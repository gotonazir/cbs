﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using BusinessLogic.IServices;
using DataAccess.IRepository;
using DataAccess.Models;
using DomainModels;

namespace BusinessLogic.Services
{
    public class BranchService : IBranchService
    {

        private readonly IBranchRepository _branchRepository;
        public BranchService(IBranchRepository branchRepository)
        {
            _branchRepository = branchRepository;
        }

        public int DeleteBranch(long id)
        {
            var branch = _branchRepository.Find(x => x.BranchID == id).FirstOrDefault();
            branch.RecordStatus = "D";
            _branchRepository.Remove(branch);
            return _branchRepository.SaveChanges();
        }

        public List<BranchModel> GetBranches()
        {
            var branchDetails = Mapper.DynamicMap<List<BranchModel>>(_branchRepository.GetAll().ToList());

            return branchDetails;
        }

        public BranchModel GetBranchesById(long id)
        {
            var branchModel = Mapper.DynamicMap<BranchModel>(_branchRepository.Find(x => x.BranchID == id)
                .FirstOrDefault());

            return branchModel;
        }

        public int SaveBranch(BranchModel branchModel)
        {
            if (_branchRepository.Find(x => x.BranchName == branchModel.BranchName).Any())
            {
                throw new Exception("Branch Name already exists");
            }

            branchModel.RecordStatus = "A";
            var branch = Mapper.DynamicMap<Branch>(branchModel);
            _branchRepository.Add(branch);
            return _branchRepository.SaveChanges();
        }

        public int UpdateBranch(BranchModel branchModel)
        {
            // should be a primary key(UserID) value check 
            var branch = _branchRepository.Find(x => x.BranchID == branchModel.BranchID).FirstOrDefault();
            if (branch != null)
            {
                Mapper.CreateMap<BranchModel, Branch>()
                    .ForMember(dest => dest.BranchID, opt => opt.Ignore()); // ignore primary key while update/delete
                Branch user = (Branch)Mapper.Map(branchModel, branch);

                return _branchRepository.SaveChanges();
            }

            return 0;
        }

        public IEnumerable<ChoiceOption> GetBranchNames()
        {
            return _branchRepository.GetAll().Select(x => new ChoiceOption
            {
                id = x.BranchID,
                text = x.BranchName
            }).ToList();
        }
        public int Delete(long id)
        {
            var usr = _branchRepository.Find(x => x.BranchID == id).FirstOrDefault();
            if (usr != null)
            {
                usr.RecordStatus = "D";
                return _branchRepository.SaveChanges();
            }

            return 0;
        }
    }
}
