﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessLogic.IServices;
using DataAccess.IRepository;
using DomainModels;

namespace BusinessLogic.Services
{
    public class AccountTypeService : IAccountTypeService
    {
        private readonly IAccountTypeRepository _accountTypeRepository = null;
        public AccountTypeService(IAccountTypeRepository accountTypeRepository)
        {
            _accountTypeRepository = accountTypeRepository;
        }
        public int DeleteAccountType(long id)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<ChoiceOption> GetAccountChoiceOptions()
        {
            return _accountTypeRepository.GetAll().Select(x => new ChoiceOption
            {
                id = x.AccountTypeID,
                text = x.Name
            });
        }

        public IEnumerable<ChoiceOption> GetAccountChoiceOptions(long id)
        {
            throw new NotImplementedException();
        }

        public AccountTypeModel GetAccountTypeById(long id)
        {
            throw new NotImplementedException();
        }

        public List<AccountTypeModel> GetAccountTypes()
        {
            throw new NotImplementedException();
        }

        public int SaveAccountType(AccountTypeModel accountTypeModel)
        {
            throw new NotImplementedException();
        }

        public int UpdateAccountType(AccountTypeModel accountTypeModel)
        {
            throw new NotImplementedException();
        }
    }
}
