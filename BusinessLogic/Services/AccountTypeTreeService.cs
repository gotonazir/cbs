﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using BusinessLogic.IServices;
using DataAccess.IRepository;
using DataAccess.Models;
using DomainModels;

namespace BusinessLogic.Services
{
    public class AccountTypeTreeService : IAccountTypeTreeService
    {

        private readonly IAccountTypeTreeRepository _accountTypeTreeRepository = null;
        public AccountTypeTreeService(IAccountTypeTreeRepository accountTypeTreeRepository)
        {
            _accountTypeTreeRepository = accountTypeTreeRepository;
        }

        public IEnumerable<AccountTypeTreeModel> GetAccountTypeTrees()
        {
          return  Mapper.DynamicMap<List<AccountTypeTreeModel>>(_accountTypeTreeRepository.GetAll().ToList());
        }
    }
}
