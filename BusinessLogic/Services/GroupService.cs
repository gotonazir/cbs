﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using BusinessLogic.IServices;
using DataAccess.IRepository;
using DataAccess.Models;
using DomainModels;

namespace BusinessLogic.Services
{
    public class GroupService : IGroupService
    {
        
        private readonly IGroupRepository _groupRepository;
        private readonly IGroupTypeRepository _groupTypeRepository;

        public GroupService(IGroupRepository groupRepository, IGroupTypeRepository _groupTypeRepository)
        {
            _groupRepository = groupRepository;
        }

        public int DeleteGroup(long id)
        {
            var Group = _groupRepository.Find(x => x.GroupID == id).FirstOrDefault();
            Group.RecordStatus = "D";
            _groupRepository.Remove(Group);
            return _groupRepository.SaveChanges();
        }

        public List<GroupModel> GetGroups()
        {
            var GroupDetails = Mapper.DynamicMap<List<GroupModel>>(_groupRepository.GetGroups().ToList());

            return GroupDetails;
        }

        public GroupModel GetGroupsById(long id)
        {
            var GroupModel = Mapper.DynamicMap<GroupModel>(_groupRepository.Find(x => x.GroupID == id)
                .FirstOrDefault());

            return GroupModel;
        }

        public int SaveGroup(GroupModel GroupModel)
        {
            if (_groupRepository.Find(x => x.GroupName == GroupModel.GroupName).Any())
            {
                throw new Exception("Group Name already exists");
            }

            GroupModel.RecordStatus = "A";
            var Group = Mapper.DynamicMap<Group>(GroupModel);
            _groupRepository.Add(Group);
            return _groupRepository.SaveChanges();
        }

        public int UpdateGroup(GroupModel GroupModel)
        {
            // should be a primary key(UserID) value check 
            var Group = _groupRepository.Find(x => x.GroupID == GroupModel.GroupID).FirstOrDefault();
            if (Group != null)
            {
                Mapper.CreateMap<GroupModel, Group>()
                    .ForMember(dest => dest.GroupID, opt => opt.Ignore()); // ignore primary key while update/delete
                Group user = (Group)Mapper.Map(GroupModel, Group);

                return _groupRepository.SaveChanges();
            }

            return 0;
        }

        public IEnumerable<ChoiceOption> GetGroupNames()
        {
            return _groupRepository.GetAll().Select(x => new ChoiceOption
            {
                id = x.GroupID,
                text = x.GroupName
            }).ToList();
        }
        public int Delete(long id)
        {
            // should be a primary key(UserID) value check 
            var usr = _groupRepository.Find(x => x.GroupID == id).FirstOrDefault();
            if (usr != null)
            {
                usr.RecordStatus = "D";
                return _groupRepository.SaveChanges();
            }

            return 0;
        }
    }
}
