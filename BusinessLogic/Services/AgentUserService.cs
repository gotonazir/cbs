﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using BusinessLogic.IServices;
using DataAccess.IRepository;
using DataAccess.Models;
using DomainModels;

namespace BusinessLogic.Services
{
    public class AgentUserService : IAgentUserService
    {

        private readonly IAgentUserRepository _agentUserRepository;
        private readonly IAgentRepository _agentRepository;

        public AgentUserService(IAgentUserRepository agentUserRepository, IAgentRepository agentRepository)
        {
            _agentUserRepository = agentUserRepository;
            _agentRepository = agentRepository;
        }

        public int DeleteAgentUser(long id)
        {
            var AgentUser = _agentUserRepository.Find(x => x.AgentUserID == id).FirstOrDefault();
            AgentUser.RecordStatus = "D";
            _agentUserRepository.Remove(AgentUser);
            return _agentUserRepository.SaveChanges();
        }

        public List<AgentUserModel> GetAgentUsers()
        {
            var AgentUserDetails = Mapper.DynamicMap<List<AgentUserModel>>(_agentUserRepository.GetAgentUsers().ToList());

            return AgentUserDetails;
        }

        public AgentUserModel GetAgentUsersById(long id)
        {
            var AgentUserModel = Mapper.DynamicMap<AgentUserModel>(_agentUserRepository.Find(x => x.AgentUserID == id).FirstOrDefault());

            return AgentUserModel;
        }

        public int SaveAgentUser(AgentUserModel AgentUserModel)
        {
            if (_agentUserRepository.Find(x => x.AgentUserID == AgentUserModel.AgentUserID).Any())
            {
                throw new Exception("Agent User Name already exists");
            }

            AgentUserModel.RecordStatus = "A";
            var AgentUser = Mapper.DynamicMap<AgentUser>(AgentUserModel);
            _agentUserRepository.Add(AgentUser);
            return _agentUserRepository.SaveChanges();
        }

        public int UpdateAgentUser(AgentUserModel AgentUserModel)
        {
            // should be a primary key(UserID) value check 
            var AgentUser = _agentUserRepository.Find(x => x.AgentUserID == AgentUserModel.AgentUserID).FirstOrDefault();
            if (AgentUser != null)
            {
                Mapper.CreateMap<AgentUserModel, AgentUser>()
                    .ForMember(dest => dest.AgentUserID, opt => opt.Ignore()); // ignore primary key while update/delete
                AgentUser user = (AgentUser)Mapper.Map(AgentUserModel, AgentUser);

                return _agentUserRepository.SaveChanges();
            }

            return 0;
        }

        public IEnumerable<ChoiceOption> GetAgentNames()
        {
            return _agentRepository.GetAll().Select(x => new ChoiceOption
            {
                id = x.AgentID,
                text = x.AgentName
            }).ToList();
        }
        public int Delete(long id)
        {
            var usr = _agentUserRepository.Find(x => x.AgentUserID == id).FirstOrDefault();
            if (usr != null)
            {
                usr.RecordStatus = "D";
                return _agentUserRepository.SaveChanges();
            }

            return 0;
        }
    }
}
