﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using BusinessLogic.IServices;
using DataAccess.IRepository;
using DataAccess.Models;
using DomainModels;

namespace BusinessLogic.Services
{
    public class CityCountryService : ICityCountryService
    {

        private readonly ICityCountryRepository _cityCountryRepository;
        public CityCountryService(ICityCountryRepository cityCountryRepository)
        {
            _cityCountryRepository = cityCountryRepository;
        }
        public IEnumerable<ChoiceOption> GetCityCountryNames()
        {
            return _cityCountryRepository.GetAll().Select(x => new ChoiceOption
            {
                id = x.CityID,
                text = x.CityName
            }).ToList();
        }
    }
}
