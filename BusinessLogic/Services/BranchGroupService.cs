﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using BusinessLogic.IServices;
using DataAccess.IRepository;
using DataAccess.Models;
using DomainModels;

namespace BusinessLogic.Services
{
    public class BranchGroupService : IBranchGroupService
    {

        private readonly IBranchGroupRepository _branchGroupRepository;
        private readonly IBranchRepository _branchRepository;

        public BranchGroupService(IBranchGroupRepository branchGroupRepository, IBranchRepository branchRepository)
        {
            _branchGroupRepository = branchGroupRepository;
            _branchRepository = branchRepository;

        }

        public int DeleteBranchGroup(long id)
        {
            var branchGroup = _branchGroupRepository.Find(x => x.BranchGroupID == id).FirstOrDefault();
            branchGroup.RecordStatus = "D";
            _branchGroupRepository.Remove(branchGroup);
            return _branchGroupRepository.SaveChanges();
        }

        public List<BranchGroupModel> GetBranchGroups()
        {
            var branchGroupDetails = Mapper.DynamicMap<List<BranchGroupModel>>(_branchGroupRepository.GetAll().ToList());

            return branchGroupDetails;
        }

        public BranchGroupModel GetBranchGroupsById(long id)
        {
            var branchGroupModel = Mapper.DynamicMap<BranchGroupModel>(_branchGroupRepository.Find(x => x.BranchGroupID == id).FirstOrDefault());

            return branchGroupModel;
        }

        public int SaveBranchGroup(BranchGroupModel branchGroupModel)
        {
            if (_branchGroupRepository.Find(x => x.BranchGroupID == branchGroupModel.BranchGroupID).Any())
            {
                throw new Exception("Branch Name already exists");
            }

            branchGroupModel.RecordStatus = "A";
            var branchGroup = Mapper.DynamicMap<BranchGroup>(branchGroupModel);
            _branchGroupRepository.Add(branchGroup);
            return _branchGroupRepository.SaveChanges();
        }

        public int UpdateBranchGroup(BranchGroupModel branchGroupModel)
        {
            // should be a primary key(UserID) value check 
            var branchGroup = _branchGroupRepository.Find(x => x.BranchGroupID == branchGroupModel.BranchGroupID).FirstOrDefault();
            if (branchGroup != null)
            {
                Mapper.CreateMap<BranchGroupModel, BranchGroup>()
                    .ForMember(dest => dest.BranchGroupID, opt => opt.Ignore()); // ignore primary key while update/delete
                BranchGroup user = (BranchGroup)Mapper.Map(branchGroupModel, branchGroup);

                return _branchGroupRepository.SaveChanges();
            }

            return 0;
        }

        public IEnumerable<ChoiceOption> GetBranchNames()
        {
            return _branchRepository.GetAll().Select(x => new ChoiceOption
            {
                id = x.BranchID,
                text = x.BranchName
            }).ToList();
        }
        public int Delete(long id)
        {
            var usr = _branchGroupRepository.Find(x => x.BranchGroupID == id).FirstOrDefault();
            if (usr != null)
            {
                usr.RecordStatus = "D";
                return _branchGroupRepository.SaveChanges();
            }

            return 0;
        }
    }
}
