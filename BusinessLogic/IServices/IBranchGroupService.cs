﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DomainModels;

namespace BusinessLogic.IServices
{
    public interface IBranchGroupService
    {
        List<BranchGroupModel> GetBranchGroups();
        BranchGroupModel GetBranchGroupsById(long id);
        int SaveBranchGroup(BranchGroupModel branchGroupModel);
        int UpdateBranchGroup(BranchGroupModel branchGroupModel);
        int Delete(long id);
        IEnumerable<ChoiceOption> GetBranchNames();
    }
}
