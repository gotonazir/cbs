﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DomainModels;

namespace BusinessLogic.IServices
{
    public interface IAgentService
    {
        List<AgentModel> GetAgents();
        AgentModel GetAgentById(long id);
        int SaveAgent(AgentModel agentModel);
        int UpdateAgent(AgentModel agentModel);
        int Delete(long id);
        IEnumerable<ChoiceOption> GetAgentNames();
    }
}
