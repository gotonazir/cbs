﻿using DomainModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogic.IServices
{
    public interface IAccountTreeService
    {
        List<AccountTreeModel> GetAccountTrees();
        AccountTreeModel GetAccountTreeById(long id);
        int Save(AccountTreeModel accountTreeModel);
        int Update(AccountTreeModel accountTreeModel);

        int Delete(long id);

        IEnumerable<ChoiceOption> GetAccountChoiceOptions();
        //IEnumerable<ChoiceOption> GetAccountChoiceOptions(long id);
    }
}
