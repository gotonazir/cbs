﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DomainModels;

namespace BusinessLogic.IServices
{
    public interface IAgentAreaService
    {
        List<AgentAreaModel> GetAgentAreas();
        AgentAreaModel GetAgentAreaById(long id);
        int SaveAgentArea(AgentAreaModel AgentAreaModel);
        int UpdateAgentArea(AgentAreaModel AgentAreaModel);
        int Delete(long id);
    }
}
