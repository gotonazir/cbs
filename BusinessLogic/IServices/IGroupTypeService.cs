﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DomainModels;

namespace BusinessLogic.IServices
{
    public interface IGroupTypeService
    {
        List<GroupTypeModel> GetGroupTypes();
        GroupTypeModel GetGroupTypeById(long id);
        int SaveGroupType(GroupTypeModel groupTypeModel);
        int UpdateGroupType(GroupTypeModel groupTypeModel);
        int Delete(long id);
        IEnumerable<ChoiceOption> GetGroupTypeNames();
    }
}
