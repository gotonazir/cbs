﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataAccess.Models;
using DomainModels;

namespace BusinessLogic.IServices
{
    public interface IAccountService
    {
        List<AccountModel> GetAccountDetails();
        AccountModel GetAccountDetailsById(long id);
        int Save(AccountModel accountModel);
        int Update(AccountModel accountModel);
        int Delete(long id);
        bool HasDuplicateAccount(long accountNumber);
        IEnumerable<ChoiceOption> GetAccountNumbers();
    }
}
