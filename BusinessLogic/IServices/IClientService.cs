﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DomainModels;

namespace BusinessLogic.IServices
{
    public interface IClientService
    { 
        List<ClientModel> GetClient();
        ClientModel GetClientsById(long id);
        int Save(ClientModel client);
        int Update(ClientModel client);
        int Delete(long id);
        bool HasClient(string clientName);
        IEnumerable<ChoiceOption> GetNames();
    }
}
