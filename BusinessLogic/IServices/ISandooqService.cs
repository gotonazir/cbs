﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DomainModels;

namespace BusinessLogic.IServices
{
    public interface ISandooqService
    {
        List<SandooqModel> GetSandooqs();
        SandooqModel GetSandooqsById(long id);
        int SaveSandooq(SandooqModel SandooqModel);
        int UpdateSandooq(SandooqModel SandooqModel);
        int Delete(long id);
        IEnumerable<ChoiceOption> GetAccountNames();
    }
}
