﻿using DomainModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogic.IServices
{
    public interface ICurrencyService
    {
        List<CurrencyModel> GetCurrencies();
        CurrencyModel GetCurrenciesById(long id);
        int SaveCurrency(CurrencyModel currencyModel);
        int UpdateCurrency(CurrencyModel currencyModel);
        int Delete(long id);
        IEnumerable<ChoiceOption> GetCurrencyNames();
    }
}
