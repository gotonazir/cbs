﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DomainModels;

namespace BusinessLogic.IServices
{
    public interface IGroupService
    {
        List<GroupModel> GetGroups();
        GroupModel GetGroupsById(long id);
        int SaveGroup(GroupModel groupModel);
        int UpdateGroup(GroupModel groupModel);
        int Delete(long id);
        IEnumerable<ChoiceOption> GetGroupNames();
    }
}
