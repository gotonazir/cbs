﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DomainModels;

namespace BusinessLogic.IServices
{
    public interface IAgentGroupService
    {
        List<AgentGroupModel> GetAgentGroups();
        AgentGroupModel GetAgentGroupById(long id);
        int SaveAgentGroup(AgentGroupModel agentGroupModel);
        int UpdateAgentGroup(AgentGroupModel agentGroupModel);
        int Delete(long id);
        IEnumerable<ChoiceOption> GetAgentNames();
    }
}
