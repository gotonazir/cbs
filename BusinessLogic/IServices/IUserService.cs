﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DomainModels;

namespace BusinessLogic.IServices
{
    public interface IUserService
    {
        List<UserModel> GetUsers();
        UserModel GetUsersById(long id);
        int Save(UserModel user);
        int Update(UserModel user);
        int Delete(long userId);
    }
}
