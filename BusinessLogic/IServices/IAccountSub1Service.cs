﻿using DomainModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogic.IServices
{
    public interface IAccountSub1Service
    {
        List<AccountTypeSub1Model> GetAccountTypesSub1();
        AccountTypeSub1Model GetAccountTypeSub1ById(long id);
        int Save(AccountTypeSub1Model accountTypeSub1Model);
        int Update(AccountTypeSub1Model accountTypeSub1Model);

        int DeleteAccountSub1(long id);

        IEnumerable<ChoiceOption> GetAccountSub1ChoiceOptions();
    }
}
