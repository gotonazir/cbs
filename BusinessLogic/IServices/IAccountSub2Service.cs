﻿using DomainModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogic.IServices
{
    public interface IAccountSub2Service
    {
        List<AccountTypeSub2Model> GetAccountTypesSub2();
        AccountTypeSub2Model GetAccountTypeSub2ById(long id);
        int Save(AccountTypeSub2Model accountTypeSub2Model);
        int Update(AccountTypeSub2Model accountTypeSub2Model);

        int DeleteAccountSub2(long id);

        IEnumerable<ChoiceOption> GetAccountSub2ChoiceOptions();
        IEnumerable<ChoiceOption> GetAccountSub2ChoiceOptions(long id);
    }
}
