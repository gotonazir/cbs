﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DomainModels;

namespace BusinessLogic.IServices
{
   public interface IAccountTypeService
    {
        List<AccountTypeModel> GetAccountTypes();
        AccountTypeModel GetAccountTypeById(long id);
        int SaveAccountType(AccountTypeModel accountTypeModel);
        int UpdateAccountType(AccountTypeModel accountTypeModel);

        int DeleteAccountType(long id);

        IEnumerable<ChoiceOption> GetAccountChoiceOptions();
        IEnumerable<ChoiceOption> GetAccountChoiceOptions(long id);
    }
}
