﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DomainModels;

namespace BusinessLogic.IServices
{
    public interface IAgentUserService
    {
        List<AgentUserModel> GetAgentUsers();
        AgentUserModel GetAgentUsersById(long id);
        int SaveAgentUser(AgentUserModel agentUserModel);
        int UpdateAgentUser(AgentUserModel agentUserModel);
        int Delete(long id);
        IEnumerable<ChoiceOption> GetAgentNames();
    }
}
