﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DomainModels;

namespace BusinessLogic.IServices
{
    public interface IClientDocumentService
    {
        List<ClientDocumentModel> GetClientDocuments();
        int Save(ClientDocumentModel client);
        int Update(ClientDocumentModel client);
        int Delete(ClientDocumentModel client);
    }
}
