﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DomainModels;

namespace BusinessLogic.IServices
{
    public interface IBranchService
    {
        List<BranchModel> GetBranches();
        BranchModel GetBranchesById(long id);
        int SaveBranch(BranchModel branchModel);
        int UpdateBranch(BranchModel branchModel);
        int Delete(long id);
        IEnumerable<ChoiceOption> GetBranchNames();
    }
}
